/* JavaScript code */

$.cloudinary.config({ cloud_name: 'dvbxfq8qm', api_key: '982622374164581'});


var seleccion_palabras_consigna_type_1_2_called = false;
var seleccion_palabras_consigna_type_3_5_called = false;
var on_click_toolbar_option_called = false;


// conjuntos (sets)

function equalArrays(arr1, arr2) {
    return $(arr1).not(arr2).length === 0 && $(arr2).not(arr1).length === 0;
}

function isIn(as) {
    return function (a) {
        return as.has(a);
    };
}


// HTML entities. Solo se desencodean algunos caracteres para evitar XSS.
function unquote_html_entities(string)
{
    string = string.replace('&#39;', "'");
    string = string.replace('&quot;', '"');
    return string;
}


// cross-browsing

function isFirefox() {
    return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
}


// ejercicios sin andamiaje

function mostrar_error(mensaje)
{
    var error = $('.error');
    error.text(mensaje);
    error.show();
    window.setTimeout(function() { error.fadeOut(); }, 5000);
}

function siguiente_en_ejercicio(es_docente, avanzar_consigna, click_usuario)
{
    /*if (es_docente == true) {
        mostrar_siguiente_consigna(true, es_docente);
        deseleccionar_palabras();
        init_seleccion_multiples_palabras();
        return;
    }*/

    avanzar_consigna = typeof avanzar_consigna !== 'undefined' ? avanzar_consigna : true;
    click_usuario = typeof click_usuario !== 'undefined' ? click_usuario : false;

    if (String(consigna_type) == "5") {
        if (respuesta_correcta_andamiaje || avanzar_consigna) {
            respuesta_correcta_andamiaje = false;

            // se oculta el andamiaje
            $('#andamiaje').fadeOut();

            // se eliminan respuestas de la derecha
            $('.rta-right').text('')

            // se eliminan respuestas de arriba
            $('.word-property-top').remove();

            // se eliminan respuestas de abajo
            $('.word-property-wrapper').remove();

            mostrar_siguiente_consigna(true, es_docente);
            
            deseleccionar_palabras();
            init_seleccion_multiples_palabras();
        } else {
            if (click_usuario) {
                mostrar_error('Aún no respondiste correctamente');
            }
        }
    } else {
        $.post("/ejercicios/general/siguiente/",
            {
                "respuestas_parciales": JSON.stringify(respuestas_parciales),
                "numero_consigna": consigna_actual,
                "ejercicio_id": ejercicio_id,
                "consigna_type": consigna_type,
                "sobreescribir": click_usuario
            })
            .done(function(data) {
                var parsed_data = JSON.parse(data);
                var respuesta_correcta = parsed_data['respuesta_correcta'];

                if (!respuesta_correcta) {
                    marcar_en_rojo_respuestas_incorrectas(consigna_type, parsed_data, avanzar_consigna);
                }

                if ((respuesta_correcta || es_docente || avanzar_consigna) && click_usuario) {
                    mostrar_siguiente_consigna(respuesta_correcta, es_docente);

                    // mostrar respuestas de la siguiente consigna, si las hay
                    var nuevas_respuestas = parsed_data['nuevas_respuestas'];

                    while(respuestas_parciales.length > 0) {
                        respuestas_parciales.pop();
                    }
                    for(var i = 0; i < nuevas_respuestas.length; i++){
                        respuestas_parciales.push(nuevas_respuestas[i]);
                    }

                    if (nuevas_respuestas.length > 0) {
                        var oracion = $('#texto-oracion').text();
                        setup_divs_respuestas(oracion, false);
                        $(document).trigger("setup_divs_respuestas", [false, nuevas_respuestas, parsed_data]);

                        init_seleccion_multiples_palabras(true);
                    }
                }

                deseleccionar_palabras();

                // se lanza un segundo "siguiente" para evaluar si las nuevas respuestas son correctas o no
                if (click_usuario) {
                    siguiente_en_ejercicio(es_docente, avanzar_consigna, false);
                }
            })
            .fail(function() {
                mostrar_error('Error al presionar Siguiente, volvé a intentar.');
            });
    }
}

function marcar_en_rojo_respuestas_incorrectas(consigna_type, parsed_data, avanzar_consigna)
{
    var palabras_incorrectas = parsed_data['palabras_incorrectas'];
    var faltan_respuestas = parsed_data['faltan_respuestas'];

    if (consigna_type == 1 || consigna_type == 2) {
        var posiciones_incorrectas = new Array();

        for (var i = 0; i < palabras_incorrectas.length; i++) {
            posiciones_incorrectas.push(palabras_incorrectas[i]['posicion']);
        }

        var selector = '.split-sentence div div span';
        if (consigna_type == 2) {
            selector += ' span';
        }

        $(selector).each(function()
        {
            var posicion = '';
            if (consigna_type == 1) {
                posicion = parseInt($(this).attr('posicion'));
            } else {
                if (consigna_type == 2) {
                    posicion = parseInt($(this).parent().attr('posicion'));
                }
            }

            if(posiciones_incorrectas.indexOf(posicion) != -1) {
                $(this).css('color', 'red');
            }
        });
    } else {
        if (consigna_type == 3 || consigna_type == 5) {
            var opcion;
            var posiciones_palabras;
            var rta_id;

            // errores en respuestas debajo
            for (var i = 0; i < palabras_incorrectas.length; i++) {
                opcion = palabras_incorrectas[i]['opcion'];
                posiciones_palabras = palabras_incorrectas[i]['posiciones_palabras'];
                rta_id = get_rta_id(posiciones_palabras.sort().join(''), opcion);
                $('#' + rta_id).css('color', 'red');
            }

            // errores en respuestas a la derecha
            if (parsed_data['error_oracion']) {
                $('.rta-right').css('color', 'red');
            }
        }
    }

    if (faltan_respuestas && !avanzar_consigna) {
        mostrar_error('Faltan respuestas');
    }
}

function registrar_boton_omitir_consigna()
{
    $('#boton-omitir').confirm({
        text: "¿Estás seguro de que querés omitir esta consigna?",
        title: "Confirmación requerida",
        confirm: function(button) {
            siguiente_en_ejercicio(false, true, true);
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function mostrar_siguiente_consigna(respuesta_correcta, es_docente)
{
    es_docente = typeof es_docente !== 'undefined' ? es_docente : false;

    if (respuesta_correcta) {
        // esta consigna se pone con el tick verde
        $('.number.this').attr('class', 'number sprite done');
    } else {
        // esta consigna NO se pone con el tick verde
        $('.number.this').attr('class', 'number sprite wrong');
    }

    if (consigna_actual < cantidad_consignas) {
        // mostrar texto de la siguiente consigna y ocultar el texto actual
        $('#exercise-subtitle-' + consigna_actual).hide();
        consigna_actual++;
        $('#exercise-subtitle-' + consigna_actual).show();

        // la siguiente consigna pasa a ser la actual
        $('#consigna-' + consigna_actual).attr('class', 'number this');

        pasar_respuestas_a_record(consigna_actual - 1);

        // la lista de respuestas parciales es vaciada
        //respuestas_parciales = new Array();
        numero_respuesta = 1;

        consigna_type = $('#type-consigna-' + consigna_actual).val();

        init_seleccion_multiples_palabras();
    } else {
        mostrar_boton_finalizar(es_docente, respuesta_correcta);
    }
}

function mostrar_boton_finalizar(es_docente, ejercicio_resuelto_correctamente)
{
    if (consigna_actual >= cantidad_consignas) {
        // se ocultan los botones Siguiente y Omitir
        $('#boton-siguiente').hide();
        $('#boton-omitir').hide();

        // se muestra el boton Finalizar
        $('#boton-finalizar').show();

        if (!es_docente && ejercicio_resuelto_correctamente) {
            // Se muestra el mensaje de exito
            var alert_rail = $('.alert-rail');
            alert_rail.attr('class', 'alert-space');
            alert_rail.text('¡Ejercicio resuelto correctamente!');
            alert_rail.show();

            window.setTimeout(function() { alert_rail.fadeOut(); }, 5000);
        }
    }
}

function guardar_respuesta_parcial_clase_de_palabra(posiciones_palabras_seleccionadas, toolbar_id)
{
    posiciones_palabras_seleccionadas = typeof posiciones_palabras_seleccionadas !== 'undefined' ? posiciones_palabras_seleccionadas : new Array();
    toolbar_id = typeof toolbar_id !== 'undefined' ? toolbar_id : $('.toolbar-selected').attr('toolbar-id');

    // se muestra la respuesta
    $('.word-selected').children('span').each(function() {
        // se eliminan las respuestas siendo reemplazadas por una nueva respuesta
        $(this).siblings().remove();
        for (i = respuestas_parciales.length - 1; i >= 0; i--) {
            if (equalArrays(respuestas_parciales[i]['posiciones_palabras'], posiciones_palabras_seleccionadas)) {
                respuestas_parciales.splice(i, 1);
            }
        }

        var abreviatura = get_abreviatura(toolbar_id);
        var posicion = $(this).attr('posicion');
        var span_id = 'rta-' + posicion;
        $(this).parent().append('<div class="word-property-down lost-words"><span posicion="' + posicion + '" id="' + span_id + '">' + abreviatura + '</span></div>');
    });

    deseleccionar_palabras();

    // se guarda la respuesta
    guardar_respuesta(posiciones_palabras_seleccionadas, toolbar_id);
}

function guardar_respuesta_parcial_analisis_morfologico(posiciones_palabras_seleccionadas, toolbar_id)
{
    posiciones_palabras_seleccionadas = typeof posiciones_palabras_seleccionadas !== 'undefined' ? posiciones_palabras_seleccionadas : new Array();
    toolbar_id = typeof toolbar_id !== 'undefined' ? toolbar_id : $('.toolbar-selected').attr('toolbar-id');

    $('.word-selected span').clone().children().remove().end().each(function() {
        var posicion = $(this).attr('posicion');
        if($.inArray(posicion, posiciones_palabras_seleccionadas) === -1 && posicion !== undefined) {
            posiciones_palabras_seleccionadas.push(posicion);
        }
    });

    // se muestra la respuesta
    $('.word-selected').children('span').each(function(index) {
        var abreviatura = get_abreviatura(toolbar_id);
        var posicion = $(this).attr('posicion');

        var new_text = $(this).siblings('div').text();
        var respuesta_repetida = false;

        // primera respuesta para la palabra
        if (new_text == "") {
            new_text = abreviatura;
        } else {
            // evitar respuestas repetidas
            if (new_text.split(', ').indexOf(abreviatura) == -1) {
                new_text += ', ' + abreviatura;
            } else {
                respuesta_repetida = true;
            }
        }

        if (respuesta_repetida == false) {
            var inner_spans = "";
            var splitted_new_text = new_text.split(', ');
            var es_primera_respuesta = splitted_new_text.length == 1;
            var posicion_palabra = posiciones_palabras_seleccionadas[index];
            var span_id = get_id_span_analisis_morfologico(posicion_palabra, toolbar_id);
            var bin = '<div class="icon-bin" onclick="borrar_respuesta_analisis_morfologico(' + posicion_palabra + ', ' + toolbar_id + ');"></div>';
            var new_span = '<span class="rta-morfologico" id="' + span_id + '">' + abreviatura + bin + '</span>';

            var outer_span_id = 'rta-' + posicion;
            if (es_primera_respuesta && $('#' + outer_span_id).length == 0) {
                new_span = '<span posicion="' + posicion + '" id="' + outer_span_id + '">' + new_span + '</span>'
            }

            var prev = [];
            if (es_primera_respuesta) {
                // si no existe el div para contener a las respuestas, se lo crea
                if ($(this).siblings('div').length == 0) {
                    $(this).parent().append('<div class="word-property-down lost-words"></div>');
                }

                var appendTo = '';
                if ($('#' + outer_span_id).length == 0) {
                    appendTo = $(this).siblings('div');
                } else {
                    appendTo = $('#' + outer_span_id);
                }

                prev = $(new_span).appendTo(appendTo).children('span').last();
            } else {
                prev = $(new_span).appendTo('#' + outer_span_id).siblings('span').last();
            }

            if (! es_primera_respuesta) {
                // se agrega la coma separadora de la respuesta anterior a la nueva
                var children = prev.children();
                prev.text(prev.text() + ', ');
                prev.append(children);
            }

            // se guarda la respuesta
            guardar_respuesta([posicion_palabra.toString()], toolbar_id);
        } else {
            mostrar_error('Respuesta repetida');
        }
    });

    $('.rta-morfologico').hover(
        function() {
            $('.rta-morfologico').not(this).each(function(){
                $(this).stop().animate({opacity:0.4},200);
            });
        }, function() {
            $('.rta-morfologico').stop().animate({opacity:1},200);
        }
    );
}

function guardar_respuesta(posiciones_palabras, opcion)
{
    // se guarda la respuesta
    var respuesta = {
        'posiciones_palabras': posiciones_palabras,
        'opcion': opcion,
        'numero_respuesta': numero_respuesta
    };

    respuestas_parciales.push(respuesta);

    numero_respuesta++;
}

function guardar_respuesta_parcial_analisis_sintactico(posiciones_palabras_seleccionadas, toolbar_id)
{
    posiciones_palabras_seleccionadas = typeof posiciones_palabras_seleccionadas !== 'undefined' ? posiciones_palabras_seleccionadas : new Array();
    toolbar_id = typeof toolbar_id !== 'undefined' ? toolbar_id : $('.toolbar-selected').attr('toolbar-id');

    // se evitan respuestas repetidas
    var rta_id = get_rta_id(posiciones_palabras_seleccionadas.sort().join(''), toolbar_id);
    respuesta_repetida = $('#' + rta_id).length > 0;
    if (respuesta_repetida)
        return;

    console.log('guardar_respuesta_parcial_analisis_sintactico');

    var abreviatura = get_abreviatura(toolbar_id);
    var top = get_top(toolbar_id);
    var right = get_right(toolbar_id);
    var respuesta_mostrada = false;
    var property_box_down = $('.property-box-down');
    var property_box_up = $('.property-box-up');
    var respuesta_repetida = false;

    if (posiciones_palabras_seleccionadas.length == 0) {
        $("#exercise-content .split-sentence div.word-selected").each(function() {
            var posicion = $(this).children('span').attr('posicion');
            if($.inArray(posicion, posiciones_palabras_seleccionadas) === -1) {
                posiciones_palabras_seleccionadas.push(posicion);
            }
        });
    }

    if (posiciones_palabras_seleccionadas.length == 0 && right == "") {
        mostrar_error('La opción elegida requiere al menos una palabra seleccionada');
        return;
    }

    if (right != "") {
        posiciones_palabras_seleccionadas = new Array();
    }

    // respuestas que van a la derecha de la oracion
    if (right != "") {
        var texto_original = $('.property-box-right').children('span').text();
        var spans = "";
        var span_id = "rta-right-" + toolbar_id;

        if (texto_original != "") {
            abreviatura = ', ' + abreviatura;
        }

        spans += '<span class="rta-right" id="' + span_id + '" toolbar-id="' + toolbar_id + '" style="float: left;">' + abreviatura
                + '<div class="icon-bin" onclick="borrar_opcion_derecha(' + "'" + span_id + "'" + ');"></div></span>';

        try {
            if (es_pantalla_carga_ejercicio !== undefined) {
                $('.sprite.edit-sentence').remove();
                $('.sprite.erase-sentence').remove();
                spans += '<div class="sprite edit-sentence" onclick="editar_oracion();"></div><div class="sprite erase-sentence"></div>';
            }
        } catch(err) {
            // nothing to do
        }

        $('.property-box-right').append(spans);
    } else {
        var selector = "#exercise-content .split-sentence";
        selector += " div.word-selected";
        /*if (! es_pantalla_carga_ejercicio) {
            selector += " div.word-selected";
        }*/

        $(selector).each(function() {
            var posicion = $(this).children('span').attr('posicion');

            if (posicion == undefined) {
                console.log('undefined');
                return;
            } else {
                console.log(posicion);
            }

            var atributo_palabras = posiciones_palabras_seleccionadas.sort().join('');

            if (respuesta_mostrada == false) {
                var span_id = get_rta_id(atributo_palabras, toolbar_id);
                var line_id = 'line-' + posicion + '-' + toolbar_id;

                if (top != "") {
                    var word_width    = $('.word-selected').width() - 12;
                    var word_position = $('.word-selected').position().left + 5;
                    console.log(numero_respuesta);

                    var word_wrapper_id = get_word_wrapper_id(numero_respuesta, toolbar_id);

                    if ($('.word-selected').find('.word-property-top').length != 0) {
                        $('.word-selected').find('.word-property-top').remove();

                        property_box_up.prepend('<div class="word-property-wrapper-up" id="' + word_wrapper_id
                            + '" palabras="'+ atributo_palabras + '" numero-respuesta="' + numero_respuesta + '"><div class="word-property-top"><span id="' + span_id + '">' + abreviatura
                            + '</span><div class="line-up" id="' + line_id + '"></div>' + get_icon_bin_for_word_property_wrapper(numero_respuesta, toolbar_id) + '</div>');

                        $('#' + word_wrapper_id).css({
                                                       'width': word_width + 'px',
                                                       'left' : word_position + 'px',
                                                    });

                        start_value = null;
                    } else {
                        var selected_words_width = - 12;
                        var first_word_position = $('.word-selected').eq(0).position().left + 5;

                        property_box_up.append('<div class="word-property-wrapper-up" id="' + word_wrapper_id + '" palabras="'
                            + atributo_palabras + '" numero-respuesta="' + numero_respuesta + '"><div class="word-property-top"><span id="'
                            + span_id + '">' + abreviatura + '</span><div class="line-up" id="' + line_id
                            + '"></div>' + get_icon_bin_for_word_property_wrapper(numero_respuesta, toolbar_id) + '</div>');

                        $('.word-selected').each(function(index){
                            var single_word_width = $(this).width();
                            selected_words_width += single_word_width;
                        });

                        $('#' + word_wrapper_id).css({
                                                       'width': selected_words_width + 'px',
                                                       'left' : word_position + 'px',
                                                    });
                    }
                } else {
                    // se evitan respuestas repetidas
                    var rta_id = get_rta_id(posiciones_palabras_seleccionadas.sort().join(''), toolbar_id);
                    respuesta_repetida = $('#' + rta_id).length > 0;
                    console.log(respuesta_repetida);

                    if (!respuesta_repetida) {
                        var top_attr = null;
                        var outer_properties_to_lower = new Array();
                        var hay_respuesta_en_lugar = false;

                        // se determina si la respuesta tiene otra respuesta en su lugar, solo en ese caso se deberia bajar una outer respuesta
                        $('.word-property-wrapper[palabras]').each(function()
                        {
                            // Respuesta contenida en la respuesta nueva y a la misma altura
                            if ($(this).attr('palabras').indexOf(atributo_palabras) != -1 && $(this).css('top') == top_attr_new_word_property_wrapper + 'px') {
                                hay_respuesta_en_lugar = true;
                                return false;
                            }
                        });

                        var altura_actualizada = false;
                        var hay_interseccion = false;

                        $('.word-property-wrapper[palabras]').each(function()
                        {
                            var is_inner = $(this).attr('palabras').indexOf(atributo_palabras) != -1;
                            var is_outer = atributo_palabras.indexOf($(this).attr('palabras')) != -1;

                            // se buscan intersecciones entre respuestas existentes a la misma altura
                            hay_interseccion = false;
                            var array_posiciones = $(this).attr('palabras').split(',');
                            for (var i = 0; i < array_posiciones.length; i++) {
                                if (atributo_palabras.indexOf(array_posiciones[i]) != -1 && $(this).css('top') == top_attr_new_word_property_wrapper + 'px' && !is_inner && !is_outer) {
                                    hay_interseccion = true;
                                    break;
                                }
                            }

                            if (hay_interseccion) {
                                mostrar_error('Selección inválida');
                                return false;
                            }

                            // Si is_inner y is_outer valen ambos true significa que las palabras seleccionadas son exactamente
                            // las mismas de una respuesta anterior, entonces la nueva respuesta se coloca encima como si fuese
                            // inner.
                            if (is_inner && is_outer) {
                                is_outer = false;
                            }

                            // Si no es inner ni outer, se asume que hay respuesta en el lugar para ser desplazada hacia abajo
                            if (!is_inner && !is_outer && !hay_respuesta_en_lugar) {
                                hay_respuesta_en_lugar = true;
                            }

                            top_attr = parseInt($(this).position().top);

                            // la nueva propiedad es "inner"
                            if (is_inner)
                            {
                                if (hay_respuesta_en_lugar) {
                                    outer_properties_to_lower.push($(this));
                                }
                                // la altura del nuevo es el minimo entre las alturas de las respuestas viejas
                                if (top_attr < top_attr_new_word_property_wrapper) {
                                    top_attr_new_word_property_wrapper = top_attr;
                                    altura_actualizada = true;
                                }
                            }

                            // la nueva propiedad es "outer"
                            if (is_outer)
                            {
                                // la altura del nuevo es el maximo entre las alturas de las respuestas viejas
                                top_attr += INCREMENTO_VERTICAL;

                                if (top_attr > top_attr_new_word_property_wrapper || top_attr_new_word_property_wrapper == BIG_NUMBER) {
                                    top_attr_new_word_property_wrapper = top_attr;
                                    altura_actualizada = true;
                                }
                            }

                            if (!altura_actualizada || top_attr_new_word_property_wrapper == null) {
                                top_attr_new_word_property_wrapper = 0;
                            }
                        });

                        // si la seleccion no es valida, no se sigue
                        if (hay_interseccion) {
                            return false;
                        }

                        // se ordenan por altura las propiedades a bajar
                        outer_properties_to_lower.sort(function(a, b) {
                            var a_top = a.css('top').split('px')[0];
                            var b_top = b.css('top').split('px')[0];
                            return a_top - b_top;
                        });

                        // bajar word-property-wrapper de respuesta existente solo si necesita ser bajado
                        for (var i = 0; i < outer_properties_to_lower.length; i++) {
                            var bajar = false;

                            var current = outer_properties_to_lower[i];
                            if (i > 0) {
                                var previous = outer_properties_to_lower[i - 1];
                                var previous_top = parseInt(previous.css('top').split('px')[0]);
                                var current_top = parseInt(current.css('top').split('px')[0]);
                                if (current_top < previous_top + INCREMENTO_VERTICAL) {
                                    bajar = true;
                                }
                            } else {
                                bajar = current.css('top') == '0px';
                            }

                            if (bajar) {
                                top_attr = parseInt(current.position().top);
                                current.css({'top': (top_attr + INCREMENTO_VERTICAL) + 'px'});
                            }
                        }

                        if (top_attr_new_word_property_wrapper == BIG_NUMBER) {
                            top_attr_new_word_property_wrapper = 0;
                        }

                        if ($('.split-sentence').find('.word-selected').length == 1) {

                            // Se genera un id x cada 'word-property-wrapper' creado

                            var word_wrapper_id = get_word_wrapper_id(numero_respuesta, toolbar_id);
                            var class_raya = ' ' + get_class_raya(toolbar_id, posiciones_palabras_seleccionadas);

                            property_box_down.append('<div class="word-property-wrapper" id="' + word_wrapper_id + '" palabras="'
                                + atributo_palabras + '" numero-respuesta="' + numero_respuesta + '"><div class="word-property-down ' + class_raya + '" id="' + line_id
                                + '"></div><span id="' + span_id + '">' + abreviatura + '</span>' + get_icon_bin_for_word_property_wrapper(numero_respuesta, toolbar_id) + '</div>');


                            // Se asigna una posición vertical que aumenta cada vez que se elije la misma palabra


                            // Se le asigna como width, la suma del width de todas las palabras seleccionadas

                            var word_width    = $('.word-selected').width() - 12;
                            var word_position = $('.word-selected').position().left + 5;

                            $('#' + word_wrapper_id).css({
                                                           'width': word_width + 'px',
                                                           'left' : word_position + 'px',
                                                           'top'  : top_attr_new_word_property_wrapper + 'px',
                                                        });

                            start_value = null;
                        } else {
                            var word_wrapper_id = get_word_wrapper_id(numero_respuesta, toolbar_id);

                            var selected_words_width = - 12;
                            var first_word_position = $('.word-selected').eq(0).position().left + 5;

                            $('.word-selected').each(function(index){
                                var single_word_width = $(this).width();
                                selected_words_width += single_word_width;
                            });

                            $('.word-selected').attr('related-width', selected_words_width + 'px');

                            var related_width = parseFloat($('.word-selected').attr('related-width'));
                            var class_raya = ' ' + get_class_raya(toolbar_id, posiciones_palabras_seleccionadas);

                            var word_property_wrapper = '<div class="word-property-wrapper" id="' + word_wrapper_id + '" palabras="'
                                + atributo_palabras + '" numero-respuesta="' + numero_respuesta + '"><div class="word-property-down' + class_raya + '" id="'
                                + line_id + '"></div><span id="' + span_id + '">' + abreviatura + '</span>'
                                + get_icon_bin_for_word_property_wrapper(numero_respuesta, toolbar_id) + '</div>';
                            if (related_width > selected_words_width) {
                                property_box_down.prepend(word_property_wrapper);
                            } else {
                                property_box_down.append(word_property_wrapper);
                            }

                            $('#' + word_wrapper_id).css({
                                                           'width': selected_words_width + 'px',
                                                           'left' : first_word_position + 'px',
                                                           'top'  : top_attr_new_word_property_wrapper + 'px',
                                                        });
                        }
                    }
                }

                respuesta_mostrada = true;
            }
        });
    }

    $('.word-property-wrapper').hover(
        function() {
            $('.word-property-wrapper').not(this).each(function(){
                $(this).stop().animate({opacity:0.4},200);
            });
        }, function() {
            $('.word-property-wrapper').stop().animate({opacity:1},200);
        }
    );

    $('.word-property-wrapper-up').hover(
        function() {
            $('.word-property-wrapper-up').not(this).each(function(){
                $(this).stop().animate({opacity:0.4},200);
            });
        }, function() {
            $('.word-property-wrapper-up').stop().animate({opacity:1},200);
        }
    );

    $('.rta-right').hover(
        function() {
            $('.rta-right').not(this).each(function(){
                $(this).stop().animate({opacity:0.4},200);
            });
        }, function() {
            $('.rta-right').stop().animate({opacity:1},200);
        }
    );

    if (respuesta_mostrada && !respuesta_repetida) {
        guardar_respuesta(posiciones_palabras_seleccionadas, toolbar_id);
        deseleccionar_palabras();

        var sobreescribir = true;  // TODO: sobrescribir cuando sea click del usuario

        if (consigna_type == 5 && es_pantalla_carga_ejercicio == false) {
            // se llama al web service que valida en el acto la respuesta y se muestra el andamiaje
            $.post("/ejercicios/con_andamiaje/validar_respuesta/",
            {
                "posiciones_palabras": posiciones_palabras_seleccionadas,
                "opcion_de_respuesta": toolbar_id,
                "numero_consigna": consigna_actual,
                "ejercicio_id": ejercicio_id,
                "consigna_type": consigna_type,
                "sobreescribir": sobreescribir
            })
            .done(function(data) {
                var parsed_data = JSON.parse(data);
                var mensaje = parsed_data['mensaje'];

                if (parsed_data['respuesta_correcta']) {
                    $('#andamiaje').attr('class', 'correct');
                    respuesta_correcta_andamiaje = true;
                } else {
                    $('#andamiaje').attr('class', 'incorrect');
                    respuesta_correcta_andamiaje = false;
                }

                $('#texto-andamiaje').text(mensaje);
                $('#andamiaje').fadeIn();
            })
            .fail(function() {
                mostrar_error('Error al procesar andamiaje.');
            });
        }
    }
}

function render_abreviatura(element_id, toolbar_id)
{
    $(document).ready(function()
    {
        $('#' + element_id).text(get_abreviatura(toolbar_id));
    });
}

function get_abreviatura(toolbar_id)
{
    return _get_property('abrev', toolbar_id);
}

function get_top(toolbar_id)
{
    return _get_property('top', toolbar_id);
}

function get_right(toolbar_id)
{
    return _get_property('right', toolbar_id);
}

function _get_property(property_name, toolbar_id)
{
    // TODO obtener los valores la primera vez solamente y poner en un diccionario
    var property = "";
    $('a').each(function() {
        if ($(this).attr('toolbar-id') == toolbar_id) {
            property = $(this).attr(property_name);
            return false;
        }
    });

    if (property == undefined) {
        property = "";
    }

    return property;
}

function init_ejercicio()
{
    $(document).ready(function()
    {
        console.log('init_ejercicio');

        var oracion = $("p").text();
        setup_divs_respuestas(oracion, true);
        init_seleccion_multiples_palabras();
        registrar_boton_omitir_consigna();
    });

    $(window).resize(function() {
        var oracion_width = $('#exercise-content').width();
        var toolbar_width = $('#toolbar').width();
        var window_width = $(window).width();

        if (oracion_width + toolbar_width > window_width) {
            $('.horizontal-scroll').css('overflow', 'scroll');
        } else {
            $('.horizontal-scroll').css('overflow', 'auto');
        }
    });
}

function setup_divs_respuestas(oracion, primera_vez)
{
    console.log('setup_divs_respuestas, primera_vez = ' + primera_vez + ', oracion = "' + oracion + '"');

    var exercise_width = 0;

    // se borran todos los hijos antes de agregar las palabras una por una
    $("#exercise-content .split-sentence").children().remove();

    $(oracion.split(' ')).each(function(index){
        $("#exercise-content .split-sentence").append('<div class="word" vertical="-' + INCREMENTO_VERTICAL + 'px"><span posicion=' + index + '>' + this + '</span></div>');
    });

    $("#exercise-content").css('width', exercise_width + 0 + 'px');
    $('.full-sentense').remove();

    var oracion_divs = '<div class="icon-bracket-left"></div><div class="icon-bracket-right"></div><div class="property-box-right"><span></span>';

    try {
        if (es_pantalla_carga_ejercicio !== undefined) {
            oracion_divs += '<div class="sprite edit-sentence" onclick="editar_oracion();"></div><div class="sprite erase-sentence"></div>';
        }
    } catch(err) {
        // nothing to do
    }
    oracion_divs += '</div>';
    $('.split-sentence').append(oracion_divs);

    $('.word').each(function(index){
        var word_width = $(this).width();
        $(this).css('width', word_width + 'px');
        exercise_width += $(this).width();
    });

    $("#exercise-content").css('width', exercise_width + 'px');

    $("#exercise-content p .word").click(function () {
        if ((consigna_type == 3 || consigna_type == 5) && start_value == null && end_value == null) {
            deseleccionar_palabras();
            $(this).toggleClass('word-selected');
        }
    });

    if (primera_vez) {
        $(document).trigger("setup_divs_respuestas", [primera_vez]);
    }
}

function toggle_word_selected_class()
{
    $(this).toggleClass('word-selected');
}

function onclick_multiple_word_selected()
{
    // var value = $(this).index();
    var position = $(this).find('span').attr('posicion');
    var value    = parseInt(position);

    if(start_value == null) {
        start_value = value;
    } else {
        end_value = value;
    }

    console.log( 'start ' + start_value );
    console.log( 'end ' + end_value );
    console.log( ' ' );

    if (start_value != null && end_value != null){
        // swap
        if (start_value > end_value) {
            var aux = start_value;
            start_value = end_value;
            end_value = aux;
        }
        $('.split-sentence .word').slice( start_value, end_value + 1 ).addClass('word-selected');

        end_value = null;
        start_value = null;
    }
}

function init_seleccion_multiples_palabras(force)
{
    // prepara las palabras para seleccion multiple de analisis sintactico
    force = typeof force !== 'undefined' ? force : false;
    consigna_type = parseInt(consigna_type);

    if (consigna_type == 3 || consigna_type == 5) {
        if (seleccion_palabras_consigna_type_3_5_called == false || force) {
            console.log( 'start ' + start_value );
            console.log( 'end ' + end_value );
            console.log( ' ' );

            $('.property-box-down').width();

            var paragraph_width = $('.split-sentence').width();
            $('.property-box-down').css('width',paragraph_width + 'px');
            $('.property-box-up').css('width',paragraph_width + 'px');

            $("#exercise-content p .word").unbind('click', toggle_word_selected_class);
            $('.split-sentence .word').click(onclick_multiple_word_selected);

            seleccion_palabras_consigna_type_1_2_called = false;
            seleccion_palabras_consigna_type_3_5_called = true;
        }
    } else {
        if (consigna_type == 1 || consigna_type == 2) {
            if (seleccion_palabras_consigna_type_1_2_called == false || force) {
                $('.split-sentence .word').unbind('click', onclick_multiple_word_selected);
                $("#exercise-content p .word").click(toggle_word_selected_class);

                seleccion_palabras_consigna_type_1_2_called = true;
                seleccion_palabras_consigna_type_3_5_called = false;
            }
        }
    }

    /* TODO: Arreglar deseleccion de palabras al hacer click fuera de la oracion, el codigo comentado rompe la
             seleccion de palabras en Firefox. */
    /*$('#exercise-wrapper').click(function () {
        deseleccionar_palabras();
    });*/

    $('.word').click(function (event) {
        event.stopPropagation();
    });
}

function pasar_respuestas_a_record(consigna)
{
    // consignas de analisis sintactico no van al record
    if (consigna_type != 3 && consigna_type != 5) {
        var i = 0;
        var record_element;

        $('.word-property-down.lost-words > span').each(function() {
            var rta_palabra = '';

            if (consigna_type == 1) {
                rta_palabra = $(this).text();
            } else {
                if (consigna_type == 2) {
                    $(this).children('span').each(function() {
                        rta_palabra += $(this).text();
                    });
                }
            }

            var word_width = $(this).parent().width();
            record_element = $('#record-consigna-' + consigna + '-palabra-' + i);
            record_element.text(rta_palabra);
            record_element.css('width', word_width + 'px');

            $(this).remove();
            i++;
        });

        // mostrar consigna en el record
        $('#record-consigna-' + consigna).show().find('li[id^=record-consigna]').wrapAll("<div class='record-wrapper' />");

        // mismo width que la oración
        var paragraph_width = $('.split-sentence').width();
        var record_wrapper_height = $('.record-wrapper').height();
        $('.record-wrapper').css('width',paragraph_width + 'px');
        var MIN_RECORD_HEIGHT = 20;
        if (record_wrapper_height < MIN_RECORD_HEIGHT) {
            // altura minima en caso de record vacio al omitir consigna
            record_wrapper_height = MIN_RECORD_HEIGHT;
        }

        // ajuste de altura
        record_wrapper_height = record_wrapper_height * 2.5;

        $('.record-wrapper').css('width', paragraph_width + 'px');
        $('#record-consigna-' + consigna).css('height',record_wrapper_height + 'px');

        // mismo posición que la oración
        function wrapperLeft() {
            var paragraph_position = $('.split-sentence').offset().left;
            $('.record-wrapper').css('left',paragraph_position + 'px');
        }
        wrapperLeft();
        $(window).resize(wrapperLeft);

        // se coloca un guion para las palabras que no tienen respuesta (TODO: fixear posicion horizontal)
        $('.record-wrapper > li').each(function(){ if($(this).text() == "") { $(this).text('-'); } });
    }
}

function deseleccionar_palabras()
{
    // se deseleccionan las palabras
    if (consigna_type == 3 || consigna_type == 5) {
        if($('.word-range-selected').length != 0 ){
            if($('.word-range-sleep').length == 0 ){
                $('.word-selected').unwrap().unwrap();
            }
        }
        $('.word-range-selected').attr('class', 'word-range-unselected');
        $('.word-selected').removeClass('word-selected');
    } else {
        $('.word-selected').removeClass('word-selected');
    }

    start_value = null;
    end_value = null;
}

function get_word_wrapper_id(numero_respuesta, toolbar_id)
{
    return 'wrapper-' + numero_respuesta + '-' + toolbar_id;
}

function borrar_word_property_wrapper(numero_respuesta, toolbar_id)
{
    $('.word-property-wrapper').stop().animate({opacity:1},200);
    var word_property_wrapper_id = get_word_wrapper_id(numero_respuesta, toolbar_id);
    var posiciones_palabras = new Array();
    var numero_respuesta_wrapper = $('#' + word_property_wrapper_id).attr('numero-respuesta');

    borrar_respuesta_parcial(toolbar_id, posiciones_palabras, numero_respuesta_wrapper);

    // se borra del DOM
    $('#' + word_property_wrapper_id).remove();
}

function borrar_opcion_derecha(span_id)
{
    var span = $('#' + span_id);

    // Si se borra la primera opcion y hay una segunda, se le tiene que sacar la coma a esta ultima.
    // Si no tiene previous sibling, entonces es el primero.
    if (span.prev().text() == "" && span.next().text() != "") {
        var next = span.next();
        var children = next.children();
        next.text(next.text().split(', ')[1]);
        next.append(children);
    }

    var toolbar_id = span.attr('toolbar-id');
    var posiciones_palabras = new Array();
    borrar_respuesta_parcial(toolbar_id, posiciones_palabras);

    span.remove();
}

function borrar_respuesta_analisis_morfologico(posicion_palabra, toolbar_id)
{
    var span_id = get_id_span_analisis_morfologico(posicion_palabra, toolbar_id);
    var span = $('#' + span_id);

    // ajuste de las comas separadoras
    var prev = span.prev();
    var next = span.next();

    // Si se elimina el ultimo, se saca la coma del penultimo
    if (prev.length > 0 && next.length == 0) {
        var children = prev.children();
        prev.text(prev.text().split(', ')[0]);
        prev.append(children);
    }

    // borrar del DOM
    span.remove();
    $('.rta-morfologico').css('opacity','1');
    borrar_respuesta_parcial(toolbar_id, [posicion_palabra.toString()]);
}

function get_id_span_analisis_morfologico(posicion_palabra, toolbar_id)
{
    return 'rta-morfologico-' + posicion_palabra + '-' + toolbar_id;
}

function borrar_respuesta_parcial(toolbar_id, posiciones_palabras, numero_respuesta)
{
    numero_respuesta = typeof numero_respuesta !== 'undefined' ? numero_respuesta : '';

    // se borra de la lista
    for (var i = 0; i < respuestas_parciales.length; i++) {
        if (respuestas_parciales[i]['opcion'] == toolbar_id) {
            var array1 = respuestas_parciales[i]['posiciones_palabras'];
            var array2 = posiciones_palabras;
            var is_same = (array1.length == array2.length) && array1.every(function(element, index) {
                return element === array2[index];
            });
            if (is_same || (posiciones_palabras.length == 0 && respuestas_parciales[i]['numero_respuesta'] == numero_respuesta)) {
                respuestas_parciales.splice(i, 1);
                break;
            }
        }
    }
}

function get_icon_bin_for_word_property_wrapper(numero_respuesta, toolbar_id)
{
    return '<div class="icon-bin" onclick="borrar_word_property_wrapper(' + numero_respuesta + ', ' + toolbar_id + ');"></div>';
}

function get_rta_id(posiciones_palabras, opcion)
{
    var posiciones_palabras_str = posiciones_palabras.replace(',', '');
    return 'rta-' + opcion + '-' + posiciones_palabras_str;
}


// tipos de rayas

function get_toolbar_ids_sin_raya()
{
    // Subordinante, Subordinante Preposicional, Subordinante Comparativo y Subordinante Incluyente
    return [ '404', '405', '406', '327' ];
}

function get_toolbar_ids_con_raya_recta()
{
    // Núcleo, Signo de Cuasirreflejo, Signo de Cuasirreflejo Pasivo y Signo de Cuasirreflejo Impersonal
    return [ '323', '358', '359', '360' ];
}

function get_class_raya(toolbar_id, palabras)
{
    var class_raya = '';

    // caso particular de MD
    if (toolbar_id == '324') {
        if (palabras.length > 1) {
            // lleva caja
            class_raya = 'line-down-sides';
        }
    } else {
        // no lleva raya
        if (get_toolbar_ids_sin_raya().indexOf(toolbar_id) != -1) {
            class_raya = '';
        } else {
            // lleva raya recta
            if (get_toolbar_ids_con_raya_recta().indexOf(toolbar_id) != -1) {
                class_raya = 'line-down';
            } else {
                // lleva caja
                class_raya = 'line-down-sides';
            }
        }
    }

    return class_raya;
}

function init_record()
{
    $(document).ready(function(){
		var title = $('.exercise-record .title');

		title.click(function () {
			var icon 	    = $(this).find('.sprite');
			var main_height = $(this).parent().parent().height();
			var record_id   = $(this).parent().parent().attr('id');

			if (icon.hasClass('plus-icon')) {
				icon.removeClass('plus-icon');
				icon.addClass('minus-icon');

                var new_height = main_height + 'px';
				if (record_id in record_alturas_consignas) {
                    new_height = record_alturas_consignas[record_id];
				}

				$(this).parent().parent().css('height', new_height);
				$(this).siblings().show();
			} else {
				icon.removeClass('minus-icon');
				icon.addClass('plus-icon');

				record_alturas_consignas[record_id] = $(this).parent().parent().css('height');

				$(this).parent().parent().css('height', '20px');
				$(this).siblings().hide();
			}
		});

	});
}

function _init_modal(clase, id_boton)
{
    var modal = $('.modal.' + clase);

    $('#' + id_boton).click(function() {
        modal
            .css('opacity', 0)
            .animate(
                { opacity: 1 },
                { queue: false, duration: 'slow' }
            )
            .slideDown('fast');
    });
    $('.modal.folder .close-modal').click(function() {
        modal
            .animate(
                { opacity: 0 },
                { queue: false, duration: 'slow' }
            )
            .fadeOut();

            // se recarga la pagina para mostrar el nuevo item creado
            if (item_creado) {
                recargar_pagina();
            }
    });
}

function init_modal_unirme_materia()
{
    _init_modal('unirme-materia', 'boton-unirme-materia');
}

function init_modal_crear_curso()
{
    var clase = 'nuevo-curso';
    _init_modal(clase, 'boton-nuevo-curso');

    var modal = $('.modal.' + clase);
    $('.modal.folder .close-modal').click(function() {
        modal
            .animate(
                { opacity: 0 },
                { queue: false, duration: 'slow' }
            )
            .fadeOut();

            // se recarga la pagina para mostrar el nuevo item creado
            if (item_creado) {
                recargar_pagina();
            } else {
                $('#nombre-nuevo-curso').val('');
                $('.error-space').text('');
            }
    });
}

function init_modal_editar_curso()
{
    var clase = 'editar-curso';
    var modal = $('.modal.' + clase);

     $('.sprite.edit').click(function() {
        modal
            .css('opacity', 0)
            .animate(
                { opacity: 1 },
                { queue: false, duration: 'slow' }
            )
            .slideDown('fast');

        $('#nombre-curso').val($(this).attr('curso_nombre'));
        curso_editado_id = Number($(this).attr('curso_id'));
    });

    function close() {
        modal
            .animate(
                { opacity: 0 },
                { queue: false, duration: 'slow' }
            )
            .fadeOut();
    }
    $('.modal.folder .close-modal').click(function() {
        close();
    });

    $('#boton-editar-curso-cancelar').click(function() {
        close();
    });

    $('#boton-editar-curso-guardar').click(function() {
        editar_curso();
        close();
    });
}


function init_modal_copiar_carpeta()
{
     var clase = 'copiar-carpeta';
     var modal = $('.modal.' + clase);
     var carpeta_a_copiar_id = "";
     var carpeta_a_copiar_nombre = "";
     var curso_donde_copiar_id = "";

     $('.sprite.copy').click(function() {
        modal
            .css('opacity', 0)
            .animate(
                { opacity: 1 },
                { queue: false, duration: 'slow' }
            )
            .slideDown('fast');

        carpeta_a_copiar_id = Number($(this).attr('carpeta_id'));
        carpeta_a_copiar_nombre = $(this).attr('carpeta_nombre');

        $('#nombre-carpeta-a-copiar').text(carpeta_a_copiar_nombre);
    });

    function close() {
        modal
            .animate(
                { opacity: 0 },
                { queue: false, duration: 'slow' }
            )
            .fadeOut();
    }
    $('.modal.folder .close-modal').click(function() {
        close();
    });

    $('#boton-copiar-carpeta-cancelar').click(function() {
        close();
    });

    $('#boton-copiar-carpeta-ok').click(function() {
        copiar_carpeta(carpeta_a_copiar_id, carpeta_a_copiar_nombre);
        close();
    });
}


function init_modal_editar_carpeta()
{
    var clase = 'editar-carpeta';
    var modal = $('.modal.' + clase);

     $('.sprite.edit').click(function() {
        modal
            .css('opacity', 0)
            .animate(
                { opacity: 1 },
                { queue: false, duration: 'slow' }
            )
            .slideDown('fast');

        $('#nombre-carpeta').val($(this).attr('carpeta_nombre'));
        carpeta_editada_id = Number($(this).attr('carpeta_id'));
    });

    function close() {
        modal
            .animate(
                { opacity: 0 },
                { queue: false, duration: 'slow' }
            )
            .fadeOut();
    }
    $('.modal.folder .close-modal').click(function() {
        close();
    });

    $('#boton-editar-carpeta-cancelar').click(function() {
        close();
    });

    $('#boton-editar-carpeta-guardar').click(function() {
        editar_carpeta();
        close();
    });
}

function init_modal_crear_carpeta()
{
    var clase = 'nueva-carpeta';
    _init_modal(clase, 'boton-nueva-carpeta');

    var modal = $('.modal.' + clase);
    $('.modal.folder .close-modal').click(function() {
        close_modal_crear_carpeta();
    });
}

function close_modal_crear_carpeta()
{
    var clase = 'nueva-carpeta';
    var modal = $('.modal.' + clase);

    modal
        .animate(
            { opacity: 0 },
            { queue: false, duration: 'slow' }
        )
        .fadeOut();

        // se recarga la pagina para mostrar el nuevo item creado
        if (item_creado) {
            recargar_pagina();
        } else {
            $('#nombre-nueva-carpeta').val('');
            $('.error-space').text('');
        }
}

function set_background_color()
{
    $('body').css('background-color','#f7f8fa');
}

function actualizar_carpetas_en_select()
{
    var curso_id_elegido = null;
    if (carpeta_id != null) {
        curso_id_elegido = curso_id;
    } else {
        curso_id_elegido = $('#select-group').val();
    }

    var div_folder = $('#div-folder-' + curso_id_elegido);

    $('.select-arrow.cursos').hide();
    $('#label-carpeta').parent().show();
    $('#label-carpeta').show();
    div_folder.show();

    if (carpeta_id != null) {
        $('#select-folder-' + curso_id).val(carpeta_id);

        // Se ponen en null para no interferir en caso de que el usuario quiera editar el curso o carpeta
        curso_id = null;
        carpeta_id = null;
    }
}

function actualizacion_tipo_actividad()
{
    var tipo_actividad = Number($('#select-tipo-actividad').val());

    // si es andamiaje
    if (tipo_actividad == 3) {
        $('.con-andamiaje').fadeIn();
    } else {
        $('.con-andamiaje').fadeOut();
    }

    switch(tipo_actividad) {
        case 0:
            consigna_type = 1;
            break;
        case 1:
            consigna_type = 2;
            break;
        case 2:
            consigna_type = 3;
            break;
        case 3:
            consigna_type = 5;
            break;
        default:
            consigna_type = null;
    }

    if (consigna_type != null && !$('.sentence-ok').is(":visible")) {
        $('#input-oracion').fadeIn();
        $('#guardar-oracion').fadeIn();
    } else {
        $('#input-oracion').fadeOut();
        $('#guardar-oracion').fadeOut();
    }

    deseleccionar_palabras();

    if ($('#input-oracion').val().trim() != "") {
        init_seleccion_multiples_palabras();
    }
}

function guardar_nuevo_ejercicio()
{
    var hubo_error = false;
    var nombre_ejercicio = $('#nombre-ejercicio').val().trim();
    var curso_id = $('#select-group').val();
    var carpeta_id = null;
    if (curso_id != null) {
        carpeta_id = $('#select-folder-' + curso_id).val();
    }
    var oracion = $('#input-oracion').val().trim();
    //var categorias = $('#select-categorias').val();

    if (nombre_ejercicio == "") {
        $('#error-nombre-ejercicio').text('Este campo es obligatorio');
        $('#error-nombre-ejercicio').show();
        hubo_error = true;
    } else {
        $('#error-nombre-ejercicio').hide();
    }

    if (curso_id == null) {
        $('#error-select-group').show();
        hubo_error = true;
    } else {
        $('#error-select-group').hide();
    }

    if (carpeta_id == null) {
        $('#error-select-carpetas').show();
        hubo_error = true;
    } else {
        $('#error-select-carpetas').hide();
    }

    /*if (categorias == null) {
        $('#error-select-categorias').show();
        hubo_error = true;
    } else {
        $('#error-select-categorias').hide();
    }*/

    if (oracion == "") {
        $('#error-oracion').text('Tenés que ingresar la oración a analizar antes de guardar el ejercicio.');
        $('#error-oracion').show();
        hubo_error = true;
    }

    if (oracion != "") {
        $('#error-oracion').hide();
    }

    var mensaje_error_datos = 'No se pudo guardar el ejercicio. Por favor verificá la información ingresada.';

    if (hubo_error) {
        mostrar_error_carga_ejercicio(mensaje_error_datos);
    } else {
        // se guarda el ejercicio nuevo
        var respuestas_a_enviar;
        if (consigna_type == 5) {
            respuestas_a_enviar = JSON.stringify(andamiajes);
        } else {
            respuestas_a_enviar = JSON.stringify(respuestas_parciales);
        }

        $.post("/ejercicios/nuevo/crear/",
            {
                "nombre_ejercicio": nombre_ejercicio,
                "curso_id": curso_id,
                "carpeta_id": carpeta_id,
                //"categorias": JSON.stringify(categorias),
                "oracion": oracion,
                "respuestas": respuestas_a_enviar,
                "ejercicio_id": ejercicio_id
            })
            .done(function(data) {
                var parsed_data = JSON.parse(data);
                ejercicio_id = parsed_data['ejercicio_id'];

                $('#boton-ver-ejercicio').show();

                var mensaje = $('#alert-space');
                mensaje.text('El ejercicio fue guardado con éxito');
                mensaje.attr('class', 'alert-space');
                mensaje.show();

                enable_boton_agregar_consigna(ejercicio_id);

                window.setTimeout(function() { mensaje.fadeOut(); }, 5000);
            })
            .fail(function(data) {
                try {
                    var parsed_data = JSON.parse(data.responseText);
                    var error_nombre_ejercicio = parsed_data['error'];
                    $('#error-nombre-ejercicio').text(error_nombre_ejercicio);
                } catch(err) {
                    $('#error-nombre-ejercicio').text('Error inesperado: no se pudo guardar el ejercicio.');
                }

                $('#error-nombre-ejercicio').show();

                if (data.status == 400) {
                    mostrar_error_carga_ejercicio(mensaje_error_datos);
                } else {
                    mostrar_error_carga_ejercicio('Se produjo un error al crear el ejercicio, volvé a intentar más tarde.');
                }
            });
    }
}

function mostrar_error_carga_ejercicio(mensaje)
{
    var error = $('#alert-space');
    error.text(mensaje);
    error.attr('class', 'alert-space error');
    error.show();
    window.setTimeout(function() { error.fadeOut(); }, 5000);
}

function editar_oracion()
{
    deseleccionar_palabras();
    respuestas_parciales = new Array();
    $('#input-oracion').show();
    $('#guardar-oracion').show();
    $('.sentence-ok').hide();
}

function guardar_oracion(oracion)
{
    oracion = typeof oracion !== 'undefined' ? oracion : $('#input-oracion').val();

    if (oracion != '') {
        $('#input-oracion').hide();
    }
    $('#guardar-oracion').hide();
    $('#sentence-display').text(oracion);
    $('.sentence-ok').show();
    $('#error-oracion').hide();

    setup_divs_respuestas(oracion, false);
    init_seleccion_multiples_palabras();
}

function ver_ejercicio()
{
    window.location = '/ejercicios/general/' + ejercicio_id + '/';
}

function crear_curso()
{
    var nombre_curso = $('#nombre-nuevo-curso').val().trim();

    if (nombre_curso == "") {
        $('.error-space').attr('class', 'error-space error-visible');
        $('.error-space').text('El nombre del curso no puede estar vacío.');
    } else {
        $('.error-space').text('');

        $.post("/docente/cursos/crear/",
            {
                "nombre": nombre_curso
            })
            .done(function(data) {
                var parsed_data = JSON.parse(data);
                var curso_code = parsed_data['curso_code'];

                $('.modal-content').css('height', '320px');

                $('.error-space').attr('class', 'error-space');
                $('.error-space').html('Curso creado correctamente.<br/>Informale a tus alumnos el siguiente código para que se puedan unir al nuevo curso: ' + curso_code);
                $('.error-space').css('height', '75px');

                $('#boton-crear-curso').hide();
                $('#boton-crear-curso-cerrar').show();

                item_creado = true;
            })
            .fail(function(data) {
                var parsed_data = JSON.parse(data.responseText);
                var error = 'Se produjo un error al intentar crear el curso, volvé a intentar por favor.';
                try {
                    error = parsed_data['error'];
                } catch(err) {}

                $('.error-space').attr('class', 'error-space error-visible');
                $('.error-space').text(error);
            });
    }
}

function crear_carpeta(curso_id)
{
    var nombre_carpeta = $('#nombre-nueva-carpeta').val().trim();

    if (nombre_carpeta == "") {
        $('.error-space').attr('class', 'error-space error-visible');
        $('.error-space').text('El nombre de la carpeta no puede estar vacío.');
    } else {
        $('.error-space').text('');

        $.post("/docente/cursos/carpetas/crear/",
            {
                "nombre": nombre_carpeta,
                "curso_id": curso_id
            })
            .done(function(data) {
                item_creado = true;
                close_modal_crear_carpeta();
            })
            .fail(function(data) {
                var parsed_data = JSON.parse(data.responseText);
                var error = 'Se produjo un error al intentar crear el curso, volvé a intentar por favor.';
                try {
                    error = parsed_data['error'];
                } catch(err) {}

                $('.error-space').attr('class', 'error-space error-visible');
                $('.error-space').text(error);
            });
    }
}

function recargar_pagina()
{
    window.location.reload();
}

function volver_a_carpeta(carpeta_id)
{
    if (carpeta_id != null) {
        window.location = '/docente/cursos/carpetas/ejercicios/' + carpeta_id + '/';
    }
}

function init_select_group_carga_ejercicio(curso_id)
{
    $('#select-group').val(curso_id);
    actualizar_carpetas_en_select();
}

function init_borrar_curso(curso_id, nombre_curso)
{
    $('#boton-borrar-curso-' + curso_id).confirm({
        text: '¿Estás seguro de que querés borrar el curso "' + nombre_curso + '"?',
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/docente/cursos/borrar/",
                {
                    "curso_id": curso_id
                })
                .done(function(data) {
                    $('#li-' + curso_id).remove();
                })
                .fail(function(data) {
                    $('.error-space').text('Error al borrar el curso, volvé a intentar más tarde.');
                    $('.error-space').show();
                    window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
                });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function init_borrar_estudiante(user_id, fullname, curso_name, curso_id)
{
    $('#boton-borrar-estudiante-' + user_id).confirm({
        text: '¿Estás seguro de que querés borrar a ' + fullname + ' del curso "' + curso_name + '"?',
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/docente/cursos/estudiantes/borrar/",
                {
                    "user_id": user_id,
                    "curso_id": curso_id
                })
                .done(function(data) {
                    $('#li-' + user_id).remove();
                })
                .fail(function(data) {
                    $('.error-space').text('Error al borrar al estudiante de este curso, volvé a intentar más tarde.');
                    $('.error-space').show();
                    window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
                });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function init_borrar_todos_estudiantes(curso_name, curso_id)
{
    $('.estudiantes-columna-borrar').confirm({
        text: '¿Estás seguro de que querés borrar a todos los estudiantes del curso "' + curso_name + '"?',
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/docente/cursos/estudiantes/borrartodos/",
                {
                    "curso_id": curso_id
                })
                .done(function(data) {
                    window.location.reload();
                })
                .fail(function(data) {
                    $('.error-space').text('Error al borrar a todos los estudiantes de este curso, volvé a intentar más tarde.');
                    $('.error-space').show();
                    window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
                });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function init_borrar_carpeta(carpeta_id, nombre_carpeta)
{
    $('#boton-borrar-carpeta-' + carpeta_id).confirm({
        text: '¿Estás seguro de que querés borrar la carpeta "' + nombre_carpeta + '"?',
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/docente/cursos/carpetas/borrar/",
                {
                    "carpeta_id": carpeta_id
                })
                .done(function(data) {
                    $('#li-' + carpeta_id).remove();
                })
                .fail(function(data) {
                    $('.error-space').text('Error al borrar la carpeta, volvé a intentar más tarde.');
                    $('.error-space').show();
                    window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
                });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function init_borrar_ejercicio(ejercicio_id, nombre_ejercicio)
{
    $('#boton-borrar-ejercicio-' + ejercicio_id).confirm({
        text: '¿Estás seguro de que querés borrar el ejercicio "' + nombre_ejercicio + '"?',
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/docente/cursos/carpetas/ejercicios/borrar/",
                {
                    "ejercicio_id": ejercicio_id
                })
                .done(function(data) {
                    $('#li-' + ejercicio_id).remove();
                })
                .fail(function(data) {
                    $('.error-space').text('Error al borrar el ejercicio, volvé a intentar más tarde.');
                    $('.error-space').show();
                    window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
                });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function editar_curso()
{
    var nombre_editado = $('#nombre-curso').val().trim();

    if (nombre_editado == "") {
        $('#error-general').text('El nombre del curso no puede estar vacío.');
        $('#error-general').show();
        window.setTimeout(function() { $('#error-general').fadeOut(); }, 5000);
    } else {
        $.post("/docente/cursos/editar/",
            {
                "nombre": nombre_editado,
                "curso_id": curso_editado_id
            })
            .done(function(data) {
                $('#curso-nombre-' + curso_editado_id).text(nombre_editado);

                var curso_id = null;
                $('.sprite.edit').each(function() {
                    curso_id = Number($(this).attr('curso_id'));

                    if (curso_id == curso_editado_id) {
                        $(this).attr('curso_nombre', nombre_editado);
                        return false;;
                    }
                });
            })
            .fail(function(data) {
                $('.error-space').text('Error al editar el curso, volvé a intentar más tarde.');
                $('.error-space').show();
                window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
            });
    }
}

function editar_carpeta()
{
    var nombre_editado = $('#nombre-carpeta').val().trim();

    if (nombre_editado == "") {
        $('#error-general').text('El nombre de la carpeta no puede estar vacío.');
        $('#error-general').show();
        window.setTimeout(function() { $('#error-general').fadeOut(); }, 5000);
    } else {
        $.post("/docente/cursos/carpetas/editar/",
            {
                "nombre": nombre_editado,
                "carpeta_id": carpeta_editada_id
            })
            .done(function(data) {
                $('#carpeta-nombre-' + carpeta_editada_id).text(nombre_editado);

                var carpeta_id = null;
                $('.sprite.edit').each(function() {
                    carpeta_id = Number($(this).attr('carpeta_id'));

                    if (carpeta_id == carpeta_editada_id) {
                        $(this).attr('carpeta_nombre', nombre_editado);
                        return false;;
                    }
                });
            })
            .fail(function(data) {
                $('.error-space').text('Error al editar la carpeta, volvé a intentar más tarde.');
                $('.error-space').show();
                window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
            });
    }
}

function copiar_carpeta(carpeta_a_copiar_id, carpeta_a_copiar_nombre)
{
    var curso_donde_copiar_id = Number($('#curso-donde-copiar').val().trim());
    var curso_id = Number($('#curso_id').attr('curso_id'));

    if (curso_donde_copiar_id == curso_id) {
        $('#error-general').text('El curso a donde copiar la carpeta debe ser diferente al curso de origen.');
        $('#error-general').show();
        window.setTimeout(function() { $('#error-general').fadeOut(); }, 5000);
    } else {
        $.post("/docente/cursos/carpetas/copiar/",
            {
                "curso_id": curso_donde_copiar_id,
                "carpeta_id": carpeta_a_copiar_id
            })
            .done(function(data) {
                $('.alert-space').text('Carpeta copiada correctamente.');
                $('.alert-space').show();
                window.setTimeout(function() { $('.alert-space').fadeOut(); }, 5000);
            })
            .fail(function(data) {
                $('.error-space').text('Error al copiar la carpeta, volvé a intentar más tarde.');
                $('.error-space').show();
                window.setTimeout(function() { $('.error-space').fadeOut(); }, 5000);
            });
    }
}

function consigna_type_to_tipo_actividad(consigna_type)
{
    switch(consigna_type) {
        case 1:
            tipo_actividad = 0;
            break;
        case 2:
            tipo_actividad = 1;
            break;
        case 3:
            tipo_actividad = 2;
            break;
        case 5:
            tipo_actividad = 3;
            $('.con-andamiaje').show();
            break;
        default:
            tipo_actividad = '';
    }
    return tipo_actividad;
}

function init_edicion_ejercicio(consigna_type, oracion, respuestas_parciales, consigna)
{
    $(document).on('setup_divs_respuestas', function(event, primera_vez, respuestas, parsed_data) {
        respuestas = typeof respuestas !== 'undefined' ? respuestas : new Array();
        parsed_data = typeof parsed_data !== 'undefined' ? parsed_data : {'palabras_incorrectas': new Array(), 'faltan_respuestas': false, 'error_oracion': ''};

        if (respuestas_parciales.length == 0 && respuestas.length > 0) {
            respuestas_parciales = respuestas;
        }

        console.log('init_edicion_ejercicio');

        // tipo actividad
        var tipo_actividad = consigna_type_to_tipo_actividad(consigna_type);

        // categorias
        $("#select-categorias").multipleSelect("setSelects", categorias_guardadas);

        $('#select-tipo-actividad').val(tipo_actividad);

        // consigna
        $('#consigna-1').val(unquote_html_entities(consigna));

        // oracion
        $('#input-oracion').val(oracion);

        if (primera_vez) {
            guardar_oracion(oracion);
        }

        // se muestran las respuestas
        var index;
        var posiciones_palabras;
        var opcion;

        //respuestas_parciales = [{"opcion":"401","posiciones_palabras":["0"]},{"opcion":"401","posiciones_palabras":["1"]},{"opcion":"401","posiciones_palabras":["2"]},{"opcion":"401","posiciones_palabras":["3"]},{"opcion":"401","posiciones_palabras":["4"]}]

        var respuestas_parciales_length = respuestas_parciales.length;

        for (index = 0; index < respuestas_parciales_length; index++) {
            posiciones_palabras = respuestas_parciales[index]['posiciones_palabras'];
            opcion = respuestas_parciales[index]['opcion'];

            // se seleccionan las palabras para poder colocarles las respuestas
            $("#exercise-content p .word").each(function (i) {
                // a veces la posicion viene como entero y otras veces viene como string
                if ((posiciones_palabras.indexOf(String(i)) != -1) || (posiciones_palabras.indexOf(i) != -1)) {
                    $(this).toggleClass('word-selected');
                }
            });

            // se llama a la funcion que guarda la respuesta para renderizarla
            switch(consigna_type) {
                case 1:
                    // se aplanan las respuestas por simplicidad en su procesamiento
                    for (var i=0; i<posiciones_palabras.length; i++) {
                        var temp_array = new Array();
                        posicion_palabra = posiciones_palabras[i];
                        temp_array.push(posicion_palabra);
                        guardar_respuesta_parcial_clase_de_palabra(temp_array, opcion);
                    }
                    break;
                case 2:
                    guardar_respuesta_parcial_analisis_morfologico(posiciones_palabras, opcion);
                    break;
                case 3:
                case 5:
                    guardar_respuesta_parcial_analisis_sintactico(posiciones_palabras, opcion);
                    break;
                default:
                    break;
            }

            deseleccionar_palabras();
        }

        init_click_palabra();

        init_seleccion_multiples_palabras();
        marcar_en_rojo_respuestas_incorrectas(consigna_type, parsed_data, false);
    });
}

function init_click_palabra()
{
    // inicializacion del evento de clic sobre una palabra para poder guardar las respuestas parciales
    var toSelect = $('.cd-accordion-menu a');

    if (on_click_toolbar_option_called == false) {
        toSelect.click(function() {
            var thisone = $(this);

            if (!thisone.hasClass('toolbar-selected')){
                toSelect.removeClass('toolbar-selected');
                thisone.addClass('toolbar-selected');
                setTimeout(function(){ thisone.removeClass('toolbar-selected'); }, 150);
            }

            var posiciones_palabras = new Array();
            $("#exercise-content p .word-selected span").each(function() {
                posiciones_palabras.push($(this).attr('posicion'));
            });
            posiciones_palabras = $.unique(posiciones_palabras);
            var opcion = $('.toolbar-selected').attr('toolbar-id');

            switch(consigna_type) {
                case 1:
                    // se aplanan las respuestas por simplicidad en su procesamiento
                    for (var i=0; i<posiciones_palabras.length; i++) {
                        var temp_array = new Array();
                        posicion_palabra = posiciones_palabras[i];
                        temp_array.push(posicion_palabra);
                        guardar_respuesta_parcial_clase_de_palabra(temp_array, opcion);
                    }
                    break;
                case 2:
                    guardar_respuesta_parcial_analisis_morfologico(posiciones_palabras, opcion);
                    break;
                case 3:
                case 5:
                    guardar_respuesta_parcial_analisis_sintactico(posiciones_palabras, opcion);
                    break;
                default:
                    break;
            }
        });
    }

    on_click_toolbar_option_called = true;
}

function unirme_materia()
{
    var codigo_materia = $('#curso-code').val().trim();

    if (codigo_materia != "") {
        $('.error-space.error-visible').text('');

        $.post("/estudiante/materias/unirme/",
            {
                "curso_code": codigo_materia,
            })
            .done(function(data) {
                recargar_pagina();
            })
            .fail(function(data) {
                if (data.status == 404) {
                    $('.error-space.error-visible').text('El código ingresado no corresponde a ninguna materia.');
                } else {
                    $('.error-space.error-visible').text('Error al unirse a la materia, volvé a intentar más tarde.');
                }
            });
    } else {
        $('.error-space.error-visible').text('El código de materia está vacío.');
    }
}

function agregar_andamiaje()
{
    var mensaje = $('#mensaje-andamiaje').val().trim();
    var error = $('#error-andamiaje');

    if (mensaje.length == 0) {
        error.text('El mensaje del andamiaje no puede estar vacío.');
        error.show();
    } else {
        if (respuestas_parciales.length == 0) {
            error.text('El andamiaje debe tener una respuesta en la oración.');
            error.show();
        } else {
            /*if (respuestas_parciales.length > 1) {
                error.text('Por cada andamiaje debe haber una única respuesta en la oración.');
                error.show();
            } else {*/
                error.fadeOut();

                var respuesta = respuestas_parciales[0];

                var correcta = $('.switch-button-label.on').text() == "Correcta";
                var opcion = respuesta['opcion'];
                var posiciones_palabras = respuesta['posiciones_palabras'];
                var andamiaje_id = 'N-' + andamiaje_id_counter;

                var andamiaje = {
                    'mensaje': mensaje,
                    'correcta': correcta,
                    'posiciones_palabras': posiciones_palabras,
                    'opcion': opcion,
                    'andamiaje_id': andamiaje_id
                };
                andamiajes.push(andamiaje);

                var palabras = new Array();
                var posicion;
                $('.word > span').each(function() {
                    posicion = $(this).attr('posicion');
                    if (posiciones_palabras.indexOf(posicion) != -1) {
                        palabras.push($(this).text());
                    }
                });

                if (correcta && !hay_andamiaje_respuesta_correcta) {
                    hay_andamiaje_respuesta_correcta = true;
                }

                var abrev_opcion = get_abreviatura(opcion);
                var ul_class = correcta ? 'correct' : 'incorrect';
                var new_ul = '<ul class="andamiajes ' + ul_class + '" id="' + andamiaje_id + '">';
                new_ul += '<li><h1>' + mensaje + '</h1><h2>' + abrev_opcion + '</h2>';
                new_ul += '<span>' + palabras.join(' ') + '</span><div class="sprite erase-andamiaje" ';
                new_ul += 'onclick="borrar_nuevo_andamiaje(\'' + andamiaje_id + '\');"></div></li></ul>';
                $('#andamiajes').append(new_ul);

                // se resetean los widgets
                $('#mensaje-andamiaje').val('');
                if (correcta) {
                    $('.switch-button-background').click();
                }

                // se borran las respuestas de la oracion
                $('.icon-bin').click();
                deseleccionar_palabras();

                andamiaje_id_counter++;
            //}
        }
    }
}

function __borrar_andamiaje(andamiaje_id)
{
    for (var i=0; i<andamiajes.length; i++) {
        if (andamiajes[i]['andamiaje_id'] == andamiaje_id) {
            if (andamiajes[i]['correcta']) {
                hay_andamiaje_respuesta_correcta = false;
            }
            andamiajes.splice(i, 1);
            break;
        }
    }
    $('#' + andamiaje_id).remove();
}

function borrar_nuevo_andamiaje(andamiaje_id)
{
    __borrar_andamiaje(andamiaje_id);
}

function borrar_andamiaje_existente(andamiaje_id)
{
    __borrar_andamiaje(andamiaje_id);
}

function guardar_consigna(carpeta_id, ejercicio_id, numero_consigna)
{
    var tipo_actividad = consigna_type_to_tipo_actividad(consigna_type);
    var texto_consigna = $('#consigna-1').val();
    var hubo_error = false;

    var respuestas = null;
    if (consigna_type == 5) {
        respuestas = andamiajes;
    } else {
        respuestas = respuestas_parciales;
    }

    if (tipo_actividad < 0) {
        tipo_actividad = $('#select-tipo-actividad').val();
    }

    if (texto_consigna == "") {
        $('#error-consigna').show();
        window.setTimeout(function() { $('#error-consigna').fadeOut(); }, 3000);
        hubo_error = true;
    } else {
        $('#error-consigna').hide();
    }

    if (tipo_actividad == null) {
        $('#error-select-tipo-actividad').show();
        window.setTimeout(function() { $('#error-select-tipo-actividad').fadeOut(); }, 3000);
        hubo_error = true;
    } else {
        $('#error-select-tipo-actividad').hide();
    }

    if (respuestas.length == 0 && tipo_actividad != null && tipo_actividad != 3) {
        $('#error-oracion').text('Tenés que ingresar al menos una respuesta sobre la oración antes de guardar el ejercicio.');
        $('#error-oracion').show();
        hubo_error = true;
    } else {
        $('#error-oracion').hide();
    }

    var error_andamiaje = $('#error-andamiaje');
    if (tipo_actividad == 3 && andamiajes.length == 0) {
        error_andamiaje.text('No ingresaste ningún andamiaje.');
        error_andamiaje.show();
        hubo_error = true;
    } else {
        error_andamiaje.hide();
    }

    if (tipo_actividad == 3 && andamiajes.length > 0 && hay_andamiaje_respuesta_correcta == false) {
        error_andamiaje.text('No ingresaste ningún andamiaje para la respuesta correcta.');
        error_andamiaje.show();
        hubo_error = true;
    } else {
        error_andamiaje.hide();
    }

    if (!hubo_error)
    {
        // se guarda la consigna
        $.post("/ejercicios/nuevo/guardar_consigna/",
        {
            "ejercicio_id": ejercicio_id,
            "numero_consigna": numero_consigna,
            "texto_consigna": texto_consigna,
            "tipo_actividad": tipo_actividad,
            "respuestas": JSON.stringify(respuestas)
        })
        .done(function(data) {
            // se hace redirect a la pagina del ejercicio
            window.location = '/ejercicios/nuevo/' + carpeta_id + '/' + ejercicio_id + '/';
        })
        .fail(function(data) {
            $('.error-space.error-visible').text('Error al guardar la consigna, volvé a intentar más tarde.');
        });
    }
}

function registrar_borrar_consigna(ejercicio_id, numero_consigna)
{
    $('#boton-borrar-consigna-' + ejercicio_id + '-' + numero_consigna).confirm({
        text: "¿Estás seguro de que querés borrar esta consigna?",
        title: "Confirmación requerida",
        confirm: function(button) {
            $.post("/ejercicios/nuevo/borrar_consigna/",
            {
                "ejercicio_id": ejercicio_id,
                "numero_consigna": numero_consigna
            })
            .done(function(data) {
                /*$('#tr-' + ejercicio_id + '-' + numero_consigna).remove();

                var mensaje = $('#alert-space');
                mensaje.text('Se borró la consigna');
                mensaje.attr('class', 'alert-space');
                mensaje.show();
                window.setTimeout(function() { mensaje.fadeOut(); }, 5000);*/

                recargar_pagina();
            })
            .fail(function(data) {
                $('.error-space.error-visible').text('Error al borrar la consigna, volvé a intentar más tarde.');
            });
        },
        cancel: function(button) {
            // nothing to do
        },
        confirmButton: "Sí",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });
}

function publicar_u_ocultar(carpeta_id)
{
    $.post("/docente/cursos/carpetas/visibilidad/",
    {
        "carpeta_id": carpeta_id
    })
    .done(function(data) {
        // Boton se pone gris al ocultar, y se colorea al volver a cliquearlo.
        var selector = '#boton-mostrar-carpeta-' + String(carpeta_id);
        var current_class = $(selector).attr('class');
        var new_class;
        var new_title;

        if (current_class == "sprite see-it") {
            new_class = "sprite see-it disabled";
            new_title = 'Publicar';
        } else {
            new_class = "sprite see-it";
            new_title = 'Ocultar';
        }

        $(selector).attr('class', new_class);
        $(selector).attr('title', new_title);
    })
    .fail(function(data) {
        $('.error-space.error-visible').text('Error al cambiar la visibilidad de la carpeta, volvé a intentar más tarde.');
    });

}

function enable_boton_agregar_consigna(ejercicio_id)
{
    var href = '/ejercicios/nuevo/crear/consignas/' + ejercicio_id + '/';
    $('#link-boton-agregar-consigna').attr('href', href);

    $('#boton-agregar-consigna').removeAttr('disabled');
    $('#boton-agregar-consigna').css('background', '#4687fb');
}

function disable_boton_agregar_consigna()
{
    $('#boton-agregar-consigna').attr('disabled', 'true');
    $('#boton-agregar-consigna').css('background', 'lightgray');
}
