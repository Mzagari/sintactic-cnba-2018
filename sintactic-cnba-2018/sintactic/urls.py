"""sintactic URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from sintactic import views
from allauth.socialaccount.views import signup
from django.contrib.auth.views import logout


admin.autodiscover()

urlpatterns = [
    # allauth
    url(r'^accounts/logout/$', logout, {'next_page': '/'}),
    url(r'^accounts/social/signup/$', signup, name='socialaccount_signup', kwargs={'next_page': '/accounts/social/signup_check/'}),
    url(r'^accounts/', include('allauth.urls')),

    # exercise
    url(r'^ejercicios/general/(?P<ejercicio_id>\d+)/(?P<numero_consigna_actual>\d+)/$', views.ejercicio_general, name='ejercicio_general'),
    url(r'^ejercicios/general/siguiente/$', views.ejercicio_general_siguiente, name='ejercicio_general_siguiente'),
    url(r'^ejercicios/con_andamiaje/validar_respuesta/$', views.ejercicio_con_andamiaje, name='ejercicio_con_andamiaje'),
    url(r'^ejercicios/nuevo/(?P<carpeta_id>\d+)/(?P<ejercicio_id>\d+)/$', views.ejercicio_nuevo, name='ejercicio_nuevo'),
    url(r'^ejercicios/nuevo/(?P<carpeta_id>\d+)/$', views.ejercicio_nuevo, name='ejercicio_nuevo'),
    url(r'^ejercicios/nuevo/$', views.ejercicio_nuevo, name='ejercicio_nuevo'),
    url(r'^ejercicios/nuevo/crear/$', views.web_service_ejercicio_nuevo, name='web_service_ejercicio_nuevo'),
    url(r'^ejercicios/nuevo/crear/consignas/(?P<ejercicio_id>\d+)/$', views.nueva_consigna, name='nueva_consigna'),
    url(r'^ejercicios/nuevo/editar/consignas/(?P<ejercicio_id>\d+)/(?P<numero_consigna>\d+)/$', views.editar_consigna, name='editar_consigna'),
    url(r'^ejercicios/nuevo/guardar_consigna/$', views.web_service_guardar_consigna, name='web_service_guardar_consigna'),
    url(r'^ejercicios/nuevo/borrar_consigna/$', views.web_service_borrar_consigna, name='web_service_borrar_consigna'),
    url(r'^ejercicios/corregir/(?P<carpeta_id>\d+)/(?P<ejercicio_id>\d+)/(?P<estudiante_id>\d+)/(?P<numero_consigna_anterior>\d+)/$', views.corregir_ejercicio, name='corregir_ejercicio'),
    url(r'^ejercicios/corregir/(?P<carpeta_id>\d+)/(?P<ejercicio_id>\d+)/$', views.corregir_ejercicios, name='corregir_ejercicios'),

    # home
    url(r'^$', views.home, name='home'),

    # usuarios
    url(r'^mi_perfil$', views.mi_perfil, name='mi_perfil'),

    # docente
    url(r'^docente/cursos/carpetas/ejercicios/(?P<carpeta_id>\d+)/$', views.docente_ejercicios, name='docente_ejercicios'),
    url(r'^docente/cursos/carpetas/ejercicios/borrar/$', views.web_service_borrar_ejercicio, name='web_service_borrar_ejercicio'),
    url(r'^docente/cursos/carpetas/crear/$', views.web_service_carpeta_nueva, name='web_service_carpeta_nueva'),
    url(r'^docente/cursos/carpetas/editar/$', views.web_service_editar_carpeta, name='web_service_editar_carpeta'),
    url(r'^docente/cursos/carpetas/copiar/$', views.web_service_copiar_carpeta, name='web_service_copiar_carpeta'),
    url(r'^docente/cursos/carpetas/borrar/$', views.web_service_borrar_carpeta, name='web_service_borrar_carpeta'),
    url(r'^docente/cursos/carpetas/visibilidad/$', views.docente_visibilidad_carpeta, name='docente_visibilidad_carpeta'),
    url(r'^docente/cursos/carpetas/(?P<curso_id>\d+)/$', views.docente_carpetas, name='docente_carpetas'),
    url(r'^docente/cursos/crear/$', views.web_service_curso_nuevo, name='web_service_curso_nuevo'),
    url(r'^docente/cursos/borrar/$', views.web_service_borrar_curso, name='web_service_borrar_curso'),
    url(r'^docente/cursos/editar/$', views.web_service_editar_curso, name='web_service_editar_curso'),
    url(r'^docente/cursos/$', views.docente_cursos, name='docente_cursos'),
    url(r'^docente/cursos/estudiantes/(?P<curso_id>\d+)/$', views.docente_ver_estudiantes_curso, name='docente_ver_estudiantes_curso'),
    url(r'^docente/cursos/estudiantes/borrar/$', views.web_service_borrar_estudiante_de_curso, name='web_service_borrar_estudiante_de_curso'),
    url(r'^docente/cursos/estudiantes/borrartodos/$', views.web_service_borrar_todos_estudiantes_de_curso, name='web_service_borrar_todos_estudiantes_de_curso'),

    # estudiante
    url(r'^estudiante/materias/carpetas/ejercicios/(?P<carpeta_id>\d+)/$', views.estudiante_ejercicios, name='estudiante_ejercicios'),
    url(r'^estudiante/materias/carpetas/(?P<curso_id>\d+)/$', views.estudiante_carpetas, name='estudiante_carpetas'),
    url(r'^estudiante/materias/$', views.estudiante_cursos, name='estudiante_cursos'),
    url(r'^estudiante/materias/unirme/$', views.estudiante_unirme_a_curso, name='estudiante_unirme_a_curso'),

    # otros
    url(r'^terminos_y_condiciones/$', views.terminos_y_condiciones, name='terminos_y_condiciones'),

    url(r'^admin/', include(admin.site.urls)),

    # testing
    url(r'^emailtesting/(?P<email_path>.+)/$', views.email_testing, name='email_testing'),
    url(r'^error_500_test/', views.error_500_test, name='error_500_test'),

]
