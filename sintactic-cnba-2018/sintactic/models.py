# -*- coding: utf-8 -*-

from django.db import models, IntegrityError
from django.contrib.auth.models import User
from cloudinary.models import CloudinaryField
from copy import deepcopy


class PalabraVacia(Exception):
    pass


class TipoActividadInvalida(Exception):
    pass


class NombreRepetido(Exception):
    pass


class RespuestaCorrectaAndamiajeNoEncontrada(Exception):
    pass


class TipoDesconocidoDeRespuesta(Exception):
    pass


class Usuario(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    avatar = CloudinaryField('image', null=True, blank=True, max_length=255)
    user = models.OneToOneField(User, related_name='usuario')
    es_docente = models.BooleanField(default=False)

    @property
    def email(self):
        return self.user.email

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def fullname(self):
        return u'{} {}'.format(self.first_name, self.last_name)

    @property
    def last_login(self):
        return self.user.last_login

    def __str__(self):
        return u"{} <{}>".format(self.fullname, self.email)

    @staticmethod
    def email_de_docente(email):
        emails_docentes = ['gacpro@gmail.com',
                           'gacpro+docente@gmail.com',
                           'marianoduna@gmail.com',
                           'nogueirasylvia@gmail.com']
        return email in emails_docentes

    def crear_curso(self, nombre):
        if self.es_docente:
            try:
                curso = Curso(nombre=nombre, profesor=self)
                curso.save()
            except IntegrityError:
                raise NombreRepetido()
            return curso
        return None

    def crear_carpeta(self, nombre, curso):
        if self.es_docente:
            try:
                carpeta = Carpeta(nombre=nombre, curso=curso)
                carpeta.save()
            except IntegrityError:
                raise NombreRepetido()
            return carpeta
        return None

    @property
    def cursos(self):
        if self.es_docente:
            return Curso.objects.filter(profesor=self)
        else:
            return Curso.objects.filter(alumnos__id=self.id)

    @property
    def avatar_url(self):
        if self.avatar:
            url = self.avatar.url
            url = url.replace('upload/', 'upload/c_fill,g_west,h_200,w_200/')
            return url

        # default avatar
        return "http://res.cloudinary.com/dvbxfq8qm/image/upload/v1454339411/system/default-profile-picture_ge45iu.png"

    def responder_consigna(self, consigna, respuestas, sobreescribir=True):
        alumno = self

        # se borran las respuestas existentes, si es que hay algo con que reemplazarlas
        if respuestas and sobreescribir:
            respuestas_existentes = consigna.obtener_respuestas_alumno(alumno)
            respuestas_existentes.all().delete()

        # se asocia al alumno a cada respuesta dada
        for respuesta in respuestas:
            respuesta.alumno = alumno
            if sobreescribir:
                respuesta.save()

            respuesta.correcta = consigna.es_correcta(respuesta)
            if sobreescribir:
                respuesta.save()

        return respuestas

    def get_estado_resolucion_ejercicio(self, ejercicio):
        '''
        Posibles estados:
        pending: pendiente
        correct: resuelto correctamente
        wrong: resuelto incorrectamente o de forma parcial
        '''
        estado = 'pending'

        for consigna in ejercicio.consignas:
            if consigna.tipo_respuesta.objects.filter(alumno=self, correcta=False, consigna=consigna).exists():
                estado = 'wrong'
                break
            elif consigna.tipo_respuesta.objects.filter(alumno=self, correcta=True, consigna=consigna).exists():
                estado = 'correct'

        return estado


class Oracion(models.Model):
    string = models.TextField(null=False, max_length=2048)

    class Categorias(object):
        INDEFINIDO, OBS, OBC, OU = range(100, 104)

    opciones_categoria = ((Categorias.INDEFINIDO, 'Indefinida'),
                         (Categorias.OBS, 'Oración Bimembre Simple'),
                         (Categorias.OBC, 'Oración Bimembre Compuesta'),
                         (Categorias.OU,  'Oración Unimembre'))

    categoria = models.PositiveIntegerField(default=Categorias.INDEFINIDO, null=False, blank=True,
                                            choices=opciones_categoria, verbose_name="Categoría de oración")

    def save(self, *args, **kwargs):
        self.string = ' '.join(filter(lambda palabra: len(palabra) > 0, self.string.strip().split(' ')))
        super(Oracion, self).save(*args, **kwargs)

        # se borran las palabras y se vuelven a generar
        Palabra.objects.filter(oracion=self).delete()

        posicion = 0
        for palabra_str in self.string.split(' '):
            palabra = Palabra(oracion=self, string=palabra_str, posicion=posicion)
            try:
                palabra.save()
                posicion += 1
            except PalabraVacia:
                pass

    @property
    def palabras(self):
        return Palabra.objects.filter(oracion=self).order_by('posicion')

    def __str__(self):
        return self.string

    def __unicode__(self):
        return u'{}'.format(self.string)


class CategoriaEjercicio(models.Model):
    nombre = models.CharField(max_length=255, null=False, unique=True)

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return u'{}'.format(self.nombre)


class Curso(models.Model):
    nombre = models.CharField(max_length=50, null=False, blank=False)
    profesor = models.ForeignKey(Usuario, null=False)
    alumnos = models.ManyToManyField(Usuario, related_name='materias')
    code = models.CharField(max_length=4, null=False, blank=False, default='')

    class Meta:
        ordering = ['nombre']
        unique_together = ['profesor', 'nombre']

    def __str__(self):
        return u'{} (docente {})'.format(self.nombre, self.profesor.fullname)

    def __unicode__(self):
        return u'{} (docente {})'.format(self.nombre, self.profesor.fullname)

    def save(self, *args, **kwargs):
        super(Curso, self).save(*args, **kwargs)
        if self.code is '':
            self.code = str(self.id).zfill(4)
            super(Curso, self).save(*args, **kwargs)

    @property
    def carpetas(self):
        return Carpeta.objects.filter(curso=self).order_by('nombre')


class Carpeta(models.Model):
    nombre = models.CharField(max_length=50, null=False, blank=False)
    curso = models.ForeignKey(Curso, null=False, related_name='carpetas')
    visible = models.BooleanField(default=False)

    class Meta:
        ordering = ['nombre']
        unique_together = ['curso', 'nombre']

    def __str__(self):
        return u'{} - {}'.format(self.nombre, self.curso.nombre)

    def __unicode__(self):
        return u'{} - {}'.format(self.nombre, self.curso.nombre)

    @property
    def ejercicios(self):
        return Ejercicio.objects.filter(carpeta=self)

    def copiar(self, curso):
        # se copia la carpeta
        carpeta = self
        copia_carpeta = deepcopy(carpeta)
        copia_carpeta.id = None
        copia_carpeta.curso = curso
        copia_carpeta.save()

        # se copian los ejercicios en la carpeta
        for ejercicio in carpeta.ejercicios.all():
            copia_ejercicio = deepcopy(ejercicio)
            copia_ejercicio.id = None
            copia_ejercicio.carpeta = copia_carpeta
            copia_ejercicio.save()

            # se copia la oracion del ejercicio
            copia_oracion = deepcopy(ejercicio.oracion)
            copia_oracion.id = None
            copia_oracion.save()
            copia_ejercicio.oracion = copia_oracion
            copia_ejercicio.save()

            # se copian las consignas del ejercicio
            for consigna in ejercicio.consignas:
                copia_consigna = deepcopy(consigna)
                copia_consigna.id = None
                copia_consigna.ejercicio = copia_ejercicio
                copia_consigna.save()

                # se copian las respuestas haciendo que se establezcan nuevamente en la copia de la consigna
                respuestas_dict = map(lambda respuesta: respuesta.to_dict(), consigna.respuestas.all())
                copia_consigna.guardar_respuestas(respuestas_dict)

        return copia_carpeta

    def publicar(self):
        self.visible = True
        self.save()

    def ocultar(self):
        self.visible = False
        self.save()

    def cambiar_visibilidad(self):
        if self.visible:
            self.ocultar()
        else:
            self.publicar()


class Ejercicio(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    oracion = models.ForeignKey(Oracion, related_name='oracion_ejercicio', null=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    categorias = models.ManyToManyField(CategoriaEjercicio)
    carpeta = models.ForeignKey(Carpeta, related_name='ejercicios', null=False)

    class Meta:
        ordering = ['nombre']
        unique_together = ['carpeta', 'nombre']

    @staticmethod
    def crear(nombre_ejercicio, carpeta, categorias, oracion_string):
        if Ejercicio.objects.filter(carpeta=carpeta, nombre=nombre_ejercicio).exists():
            raise NombreRepetido()

        # se crea la oracion
        oracion = Oracion(string=oracion_string)
        oracion.save()

        # se crea el ejercicio
        ejercicio = Ejercicio(nombre=nombre_ejercicio, oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        # se guardan las categorias
        #Ejercicio.guardar_categorias(ejercicio, categorias)

        return ejercicio

    @staticmethod
    def editar(ejercicio_id, nombre_ejercicio, carpeta, categorias, oracion_string, consigna_string='', tipo_actividad=-1, respuestas=[], numero_consigna=0):
        ejercicio = Ejercicio.objects.get(id=ejercicio_id)

        if ejercicio.nombre != nombre_ejercicio or ejercicio.carpeta != carpeta:
            ejercicio.nombre = nombre_ejercicio
            ejercicio.carpeta = carpeta
            ejercicio.save()

        if tipo_actividad == 0:
            tipo_respuesta = RespuestaClaseDePalabra
            tipo_consigna = ConsignaClaseDePalabra
        elif tipo_actividad == 1:
            tipo_respuesta = RespuestaAnalisisMorfologico
            tipo_consigna = ConsignaAnalisisMorfologico
        elif tipo_actividad == 2:
            tipo_respuesta = RespuestaAnalisisSintactico
            tipo_consigna = ConsignaAnalisisSintactico
        elif tipo_actividad == 3:
            tipo_respuesta = RespuestaAndamiaje
            tipo_consigna = ConsignaConAndamiaje
        else:
            tipo_respuesta = None
            tipo_consigna = None

        # si hay datos de la consigna, se procesa
        if consigna_string != '' and tipo_actividad != -1:
            if numero_consigna == 0:  # nueva consigna
                # se crea la consigna
                consigna = Consigna.crear(ejercicio, consigna_string, tipo_actividad)
            else:  # consigna existente
                consigna = tipo_consigna.objects.get(ejercicio__id=ejercicio_id, numero=numero_consigna)

            if consigna.string != consigna_string:
                consigna.string = consigna_string
                consigna.save()

        if tipo_respuesta:
            # se borran todas las respuestas del ejercicio y se vuelven a generar
            tipo_respuesta.objects.filter(consigna__ejercicio__id=ejercicio_id).delete()

        # si cambia la oracion, se borran y se vuelven a generar las palabras
        if ejercicio.oracion.string != oracion_string:
            ejercicio.oracion.string = oracion_string
            ejercicio.oracion.save()

        # si hay datos de la consigna, se procesa
        if consigna_string != '' and tipo_actividad != -1:
            consigna.guardar_respuestas(respuestas)

        # se guardan las categorias
        Ejercicio.guardar_categorias(ejercicio, categorias)

        return ejercicio

    @staticmethod
    def guardar_categorias(ejercicio, categorias):
        ejercicio.categorias.clear()

        for categoria_id in categorias:
            categoria = CategoriaEjercicio.objects.get(id=categoria_id)
            ejercicio.categorias.add(categoria)
        ejercicio.save()

    def delete(self, *args, **kwargs):
        self.oracion.delete()
        super(Ejercicio, self).delete(*args, **kwargs)

    def es_respuesta_correcta(self):
        '''
        :return: El ejercicio tiene una respuesta correcta cuando fueron respondidas correctamente todas las consignas.
        '''
        for consigna in self.consignas.all():
            if not consigna.es_respuesta_correcta(): # TODO: este metodo lleva parametros, arreglar!
                return False
        return True

    @property
    def consignas(self):
        lista_consignas = []
        for tipo_consigna in Consigna.__subclasses__():
            lista_consignas.extend(tipo_consigna.objects.filter(ejercicio=self).order_by('numero'))
        return sorted(lista_consignas, key=lambda consigna: consigna.numero)


class Palabra(models.Model):
    oracion = models.ForeignKey(Oracion, null=False)
    string = models.TextField(max_length=100, null=False)
    posicion = models.IntegerField(null=False, default=0)

    class Meta:
        ordering = ['posicion']

    def __str__(self):
        return self.string

    def __unicode__(self):
        return u'{}'.format(self.string)

    def save(self, *args, **kwargs):
        self.string = Palabra.remover_signos_de_puntuacion(self.string)
        if len(self.string) == 0:
            raise PalabraVacia()
        super(Palabra, self).save(*args, **kwargs)

    @staticmethod
    def remover_signos_de_puntuacion(string):
        # se sacan de la palabra los signos de puntuacion
        signos_de_puntuacion = ['.', ',', ';', ':', '?', u'¿', u'¡', '!', '(', ')', '...', '-', '"', "'"]

        for signo_de_puntuacion in signos_de_puntuacion:
            try:
                string = string.replace(signo_de_puntuacion, '')
            except UnicodeDecodeError:
                string = string.decode('utf-8')
                string = string.replace(signo_de_puntuacion, '')
                string = string.encode('utf-8')

        return string.strip()


class Respuesta(models.Model):
    '''
    Atributo 'consigna' debe estar en todas las subclases.
    '''

    alumno = models.ForeignKey(Usuario, null=True)
    correcta = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        abstract = True

    def to_dict(self):
        raise NotImplementedError()

    @staticmethod
    def from_dict(consigna, respuestas_list):
        respuestas = list()
        oracion = consigna.ejercicio.oracion
        tipo_respuesta = consigna.tipo_respuesta

        for respuesta_item in respuestas_list:
            posiciones_palabras = respuesta_item['posiciones_palabras']
            posiciones_palabras = map(lambda posicion: int(posicion), posiciones_palabras)
            opcion = int(respuesta_item['opcion'])

            if tipo_respuesta == RespuestaClaseDePalabra or tipo_respuesta == RespuestaAnalisisMorfologico:
                for posicion in posiciones_palabras:
                    palabra = Palabra.objects.get(oracion=oracion, posicion=posicion)
                    respuesta = tipo_respuesta(palabra=palabra, consigna=consigna, opcion=opcion)
                    respuesta.save()
                    respuestas.append(respuesta)
            elif tipo_respuesta == RespuestaAnalisisSintactico or tipo_respuesta == RespuestaAndamiaje:
                palabras = map(lambda posicion: Palabra.objects.get(oracion=oracion, posicion=posicion), posiciones_palabras)
                respuesta = tipo_respuesta(consigna=consigna, opcion=opcion)
                respuesta.save() # generate ID before assigning a many-to-many field
                respuesta.palabras = palabras
                respuesta.save()
                respuestas.append(respuesta)
            else:
                raise Exception('Tipo de respuesta no esperado.')

        return respuestas

    def save(self, *args, **kwargs):
        if self.alumno is None:
            self.correcta = True
        super(Respuesta, self).save(*args, **kwargs)


class RespuestaCategoriaDeOracion(Respuesta):
    # XXX: deprecated
    opcion = models.PositiveIntegerField(default=Oracion.Categorias.INDEFINIDO, null=False, blank=True,
                                            choices=Oracion.opciones_categoria, verbose_name="Categoría de oración")


class Consigna(models.Model):
    ejercicio = models.ForeignKey(Ejercicio, null=True)
    numero = models.IntegerField(null=False, default=1)
    string = models.TextField(null=False, max_length=2048)

    class Meta:
        abstract = True

    def __str__(self):
        return self.string

    def __unicode__(self):
        return u'{}'.format(self.string)

    def es_respuesta_correcta(self, opcion_de_respuesta):
        raise NotImplementedError()

    @staticmethod
    def get_consigna(ejercicio, numero_consigna):
        for consigna_class in Consigna.__subclasses__():
            try:
                return consigna_class.objects.get(ejercicio=ejercicio, numero=numero_consigna)
            except Exception:
                pass
        return None

    @staticmethod
    def crear(ejercicio, texto_consigna, tipo_actividad):
        tipo_actividad = int(tipo_actividad)

        if tipo_actividad == 0:
            consigna_type = ConsignaClaseDePalabra
        elif tipo_actividad == 1:
            consigna_type = ConsignaAnalisisMorfologico
        elif tipo_actividad == 2:
            consigna_type = ConsignaAnalisisSintactico
        elif tipo_actividad == 3:
            consigna_type = ConsignaConAndamiaje
        else:
            raise TipoActividadInvalida('{} no es un tipo valido de actividad'.format(tipo_actividad))

        numero = len(ejercicio.consignas) + 1
        consigna = consigna_type(ejercicio=ejercicio, numero=numero, string=texto_consigna)
        consigna.save()
        return consigna

    @property
    def tipo_respuesta(self):
        if isinstance(self, ConsignaClaseDePalabra):
            tipo_respuesta = RespuestaClaseDePalabra
        elif isinstance(self, ConsignaAnalisisMorfologico):
            tipo_respuesta = RespuestaAnalisisMorfologico
        elif isinstance(self, ConsignaAnalisisSintactico):
            tipo_respuesta = RespuestaAnalisisSintactico
        elif isinstance(self, ConsignaConAndamiaje):
            tipo_respuesta = RespuestaAndamiaje
        elif isinstance(self, RespuestaCategoriaDeOracion):
            tipo_respuesta = ConsignaCategoriaDeOracion
        else:
            raise TipoDesconocidoDeRespuesta('self is instance of {}'.format(type(self)))
        return tipo_respuesta

    @property
    def respuestas(self):
        return self.tipo_respuesta.objects.filter(consigna=self, alumno=None)

    def obtener_respuestas_alumno(self, alumno):
        '''
        Devuelve las respuestas del alumno a esta consigna.
        '''
        return self.tipo_respuesta.objects.filter(alumno=alumno, consigna=self)

    def es_correcta(self, respuesta):
        respuestas_correctas = self.tipo_respuesta.objects.filter(consigna=self, opcion=respuesta.opcion, alumno=None)

        for respuesta_correcta in respuestas_correctas.all():
            if self.tipo_respuesta == RespuestaClaseDePalabra or self.tipo_respuesta == RespuestaAnalisisMorfologico:
                if respuesta_correcta.palabra == respuesta.palabra:
                    return True
            elif self.tipo_respuesta == RespuestaAnalisisSintactico or self.tipo_respuesta == RespuestaAndamiaje:
                if set(respuesta_correcta.palabras.all()) == set(respuesta.palabras.all()):
                    return True
        return False

    @property
    def consigna(self):
        # TODO: es redundante esta property?
        return self.ejercicio.consigna

    @property
    def es_consigna_andamiaje(self):
        return isinstance(self, ConsignaConAndamiaje)

    @property
    def andamiajes(self):
        if self.es_consigna_andamiaje:
            return self.andamiajes.all()
        else:
            return list()

    @property
    def type(self):
        raise NotImplementedError()

    def delete(self, *args, **kwargs):
        ejercicio = self.ejercicio
        super(Consigna, self).delete(*args, **kwargs)

        # se renumeran las demas consignas del ejercicio
        numero = 1
        for consigna in ejercicio.consignas:
            consigna.numero = numero
            consigna.save()
            numero += 1

        # se borran las respuestas asociadas a la consigna
        self.tipo_respuesta.objects.filter(consigna=self).delete()


class ConsignaAnalisisMorfologico(Consigna):

    def es_respuesta_parcial_correcta(self, palabra, opcion_de_respuesta):
        '''
        Verifica si una respuesta es correcta dentro del conjunto de todas las respuestas para esa palabra
        (respuesta parcial).
        '''
        respuestas_de_la_palabra = RespuestaAnalisisMorfologico.objects.filter(consigna=self, palabra=palabra, alumno=None)
        for respuesta in respuestas_de_la_palabra.all():
            if respuesta.opcion == opcion_de_respuesta:
                return True
        return False

    def es_respuesta_correcta(self, respuestas):
        '''
        Verifica que todas las respuestas sean correctas, es decir una respuesta completa.
        '''
        palabras_incorrectas = dict()

        cantidad_correctas = 0
        cantidad_correctas_totales = RespuestaAnalisisMorfologico.objects.filter(consigna=self, alumno=None).count()
        cantidad_correctas_palabra = dict()

        for respuesta in respuestas:
            posicion_palabra = int(respuesta['posiciones_palabras'][0])
            opcion_de_respuesta = int(respuesta['opcion'])

            if posicion_palabra not in cantidad_correctas_palabra:
                cantidad_correctas_palabra[posicion_palabra] = 0

            palabra = Palabra.objects.get(oracion=self.ejercicio.oracion, posicion=posicion_palabra)
            respuestas_de_la_palabra = RespuestaAnalisisMorfologico.objects.filter(consigna=self, palabra=palabra, alumno=None)

            for respuesta_correcta in respuestas_de_la_palabra.all():
                if respuesta_correcta.opcion == opcion_de_respuesta:
                    cantidad_correctas_palabra[posicion_palabra] += 1
                    cantidad_correctas += 1

            if respuestas_de_la_palabra.count() != cantidad_correctas_palabra[posicion_palabra]:
                palabras_incorrectas[posicion_palabra] = {'palabra': palabra}
            elif posicion_palabra in palabras_incorrectas:
                palabras_incorrectas.pop(posicion_palabra)

        faltan_respuestas = cantidad_correctas < cantidad_correctas_totales
        correcta = not (faltan_respuestas or len(palabras_incorrectas) > 0)

        palabras_incorrectas = map(lambda item: {'palabra': item['palabra'].string, 'posicion': item['palabra'].posicion}, palabras_incorrectas.values())

        return correcta, palabras_incorrectas, faltan_respuestas, False

    def guardar_respuestas(self, respuestas):
        for respuesta_data in respuestas:
            analisis_morfologico = int(respuesta_data['opcion'])
            for posicion in respuesta_data['posiciones_palabras']:
                palabra = Palabra.objects.get(oracion=self.ejercicio.oracion, posicion=posicion)
                respuesta = RespuestaAnalisisMorfologico(consigna=self, palabra=palabra, opcion=analisis_morfologico)
                respuesta.save()

    @property
    def type(self):
        return ConsignaAnalisisMorfologico._type()

    @staticmethod
    def _type():
        return 2


class RespuestaAnalisisMorfologico(Respuesta):

    class OpcionesAnalisisMorfologico(object):
        INDEFINIDO, DECLINACION_PRIMERA, DECLINACION_SEGUNDA, DECLINACION_TERCERA, DECLINACION_CUARTA, DECLINACION_QUINTA,\
        CASO_NOMINATIVO, CASO_VOCATIVO, CASO_ACUSATIVO, CASO_GENITIVO, CASO_DATIVO, CASO_ABLATIVO, CASO_LOCATIVO,\
        GENERO_MASCULINO, GENERO_FEMENINO, GENERO_NEUTRO, NUMERO_SINGULAR, NUMERO_PLURAL, VERBO_CONJUGACION_PRIMERA,\
        VERBO_CONJUGACION_SEGUNDA, VERBO_CONJUGACION_TERCERA, VERBO_CONJUGACION_CUARTA, VERBO_CONJUGACION_QUINTA,\
        VERBO_CONJUGACION_IRREGULAR, VERBO_MODO_INDICATIVO, VERBO_MODO_SUBJUNTIVO, VERBO_MODO_IMPERATIVO,\
        VERBO_MODO_POTENCIAL, VERBO_TIEMPO_PRESENTE, VERBO_TIEMPO_PRETERITO_IMPERFECTO, VERBO_TIEMPO_PRETERITO_PERFECTO,\
        VERBO_TIEMPO_PRETERITO_PERFECTO_COMPUESTO, VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO, VERBO_TIEMPO_FUTURO_IMPERFECTO,\
        VERBO_TIEMPO_FUTURO_PERFECTO, VERBO_VOZ_ACTIVA, VERBO_VOZ_PASIVA, VERBO_VOZ_DEPONENTE, VERBO_PERSONA_PRIMERA,\
        VERBO_PERSONA_SEGUNDA, VERBO_PERSONA_TERCERA, INVARIABLE = range(200, 242)

    opciones_analisis_morfologico = ((OpcionesAnalisisMorfologico.INDEFINIDO, 'Indefinido'),
                                     (OpcionesAnalisisMorfologico.DECLINACION_PRIMERA, 'Declinación Primera'),
                                     (OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA, 'Declinación Segunda'),
                                     (OpcionesAnalisisMorfologico.DECLINACION_TERCERA, 'Declinación Tercera'),
                                     (OpcionesAnalisisMorfologico.DECLINACION_CUARTA, 'Declinación Cuarta'),
                                     (OpcionesAnalisisMorfologico.DECLINACION_QUINTA, 'Declinación Quinta'),
                                     (OpcionesAnalisisMorfologico.CASO_NOMINATIVO, 'Caso Nominativo'),
                                     (OpcionesAnalisisMorfologico.CASO_VOCATIVO, 'Caso Vocativo'),
                                     (OpcionesAnalisisMorfologico.CASO_ACUSATIVO, 'Caso Acusativo'),
                                     (OpcionesAnalisisMorfologico.CASO_GENITIVO, 'Caso Genitivo'),
                                     (OpcionesAnalisisMorfologico.CASO_DATIVO, 'Caso Dativo'),
                                     (OpcionesAnalisisMorfologico.CASO_ABLATIVO, 'Caso Ablativo'),
                                     (OpcionesAnalisisMorfologico.CASO_LOCATIVO, 'Caso Locativo'),
                                     (OpcionesAnalisisMorfologico.GENERO_MASCULINO, 'Género Masculino'),
                                     (OpcionesAnalisisMorfologico.GENERO_FEMENINO, 'Género Femenino'),
                                     (OpcionesAnalisisMorfologico.GENERO_NEUTRO, 'Género Neutro'),
                                     (OpcionesAnalisisMorfologico.NUMERO_SINGULAR, 'Número Singular'),
                                     (OpcionesAnalisisMorfologico.NUMERO_PLURAL, 'Número Plural'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_PRIMERA, 'Verbo Conjugación Primera'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_SEGUNDA, 'Verbo Conjugación Segunda'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_TERCERA, 'Verbo Conjugación Tercera'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_CUARTA, 'Verbo Conjugación Cuarta'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_QUINTA, 'Verbo Conjugación Quinta'),
                                     (OpcionesAnalisisMorfologico.VERBO_CONJUGACION_IRREGULAR, 'Verbo Conjugación Irregular'),
                                     (OpcionesAnalisisMorfologico.VERBO_MODO_INDICATIVO, 'Verbo Modo Indicativo'),
                                     (OpcionesAnalisisMorfologico.VERBO_MODO_SUBJUNTIVO, 'Verbo Modo Subjuntivo'),
                                     (OpcionesAnalisisMorfologico.VERBO_MODO_IMPERATIVO, 'Verbo Modo Imperativo'),
                                     (OpcionesAnalisisMorfologico.VERBO_MODO_POTENCIAL, 'Verbo Modo Potencial'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRESENTE, 'Verbo Tiempo Presente'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_IMPERFECTO, 'Verbo Tiempo Pretérito Imperfecto'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PERFECTO, 'Verbo Tiempo Pretérito Perfecto'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PERFECTO_COMPUESTO, 'Verbo Tiempo Pretérito Perfecto Compuesto'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO, 'Verbo Tiempo Pretérito Pluscuamperfecto'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_FUTURO_IMPERFECTO, 'Verbo Tiempo Futuro Imperfecto'),
                                     (OpcionesAnalisisMorfologico.VERBO_TIEMPO_FUTURO_PERFECTO, 'Verbo Tiempo Futuro Perfecto'),
                                     (OpcionesAnalisisMorfologico.VERBO_VOZ_ACTIVA, 'Verbo Voz Activa'),
                                     (OpcionesAnalisisMorfologico.VERBO_VOZ_PASIVA, 'Verbo Voz Pasiva'),
                                     (OpcionesAnalisisMorfologico.VERBO_VOZ_DEPONENTE, 'Verbo Voz Deponente'),
                                     (OpcionesAnalisisMorfologico.VERBO_PERSONA_PRIMERA, 'Verbo Primera Persona'),
                                     (OpcionesAnalisisMorfologico.VERBO_PERSONA_SEGUNDA, 'Verbo Segunda Persona'),
                                     (OpcionesAnalisisMorfologico.VERBO_PERSONA_TERCERA, 'Verbo Tercera Persona'),
                                     (OpcionesAnalisisMorfologico.INVARIABLE, 'Invariable'),)

    opcion = models.PositiveIntegerField(default=OpcionesAnalisisMorfologico.INDEFINIDO, null=False, blank=True,
                                                       choices=opciones_analisis_morfologico, verbose_name="Análisis morfológico")
    consigna = models.ForeignKey(ConsignaAnalisisMorfologico, null=False)
    palabra = models.ForeignKey(Palabra, null=False)

    def to_dict(self):
        dictionary = dict()
        dictionary['opcion'] = self.opcion
        dictionary['posiciones_palabras'] = [self.palabra.posicion]
        return dictionary


class ConsignaAnalisisSintactico(Consigna):

    def es_respuesta_parcial_correcta(self, palabras, opcion_de_respuesta, oracion_completa):
        if oracion_completa:
            respuestas_de_las_palabras = RespuestaAnalisisSintactico.objects.filter(consigna=self,
                                                                                    oracion_completa=oracion_completa,
                                                                                    alumno=None)  # TODO ver como seleccionar el conjunto exacto de palabras
        else:
            respuestas_de_las_palabras = RespuestaAnalisisSintactico.objects.filter(consigna=self,
                                                                                    palabras__in=palabras,
                                                                                    oracion_completa=oracion_completa,
                                                                                    alumno=None)  # TODO ver como seleccionar el conjunto exacto de palabras

        # si no se encontró la respuesta correcta para comparar, se asume que la respuesta del alumno es incorrecta
        if not respuestas_de_las_palabras.exists():
            return False

        for respuesta in respuestas_de_las_palabras.all():
            # para verificar que las palabras sean exactamente las mismas, porque el filter solo permite el operador IN
            if set(respuesta.palabras.all()) == set(palabras) or oracion_completa:
                if respuesta.opcion == opcion_de_respuesta:
                    return True
        return False

    def es_respuesta_correcta(self, respuestas):
        '''
        :param respuestas: Tuplas (palabras, opcion_de_respuesta)
        '''
        respuestas_incorrectas = list()

        for respuesta in respuestas:
            opcion = int(respuesta['opcion'])
            es_respuesta_oracion_completa = opcion in RespuestaAnalisisSintactico.get_respuestas_para_oracion_completa()

            if not es_respuesta_oracion_completa:
                posiciones_palabras = respuesta['posiciones_palabras']
                palabras = Palabra.objects.filter(posicion__in=posiciones_palabras, oracion=self.ejercicio.oracion)
            else:
                palabras = []

            if not self.es_respuesta_parcial_correcta(palabras, opcion, es_respuesta_oracion_completa):
                respuestas_incorrectas.append(respuesta)

        # si hay respuestas incorrectas o faltan respuestas, no esta correcta la consigna
        faltan_respuestas = len(respuestas) < RespuestaAnalisisSintactico.objects.filter(consigna=self, alumno=None).count()
        correcta = not (len(respuestas_incorrectas) > 0 or faltan_respuestas)

        # para respuestas de la oracion completa, por ejemplo Sujeto Tacito
        error_oracion = False
        respuestas_oracion_completa = RespuestaAnalisisSintactico.get_respuestas_para_oracion_completa()
        for respuesta_incorrecta in respuestas_incorrectas:
            if int(respuesta_incorrecta['opcion']) in respuestas_oracion_completa:
                error_oracion = True
                break

        return correcta, respuestas_incorrectas, faltan_respuestas, error_oracion

    def guardar_respuestas(self, respuestas):
        for respuesta_data in respuestas:
            analisis_sintactico = int(respuesta_data['opcion'])
            respuesta = RespuestaAnalisisSintactico(consigna=self, opcion=analisis_sintactico)
            respuesta.save()

            for posicion in respuesta_data['posiciones_palabras']:
                palabra = Palabra.objects.get(oracion=self.ejercicio.oracion, posicion=posicion)
                respuesta.palabras.add(palabra)

            respuesta.save()

    @property
    def type(self):
        return ConsignaAnalisisSintactico._type()

    @staticmethod
    def _type():
        return 3


class RespuestaAnalisisSintactico(Respuesta):

    class Opciones(object):
        INDEFINIDO, CONSTRUCCION_ENDOCENTRICA_SUSTANTIVA, CONSTRUCCION_ENDOCENTRICA_ADJETIVA, CONSTRUCCION_ENDOCENTRICA_ADVERBIAL, \
        CONSTRUCCION_ENDOCENTRICA_VERBOIDAL, CONSTRUCCION_EXOCENTRICA, MODIFICADOR_NUCLEO_ORACIONAL_CONDICIONAL, \
        MODIFICADOR_NUCLEO_ORACIONAL_CONCESIVO, ORACION_SIMPLE_BIMEMBRE, ORACION_SIMPLE_UNIMEMBRE, \
        ORACION_COMPLEJA_COORDINATIVA, ORACION_COMPLEJA_ADJUNTIVA, ORACION_COMPLEJA_ADJUNTIVA_PARENTETICA, \
        PROPOSICION_INCLUIDA_SUSTANTIVA, PROPOSICION_INCLUIDA_ADJETIVA, PROPOSICION_INCLUIDA_ADVERBIAL, \
        PROPOSICION_INCLUIDA_DE_LUGAR, PROPOSICION_INCLUIDA_DE_MODO, PROPOSICION_INCLUIDA_DE_TIEMPO, \
        PROPOSICION_INCLUIDA_DE_CAUSA, PROPOSICION_INCLUIDA_DE_CANTIDAD, PROPOSICION_INCLUIDA_CONSECUTIVA, \
        PROPOSICION_INCLUIDA_COMPARATIVA, NUCLEO, MODIFICADOR_DIRECTO, MODIFICADOR_INDIRECTO, MODIFICADOR_EN_GENITIVO, \
        SUBORDINANTE, TERMINO, APOSICION, REGIMEN, SUJETO_TACITO, SUJETO_SIMPLE, SUJETO_COMPUESTO, \
        PREDICADO_VERBAL_SIMPLE, PREDICADO_VERBAL_COMPUESTO, PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO, PREDICADO_VERBAL_COMPUESTO_OBJETO_INDIRECTO, \
        PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL, PREDICADO_VERBAL_COMPUESTO_AGENTE, PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_SUBJETIVO, \
        PREDICADO_NOMINAL_SIMPLE, PREDICADO_NOMINAL_COMPUESTO, PREDICADO_ADVERBIAL_SIMPLE, PREDICADO_ADVERBIAL_COMPUESTO, \
        PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_OBJETIVO, PREDICADO_VERBOIDAL_SIMPLE, PREDICADO_VERBOIDAL_COMPUESTO,\
        CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_INFINITIVO, CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_PARTICIPIO, \
        CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_GERUNDIO, ORACION_COMPLEJA_SUBORDINATIVA, ADJUNTO_VOCATIVO, ADJUNTO_PARENTETICO,\
        SUJETO_DESINENCIAL, SUJETO_CONTEXTUAL, PREDICADO_DATIVO_PUNTO_VISTA, PREDICADO_DATIVO_POSESIVO, PREDICADO_SIGNO_CUASIRREFLEJO,\
        PREDICADO_SIGNO_CUASIRREFLEJO_PASIVO, PREDICADO_SIGNO_CUASIRREFLEJO_IMPERSONAL = range(300, 361)

    opciones = ((Opciones.INDEFINIDO, 'Indefinido'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_SUSTANTIVA, 'Construcción Endocéntrica Sustantiva'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_ADJETIVA, 'Construcción Endocéntrica Adjetiva'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_ADVERBIAL, 'Construcción Endocéntrica Adverbial'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_VERBOIDAL, 'Construcción Endocéntrica Verboidal'),
                (Opciones.CONSTRUCCION_EXOCENTRICA, 'Construcción Exocéntrica'),
                (Opciones.MODIFICADOR_NUCLEO_ORACIONAL_CONDICIONAL, 'Modificador del Núcleo Oracional Condicional'),
                (Opciones.MODIFICADOR_NUCLEO_ORACIONAL_CONCESIVO, 'Modificador del Núcleo Oracional Concesivo'),
                (Opciones.ORACION_SIMPLE_BIMEMBRE, 'Oración Simple Bimembre'),
                (Opciones.ORACION_SIMPLE_UNIMEMBRE, 'Oración Simple Unimembre'),
                (Opciones.ORACION_COMPLEJA_COORDINATIVA, 'Oración Completa Coordinativa'),
                (Opciones.ORACION_COMPLEJA_ADJUNTIVA, 'Oración Completa Adjuntiva'),
                (Opciones.ORACION_COMPLEJA_ADJUNTIVA_PARENTETICA, 'Oración Completa Adjuntiva Parentética'),
                (Opciones.PROPOSICION_INCLUIDA_SUSTANTIVA, 'Proposición Incluida Sustantiva'),
                (Opciones.PROPOSICION_INCLUIDA_ADJETIVA, 'Proposición Incluida Adjetiva'),
                (Opciones.PROPOSICION_INCLUIDA_ADVERBIAL, 'Proposición Incluida Adverbial'),
                (Opciones.PROPOSICION_INCLUIDA_DE_LUGAR, 'Proposición Incluida de lugar'),
                (Opciones.PROPOSICION_INCLUIDA_DE_MODO, 'Proposición Incluida de modo'),
                (Opciones.PROPOSICION_INCLUIDA_DE_TIEMPO, 'Proposición Incluida de tiempo'),
                (Opciones.PROPOSICION_INCLUIDA_DE_CAUSA, 'Proposición Incluida de causa'),
                (Opciones.PROPOSICION_INCLUIDA_DE_CANTIDAD, 'Proposición Incluida de cantidad'),
                (Opciones.PROPOSICION_INCLUIDA_CONSECUTIVA, 'Proposición Incluida Consecutiva'),
                (Opciones.PROPOSICION_INCLUIDA_COMPARATIVA, 'Proposición Incluida Comparativa'),
                (Opciones.NUCLEO, 'Núcleo'),
                (Opciones.MODIFICADOR_DIRECTO, 'Modificador Directo'),
                (Opciones.MODIFICADOR_INDIRECTO, 'Modificador Indirecto'),
                (Opciones.MODIFICADOR_EN_GENITIVO, 'Modificador en Genitivo'),
                (Opciones.SUBORDINANTE, 'Sobordinante'),
                (Opciones.TERMINO, 'Término'),
                (Opciones.APOSICION, 'Aposición'),
                (Opciones.REGIMEN, 'Régimen'),
                (Opciones.SUJETO_TACITO, 'Sujeto Tácito'),
                (Opciones.SUJETO_SIMPLE, 'Sujeto Simple'),
                (Opciones.SUJETO_COMPUESTO, 'Sujeto Compuesto'),
                (Opciones.PREDICADO_VERBAL_SIMPLE, 'Predicado Verbal Simple'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO, 'Predicado Verbal Compuesto'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO, 'Predicado Verbal Compuesto Objeto Directo'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_INDIRECTO, 'Predicado Verbal Compuesto Objeto Indirecto'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL, 'Predicado Verbal Compuesto Circunstancial'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_AGENTE, 'Predicado Verbal Compuesto Agente'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_SUBJETIVO, 'Predicado Verbal Compuesto Predicativo Subjetivo'),
                (Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_OBJETIVO, 'Predicado Verbal Compuesto Predicativo Objetivo'),
                (Opciones.PREDICADO_NOMINAL_SIMPLE, 'Predicado Nominal Simple'),
                (Opciones.PREDICADO_NOMINAL_COMPUESTO, 'Predicado Nominal Compuesto'),
                (Opciones.PREDICADO_ADVERBIAL_SIMPLE, 'Predicado Adverbial Simple'),
                (Opciones.PREDICADO_ADVERBIAL_COMPUESTO, 'Predicado Adverbial Compuesto'),
                (Opciones.PREDICADO_VERBOIDAL_SIMPLE, 'Predicado Verboidal Simple'),
                (Opciones.PREDICADO_VERBOIDAL_COMPUESTO, 'Predicado Verboidal Compuesto'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_INFINITIVO, 'Construcción Endocéntrica Verboidal de Infinitivo'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_PARTICIPIO, 'Construcción Endocéntrica Verboidal de Participio'),
                (Opciones.CONSTRUCCION_ENDOCENTRICA_VERBOIDAL_GERUNDIO, 'Construcción Endocéntrica Verboidal de Gerundio'),
                (Opciones.ORACION_COMPLEJA_SUBORDINATIVA, 'Oración Completa Subordinativa'),
                (Opciones.ADJUNTO_VOCATIVO, 'Adjunto Vocativo'),
                (Opciones.ADJUNTO_PARENTETICO, 'Adjunto Parentético'),
                (Opciones.SUJETO_DESINENCIAL, 'Sujeto desinencial'),
                (Opciones.SUJETO_CONTEXTUAL, 'Sujeto contextual'),
                (Opciones.PREDICADO_DATIVO_PUNTO_VISTA, 'Predicado Dativo de punto de vista'),
                (Opciones.PREDICADO_DATIVO_POSESIVO, 'Predicado Dativo posesivo'),
                (Opciones.PREDICADO_SIGNO_CUASIRREFLEJO, 'Predicado Signo de Cuasirreflejo'),
                (Opciones.PREDICADO_SIGNO_CUASIRREFLEJO_PASIVO, 'Predicado Signo de Cuasirreflejo Pasivo'),
                (Opciones.PREDICADO_SIGNO_CUASIRREFLEJO_IMPERSONAL, 'Predicado Signo de Cuasirreflejo Impersonal'),)

    opcion = models.PositiveIntegerField(default=Opciones.INDEFINIDO, null=False, blank=True,
                                                      choices=opciones, verbose_name="Análisis sintáctico")
    consigna = models.ForeignKey(ConsignaAnalisisSintactico, null=False)
    palabras = models.ManyToManyField(Palabra)
    oracion_completa = models.BooleanField(default=False)

    @staticmethod
    def get_respuestas_para_oracion_completa():
        return [RespuestaAnalisisSintactico.Opciones.ORACION_SIMPLE_BIMEMBRE,
                RespuestaAnalisisSintactico.Opciones.ORACION_SIMPLE_UNIMEMBRE,
                RespuestaAnalisisSintactico.Opciones.ORACION_COMPLEJA_COORDINATIVA,
                RespuestaAnalisisSintactico.Opciones.ORACION_COMPLEJA_ADJUNTIVA,
                RespuestaAnalisisSintactico.Opciones.ORACION_COMPLEJA_ADJUNTIVA_PARENTETICA,
                RespuestaAnalisisSintactico.Opciones.ORACION_COMPLEJA_SUBORDINATIVA,
                RespuestaAnalisisSintactico.Opciones.SUJETO_TACITO,
                RespuestaAnalisisSintactico.Opciones.SUJETO_DESINENCIAL,
                RespuestaAnalisisSintactico.Opciones.SUJETO_CONTEXTUAL]

    def save(self, *args, **kwargs):
        self.oracion_completa = self.opcion in RespuestaAnalisisSintactico.get_respuestas_para_oracion_completa()
        super(RespuestaAnalisisSintactico, self).save(*args, **kwargs)

    def to_dict(self):
        dictionary = dict()
        dictionary['opcion'] = self.opcion
        dictionary['posiciones_palabras'] = sorted(map(lambda palabra: palabra.posicion, self.palabras.all()))
        return dictionary


class OpcionDeRespuesta(models.Model):
    nombre = models.CharField(max_length=255, null=False)


class ConsignaCategoriaDeOracion(Consigna):
    # XXX: deprecated

    respuesta = models.ForeignKey(RespuestaCategoriaDeOracion, null=True)

    def es_respuesta_correcta(self, opcion_de_respuesta):
        return self.respuesta.opcion == opcion_de_respuesta

    @property
    def type(self):
        ConsignaCategoriaDeOracion._type()

    @staticmethod
    def _type():
        return 4


class ConsignaClaseDePalabra(Consigna):

    def es_respuesta_parcial_correcta(self, palabra, opcion_de_respuesta):
        try:
            respuesta_correcta = RespuestaClaseDePalabra.objects.filter(consigna=self, palabra=palabra, alumno=None)[0]
            return respuesta_correcta.opcion == opcion_de_respuesta
        except Exception:
            return False

    def es_respuesta_correcta(self, respuestas):
        '''
        :param respuestas: Diccionario de respuestas con claves "posiciones_palabras", "opcion" y "numero_respuesta".
        '''
        palabras_incorrectas = dict()
        palabras_correctas = set()
        cantidad_palabras = self.ejercicio.oracion.palabras.count()

        palabras_respondidas = dict()
        for posicion in range(0, cantidad_palabras):
            palabras_respondidas[posicion] = not RespuestaClaseDePalabra.objects.filter(consigna=self, palabra__posicion=posicion, alumno=None).exists()

        for respuesta in respuestas:
            posiciones_palabras = respuesta['posiciones_palabras']
            opcion = int(respuesta['opcion'])
            numero_respuesta = int(respuesta['numero_respuesta'])

            for posicion in posiciones_palabras:
                posicion = int(posicion)
                palabra = Palabra.objects.get(posicion=posicion, oracion=self.ejercicio.oracion)
                palabras_respondidas[posicion] = True

                if not self.es_respuesta_parcial_correcta(palabra, opcion):
                    palabras_incorrectas[posicion] = {'palabra': palabra, 'numero_respuesta': numero_respuesta}
                else:
                    # la respuesta es correcta y corrige a la anterior, es decir el numero de respuesta es posterior
                    # a la respuesta incorrecta
                    if posicion in palabras_incorrectas and numero_respuesta > palabras_incorrectas[posicion]['numero_respuesta']:
                        palabras_incorrectas.pop(posicion)
                    palabras_correctas.add(posicion)

        palabras_incorrectas = map(lambda item: {'palabra': item['palabra'].string, 'posicion': item['palabra'].posicion}, palabras_incorrectas.values())
        faltan_respuestas = not all(palabras_respondidas.values())
        correcta = not (len(palabras_incorrectas) > 0 or faltan_respuestas)

        return correcta, palabras_incorrectas, faltan_respuestas, False

    def guardar_respuestas(self, respuestas):
        RespuestaClaseDePalabra.objects.filter(consigna=self, alumno=None).delete()

        for respuesta_data in respuestas:
            clase_de_palabra = int(respuesta_data['opcion'])
            for posicion in respuesta_data['posiciones_palabras']:
                palabra = Palabra.objects.get(oracion=self.ejercicio.oracion, posicion=posicion)
                respuesta = RespuestaClaseDePalabra(palabra=palabra, consigna=self, opcion=clase_de_palabra)
                respuesta.save()

    @property
    def type(self):
        return ConsignaClaseDePalabra._type()

    @staticmethod
    def _type():
        return 1


class RespuestaClaseDePalabra(Respuesta):

    class TiposDePalabra(object):
        INDEFINIDO, SUSTANTIVO, ADJETIVO, ADVERBIO, SUBORDINANTE_PREPOSICIONAL, SUBORDINANTE_COMPARATIVO,\
        SUBORDINANTE_INCLUYENTE, COORDINANTE, VERBO, VERBOIDE, RELACIONANTE, INTERJECCION, PRONOMBRE_SUSTANTIVO,\
        PRONOMBRE_ADJETIVO, PRONOMBRE_ADVERBIO, VERBOIDE_INFINITIVO_PRESENTE_ACTIVO, VERBOIDE_INFINITIVO_PRESENTE_PASIVO,\
        VERBOIDE_INFINITIVO_PERFECTO_ACTIVO, VERBOIDE_INFINITIVO_PERFECTO_PASIVO, VERBOIDE_INFINITIVO_FUTURO_ACTIVO, \
        VERBOIDE_INFINITIVO_FUTURO_PASIVO, GERUNDIO, VERBOIDE_PARTICIPIO_PERFECTO_PASIVO, VERBOIDE_PARTICIPIO_PRESENTE_ACTIVO,\
        VERBOIDE_PARTICIPIO_FUTURO_ACTIVO, VERBOIDE_GERUNDIVO, VERBOIDE_SUPINO = range(400, 427)

    opciones_tipos_de_palabras = ((TiposDePalabra.INDEFINIDO, 'Indefinido'),
                                 (TiposDePalabra.SUSTANTIVO, 'Sustantivo'),
                                 (TiposDePalabra.ADJETIVO, 'Adjetivo'),
                                 (TiposDePalabra.ADVERBIO, 'Adverbio'),
                                 (TiposDePalabra.SUBORDINANTE_PREPOSICIONAL, 'Subordinante Preposicional'),
                                 (TiposDePalabra.SUBORDINANTE_COMPARATIVO, 'Subordinante Comparativo'),
                                 (TiposDePalabra.SUBORDINANTE_INCLUYENTE, 'Subordinante Incluyente'),
                                 (TiposDePalabra.COORDINANTE, 'Coordinante'),
                                 (TiposDePalabra.VERBO, 'Verbo'),
                                 (TiposDePalabra.VERBOIDE, 'Verboide'),
                                 (TiposDePalabra.RELACIONANTE, 'Relacionante'),
                                 (TiposDePalabra.INTERJECCION, 'Interjección'),
                                 (TiposDePalabra.PRONOMBRE_SUSTANTIVO, 'Pronombre sustantivo'),
                                 (TiposDePalabra.PRONOMBRE_ADJETIVO, 'Pronombre adjetivo'),
                                 (TiposDePalabra.PRONOMBRE_ADVERBIO, 'Pronombre adverbio'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_PRESENTE_ACTIVO, 'Verboide infinitivo presente activo'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_PRESENTE_PASIVO, 'Verboide infinitivo presente pasivo'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_PERFECTO_ACTIVO, 'Verboide infinitivo perfecto activo'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_PERFECTO_PASIVO, 'Verboide infinitivo perfecto pasiv'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_FUTURO_ACTIVO, 'Verboide infinitivo futuro activo'),
                                 (TiposDePalabra.VERBOIDE_INFINITIVO_FUTURO_PASIVO, 'Verboide infinitivo futuro pasivo'),
                                 (TiposDePalabra.GERUNDIO, 'Gerundio'),
                                 (TiposDePalabra.VERBOIDE_PARTICIPIO_PERFECTO_PASIVO, 'Verboide participio perfecto pasivo'),
                                 (TiposDePalabra.VERBOIDE_PARTICIPIO_PRESENTE_ACTIVO, 'Verboide participio presente activo'),
                                 (TiposDePalabra.VERBOIDE_PARTICIPIO_FUTURO_ACTIVO, 'Verboide participio futuro activo'),
                                 (TiposDePalabra.VERBOIDE_GERUNDIVO, 'Verboide Gerundivo'),
                                 (TiposDePalabra.VERBOIDE_SUPINO, 'Supino'),)

    opcion = models.PositiveIntegerField(default=TiposDePalabra.INDEFINIDO, null=False, blank=True,
                                                   choices=opciones_tipos_de_palabras, verbose_name="Clase de palabra")
    consigna = models.ForeignKey(ConsignaClaseDePalabra, null=False)
    palabra = models.ForeignKey(Palabra, null=False)

    def to_dict(self):
        dictionary = dict()
        dictionary['opcion'] = self.opcion
        dictionary['posiciones_palabras'] = [self.palabra.posicion]
        return dictionary


class ConsignaConAndamiaje(Consigna):
    andamiajes = models.ManyToManyField('Andamiaje', related_name='andamiajes')
    mensaje_default = models.CharField(max_length=1024, default='Revisá tu respuesta por favor.')

    def es_respuesta_correcta(self, palabras, opcion_de_respuesta):
        for andamiaje in self.andamiajes.all():
            # respuesta correcta
            acierto_andamiaje = andamiaje.acierto
            if set(acierto_andamiaje.palabras.all()) == set(palabras) and acierto_andamiaje.opcion == opcion_de_respuesta:
                return True, acierto_andamiaje.mensaje

            # respuestas incorrectas previstas
            for error_andamiaje in andamiaje.errores.all():
                if set(error_andamiaje.palabras.all()) == set(palabras) and error_andamiaje.opcion == opcion_de_respuesta:
                    return False, error_andamiaje.mensaje

        return False, self.mensaje_default

    def guardar_respuestas(self, respuestas):
        # se borran las respuestas existentes
        Andamiaje.objects.filter(consigna=self).delete()

        def crear_respuesta_andamiaje(consigna, respuesta_andamiaje):
            opcion = respuesta_andamiaje['opcion']
            mensaje = respuesta_andamiaje['mensaje']
            posiciones_palabras = respuesta_andamiaje['posiciones_palabras']
            palabras = Palabra.objects.filter(oracion=self.ejercicio.oracion, posicion__in=posiciones_palabras)

            respuesta_andamiaje = RespuestaAndamiaje(consigna=consigna)
            respuesta_andamiaje.opcion = opcion
            respuesta_andamiaje.mensaje = mensaje
            respuesta_andamiaje.save()
            for palabra in palabras.all():
                respuesta_andamiaje.palabras.add(palabra)
            respuesta_andamiaje.save()

            return respuesta_andamiaje

        andamiaje = None

        # primero se busca y guarda la respuesta correcta
        for respuesta_andamiaje in respuestas:
            correcta = respuesta_andamiaje['correcta']
            if correcta:
                respuesta_andamiaje_acierto = crear_respuesta_andamiaje(self, respuesta_andamiaje)
                andamiaje = Andamiaje(consigna=self, acierto=respuesta_andamiaje_acierto)
                andamiaje.save()
                break

        # se lanza una excepcion si no hay respuesta correcta
        if andamiaje is None:
            raise RespuestaCorrectaAndamiajeNoEncontrada('Falta la respuesta correcta en el andamiaje')

        # se guardan las respuestas para errores del alumno
        for respuesta_andamiaje in respuestas:
            correcta = respuesta_andamiaje['correcta']
            if not correcta:
                respuesta_andamiaje_error = crear_respuesta_andamiaje(self, respuesta_andamiaje)
                andamiaje.errores.add(respuesta_andamiaje_error)
            andamiaje.save()

        self.andamiajes.add(andamiaje)
        self.save()

    @property
    def type(self):
        return ConsignaConAndamiaje._type()

    @staticmethod
    def _type():
        return 5


class RespuestaAndamiaje(Respuesta):
    palabras = models.ManyToManyField(Palabra)
    opcion = models.PositiveIntegerField(default=RespuestaAnalisisSintactico.Opciones.INDEFINIDO,
                                                      null=False, blank=True, choices=RespuestaAnalisisSintactico.opciones,
                                                      verbose_name="Respuesta del andamiaje")
    mensaje = models.CharField(max_length=2048, null=False)
    consigna = models.ForeignKey(ConsignaConAndamiaje, null=False)

    def to_dict(self):
        dictionary = dict()
        dictionary['opcion'] = self.opcion
        dictionary['mensaje'] = self.mensaje
        dictionary['posiciones_palabras'] = sorted(map(lambda palabra: palabra.posicion, self.palabras.all()))
        dictionary['correcta'] = self.consigna.es_respuesta_correcta(self.palabras.all(), self.opcion)[0]
        return dictionary


class Andamiaje(models.Model):
    errores = models.ManyToManyField(RespuestaAndamiaje, related_name='errores')
    acierto = models.ForeignKey(RespuestaAndamiaje, null=False, related_name='acierto')
    consigna = models.ForeignKey(ConsignaConAndamiaje, null=False)

    def delete(self, *args, **kwargs):
        self.acierto.delete()

        for error in self.errores.all():
            error.delete()

        super(Andamiaje, self).delete(*args, **kwargs)
