from django import forms
from sintactic.models import Usuario
from cloudinary.forms import CloudinaryFileField


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=40, label='First name', required=True)
    last_name = forms.CharField(max_length=40, label='Last name', required=True)

    class Meta:
        model = Usuario

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()

        es_docente = Usuario.email_de_docente(user.email)
        profesor = Usuario(user=user, es_docente=es_docente)
        profesor.save()


class EditUserProfileForm(forms.Form):
    first_name = forms.CharField(max_length=50, label='Nombre')
    last_name = forms.CharField(max_length=50, label='Apellido')
    avatar = CloudinaryFileField(label='Foto de perfil', required=False, options={
                    'crop': 'limit', 'width': 250, 'height': 250
                })

    class Meta:
        model = Usuario
