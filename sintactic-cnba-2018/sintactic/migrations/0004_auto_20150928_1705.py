# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0003_consigna_numero'),
    ]

    operations = [
        migrations.CreateModel(
            name='OpcionDeRespuesta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='oracion',
            name='categoria',
            field=models.PositiveIntegerField(default=0, blank=True, verbose_name=b'Bed type', choices=[(0, b'Indefinida'), (1, b'Oraci\xc3\xb3n Bimembre Simple'), (2, b'Oraci\xc3\xb3n Bimembre Compuesta'), (3, b'Oraci\xc3\xb3n Unimembre')]),
        ),
    ]
