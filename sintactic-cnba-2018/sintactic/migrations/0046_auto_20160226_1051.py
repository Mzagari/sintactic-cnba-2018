# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def create_curso_codes(apps, schema_editor):
    # We can't import the Curso model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Curso = apps.get_model("sintactic", "Curso")
    for curso in Curso.objects.filter(code=''):
        curso.code = str(curso.id).zfill(4)
        curso.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0045_auto_20160226_1050'),
    ]

    operations = [
        migrations.RunPython(create_curso_codes),
    ]
