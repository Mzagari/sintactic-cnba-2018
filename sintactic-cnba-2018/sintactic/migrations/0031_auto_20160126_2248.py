# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0030_auto_20160126_2233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categoriaejercicio',
            name='nombre',
            field=models.CharField(unique=True, max_length=255),
        ),
    ]
