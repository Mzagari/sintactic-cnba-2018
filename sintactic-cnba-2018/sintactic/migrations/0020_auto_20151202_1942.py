# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0019_auto_20151202_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='respuestaclasedepalabra',
            name='clase_de_palabra',
            field=models.PositiveIntegerField(default=400, blank=True, verbose_name=b'Clase de palabra', choices=[(400, b'Indefinido'), (401, b'Sustantivo'), (402, b'Adjetivo'), (403, b'Adverbio'), (404, b'Subordinante Preposicional'), (405, b'Subordinante Comparativo'), (406, b'Subordinante Incluyente'), (407, b'Coordinante'), (408, b'Verbo'), (409, b'Verboide'), (410, b'Relacionante'), (411, b'Interjecci\xc3\xb3n'), (412, b'Pronombre sustantivo'), (413, b'Pronombre adjetivo'), (414, b'Pronombre adverbio'), (415, b'Verboide infinitivo presente activo'), (416, b'Verboide infinitivo presente pasivo'), (417, b'Verboide infinitivo perfecto activo'), (418, b'Verboide infinitivo perfecto pasiv'), (419, b'Verboide infinitivo futuro activo'), (420, b'Verboide infinitivo futuro pasivo'), (421, b'Gerundio'), (422, b'Verboide participio perfecto pasivo'), (423, b'Verboide participio presente activo'), (424, b'Verboide participio futuro activo'), (425, b'Verboide Gerundivo'), (426, b'Supino')]),
        ),
    ]
