# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0035_auto_20160127_2305'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ejercicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(default=b'Ejercicio nuevo', max_length=100)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
                ('carpeta', models.ForeignKey(related_name='ejercicios', to='sintactic.Carpeta')),
                ('categorias', models.ManyToManyField(to='sintactic.CategoriaEjercicio')),
                ('enunciado', models.ForeignKey(related_name='enunciado_ejercicio', to='sintactic.Enunciado', null=True)),
                ('oracion', models.ForeignKey(related_name='oracion_ejercicio', to='sintactic.Oracion')),
            ],
        ),
    ]
