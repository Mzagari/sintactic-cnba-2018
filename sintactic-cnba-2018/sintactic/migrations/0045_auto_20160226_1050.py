# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0044_auto_20160222_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='curso',
            name='code',
            field=models.CharField(default=b'', max_length=4),
        ),
        migrations.AlterField(
            model_name='carpeta',
            name='nombre',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='curso',
            name='nombre',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='curso',
            name='profesor',
            field=models.ForeignKey(to='sintactic.Usuario'),
        ),
    ]
