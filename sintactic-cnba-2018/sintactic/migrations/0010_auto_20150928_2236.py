# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0009_respuestaclasedepalabra'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consignaanalisismorfologico',
            name='respuesta',
        ),
        migrations.DeleteModel(
            name='RespuestaAnalisisMorfologico',
        ),
    ]
