# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0014_respuestaanalisissintactico'),
    ]

    operations = [
        migrations.AlterField(
            model_name='respuestaanalisissintactico',
            name='analisis_sintactico',
            field=models.PositiveIntegerField(default=0, blank=True, verbose_name=b'An\xc3\xa1lisis sint\xc3\xa1ctico', choices=[(0, b'Indefinido'), (1, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Sustantiva'), (2, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adjetiva'), (3, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adverbial'), (4, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal'), (5, b'Construcci\xc3\xb3n Exoc\xc3\xa9ntrica'), (6, b'Modificador del N\xc3\xbacleo Oracional Condicional'), (7, b'Modificador del N\xc3\xbacleo Oracional Concesivo'), (8, b'Oraci\xc3\xb3n Simple Bimembre'), (9, b'Oraci\xc3\xb3n Simple Unimembre'), (10, b'Oraci\xc3\xb3n Completa Coordinativa'), (11, b'Oraci\xc3\xb3n Completa Adjuntiva'), (12, b'Oraci\xc3\xb3n Completa Adjuntiva Parent\xc3\xa9tica'), (13, b'Proposici\xc3\xb3n Incluida Sustantiva'), (14, b'Proposici\xc3\xb3n Incluida Adjetiva'), (15, b'Proposici\xc3\xb3n Incluida Adverbial'), (16, b'Proposici\xc3\xb3n Incluida de lugar'), (17, b'Proposici\xc3\xb3n Incluida de modo'), (18, b'Proposici\xc3\xb3n Incluida de tiempo'), (19, b'Proposici\xc3\xb3n Incluida de causa'), (20, b'Proposici\xc3\xb3n Incluida de cantidad'), (21, b'Proposici\xc3\xb3n Incluida Consecutiva'), (22, b'Proposici\xc3\xb3n Incluida Comparativa'), (23, b'N\xc3\xbacleo'), (24, b'Modificador Directo'), (25, b'Modificador Indirecto'), (26, b'Modificador en Genitivo'), (27, b'Sobordinante'), (28, b'T\xc3\xa9rmino'), (29, b'Aposici\xc3\xb3n'), (30, b'R\xc3\xa9gimen'), (31, b'Sujeto T\xc3\xa1cito'), (32, b'Sujeto Simple'), (33, b'Sujeto Compuesto'), (34, b'Predicado Verbal Simple'), (35, b'Predicado Verbal Compuesto'), (36, b'Predicado Verbal Compuesto Objeto Directo'), (37, b'Predicado Verbal Compuesto Objeto Indirecto'), (38, b'Predicado Verbal Compuesto Circunstancial'), (39, b'Predicado Verbal Compuesto Agente'), (40, b'Predicado Verbal Compuesto Predicativo Subjetivo'), (41, b'Predicado Nominal Simple'), (42, b'Predicado Nominal Compuesto'), (43, b'Predicado Adverbial Simple'), (44, b'Predicado Adverbial Compuesto'), (45, b'Predicado Verboidal Simple'), (46, b'Predicado Verboidal Compuesto')]),
        ),
    ]
