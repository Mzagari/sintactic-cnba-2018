# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0029_auto_20160116_1524'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoriaEjercicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='categorias',
            field=models.ManyToManyField(to='sintactic.CategoriaEjercicio'),
        ),
    ]
