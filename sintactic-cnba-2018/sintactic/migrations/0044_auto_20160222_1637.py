# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import cloudinary.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sintactic', '0043_auto_20160219_1351'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('avatar', cloudinary.models.CloudinaryField(max_length=255, null=True, verbose_name=b'image', blank=True)),
                ('es_docente', models.BooleanField(default=False)),
                ('user', models.OneToOneField(related_name='usuario', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='alumno',
            name='user',
        ),
        migrations.RemoveField(
            model_name='profesor',
            name='user',
        ),
        migrations.AlterField(
            model_name='curso',
            name='alumnos',
            field=models.ManyToManyField(related_name='materias', to='sintactic.Usuario'),
        ),
        migrations.AlterField(
            model_name='curso',
            name='profesor',
            field=models.ForeignKey(related_name='cursos', to='sintactic.Usuario'),
        ),
        migrations.DeleteModel(
            name='Alumno',
        ),
        migrations.DeleteModel(
            name='Profesor',
        ),
    ]
