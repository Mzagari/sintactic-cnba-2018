# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0007_auto_20150928_1827'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consignaclasedepalabra',
            name='respuesta',
        ),
        migrations.DeleteModel(
            name='RespuestaClaseDePalabra',
        ),
    ]
