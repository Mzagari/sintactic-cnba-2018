# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0006_auto_20150928_1808'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsignaAnalisisMorfologico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.IntegerField(default=1)),
                ('string', models.TextField()),
                ('enunciado', models.ForeignKey(to='sintactic.Enunciado')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConsignaAnalisisSintactico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.IntegerField(default=1)),
                ('string', models.TextField()),
                ('enunciado', models.ForeignKey(to='sintactic.Enunciado')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConsignaCategoriaDeOracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.IntegerField(default=1)),
                ('string', models.TextField()),
                ('enunciado', models.ForeignKey(to='sintactic.Enunciado')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConsignaClaseDePalabra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.IntegerField(default=1)),
                ('string', models.TextField()),
                ('enunciado', models.ForeignKey(to='sintactic.Enunciado')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RenameModel(
            old_name='AnalisisMorfologico',
            new_name='RespuestaAnalisisMorfologico',
        ),
        migrations.RenameModel(
            old_name='AnalisisSintactico',
            new_name='RespuestaAnalisisSintactico',
        ),
        migrations.RenameModel(
            old_name='CategoriaDeOracion',
            new_name='RespuestaCategoriaDeOracion',
        ),
        migrations.RenameModel(
            old_name='ClaseDePalabra',
            new_name='RespuestaClaseDePalabra',
        ),
        migrations.RemoveField(
            model_name='consigna',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='consigna',
            name='respuesta_analisis_morfologico',
        ),
        migrations.RemoveField(
            model_name='consigna',
            name='respuesta_analisis_sintactico',
        ),
        migrations.RemoveField(
            model_name='consigna',
            name='respuesta_categoria_de_oracion',
        ),
        migrations.RemoveField(
            model_name='consigna',
            name='respuesta_clase_de_palabra',
        ),
        migrations.DeleteModel(
            name='Consigna',
        ),
        migrations.AddField(
            model_name='consignaclasedepalabra',
            name='respuesta',
            field=models.ForeignKey(to='sintactic.RespuestaClaseDePalabra', null=True),
        ),
        migrations.AddField(
            model_name='consignacategoriadeoracion',
            name='respuesta',
            field=models.ForeignKey(to='sintactic.RespuestaCategoriaDeOracion', null=True),
        ),
        migrations.AddField(
            model_name='consignaanalisissintactico',
            name='respuesta',
            field=models.ForeignKey(to='sintactic.RespuestaAnalisisSintactico', null=True),
        ),
        migrations.AddField(
            model_name='consignaanalisismorfologico',
            name='respuesta',
            field=models.ForeignKey(to='sintactic.RespuestaAnalisisMorfologico', null=True),
        ),
    ]
