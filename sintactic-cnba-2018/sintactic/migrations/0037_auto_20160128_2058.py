# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0036_ejercicio'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consignaanalisismorfologico',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='consignaanalisissintactico',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='consignacategoriadeoracion',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='consignaclasedepalabra',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='consignaconandamiaje',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='ejercicio',
            name='enunciado',
        ),
        migrations.DeleteModel(
            name='Enunciado',
        ),
    ]
