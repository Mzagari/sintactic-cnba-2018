# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0032_auto_20160126_2311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consignaanalisismorfologico',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado', null=True),
        ),
        migrations.AlterField(
            model_name='consignaanalisissintactico',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado', null=True),
        ),
        migrations.AlterField(
            model_name='consignacategoriadeoracion',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado', null=True),
        ),
        migrations.AlterField(
            model_name='consignaclasedepalabra',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado', null=True),
        ),
        migrations.AlterField(
            model_name='consignaconandamiaje',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado', null=True),
        ),
        migrations.AlterField(
            model_name='ejercicio',
            name='enunciado',
            field=models.ForeignKey(related_name='enunciado_ejercicio', to='sintactic.Enunciado', null=True),
        ),
    ]
