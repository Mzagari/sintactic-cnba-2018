# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0034_ejercicio_nombre'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ejercicio',
            name='categorias',
        ),
        migrations.RemoveField(
            model_name='ejercicio',
            name='enunciado',
        ),
        migrations.RemoveField(
            model_name='ejercicio',
            name='oracion',
        ),
        migrations.RemoveField(
            model_name='carpeta',
            name='ejercicios',
        ),
        migrations.DeleteModel(
            name='Ejercicio',
        ),
    ]
