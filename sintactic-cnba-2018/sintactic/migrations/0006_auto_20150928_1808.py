# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0005_auto_20150928_1749'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalisisMorfologico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AnalisisSintactico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ClaseDePalabra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='categoriadeoracion',
            name='categoria',
            field=models.PositiveIntegerField(default=0, blank=True, verbose_name=b'Categor\xc3\xada de oraci\xc3\xb3n', choices=[(0, b'Indefinida'), (1, b'Oraci\xc3\xb3n Bimembre Simple'), (2, b'Oraci\xc3\xb3n Bimembre Compuesta'), (3, b'Oraci\xc3\xb3n Unimembre')]),
        ),
        migrations.AddField(
            model_name='consigna',
            name='respuesta_categoria_de_oracion',
            field=models.ForeignKey(to='sintactic.CategoriaDeOracion', null=True),
        ),
        migrations.AddField(
            model_name='consigna',
            name='respuesta_analisis_morfologico',
            field=models.ForeignKey(to='sintactic.AnalisisMorfologico', null=True),
        ),
        migrations.AddField(
            model_name='consigna',
            name='respuesta_analisis_sintactico',
            field=models.ForeignKey(to='sintactic.AnalisisSintactico', null=True),
        ),
        migrations.AddField(
            model_name='consigna',
            name='respuesta_clase_de_palabra',
            field=models.ForeignKey(to='sintactic.ClaseDePalabra', null=True),
        ),
    ]
