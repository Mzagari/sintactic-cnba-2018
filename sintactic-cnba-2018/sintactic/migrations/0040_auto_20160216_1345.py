# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0039_auto_20160128_2218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carpeta',
            name='nombre',
            field=models.CharField(unique=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='curso',
            name='nombre',
            field=models.CharField(unique=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='ejercicio',
            name='nombre',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
