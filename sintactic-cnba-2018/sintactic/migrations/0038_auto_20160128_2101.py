# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0037_auto_20160128_2058'),
    ]

    operations = [
        migrations.AddField(
            model_name='consignaanalisismorfologico',
            name='ejercicio',
            field=models.ForeignKey(to='sintactic.Ejercicio', null=True),
        ),
        migrations.AddField(
            model_name='consignaanalisissintactico',
            name='ejercicio',
            field=models.ForeignKey(to='sintactic.Ejercicio', null=True),
        ),
        migrations.AddField(
            model_name='consignacategoriadeoracion',
            name='ejercicio',
            field=models.ForeignKey(to='sintactic.Ejercicio', null=True),
        ),
        migrations.AddField(
            model_name='consignaclasedepalabra',
            name='ejercicio',
            field=models.ForeignKey(to='sintactic.Ejercicio', null=True),
        ),
        migrations.AddField(
            model_name='consignaconandamiaje',
            name='ejercicio',
            field=models.ForeignKey(to='sintactic.Ejercicio', null=True),
        ),
    ]
