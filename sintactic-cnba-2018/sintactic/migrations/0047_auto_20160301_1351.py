# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0046_auto_20160226_1051'),
    ]

    operations = [
        migrations.AlterField(
            model_name='respuestaandamiaje',
            name='mensaje',
            field=models.CharField(max_length=2048),
        ),
    ]
