# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0048_auto_20160301_1412'),
    ]

    operations = [
        migrations.RenameField(
            model_name='respuestaanalisismorfologico',
            old_name='analisis_morfologico',
            new_name='opcion',
        ),
        migrations.RenameField(
            model_name='respuestaanalisissintactico',
            old_name='analisis_sintactico',
            new_name='opcion',
        ),
        migrations.RenameField(
            model_name='respuestaandamiaje',
            old_name='opcion_de_respuesta',
            new_name='opcion',
        ),
        migrations.RenameField(
            model_name='respuestacategoriadeoracion',
            old_name='categoria',
            new_name='opcion',
        ),
        migrations.RenameField(
            model_name='respuestaclasedepalabra',
            old_name='clase_de_palabra',
            new_name='opcion',
        ),
    ]
