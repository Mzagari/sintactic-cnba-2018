# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0026_usuario'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carpeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Curso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
                ('administrador', models.ForeignKey(to='sintactic.Usuario')),
            ],
        ),
        migrations.AddField(
            model_name='carpeta',
            name='curso',
            field=models.ForeignKey(related_name='carpetas', to='sintactic.Curso'),
        ),
        migrations.AddField(
            model_name='carpeta',
            name='ejercicios',
            field=models.ManyToManyField(to='sintactic.Ejercicio'),
        ),
    ]
