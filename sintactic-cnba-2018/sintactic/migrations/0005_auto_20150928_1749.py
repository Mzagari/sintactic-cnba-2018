# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0004_auto_20150928_1705'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oracion',
            name='categoria',
            field=models.PositiveIntegerField(default=0, blank=True, verbose_name=b'Categor\xc3\xada de oraci\xc3\xb3n', choices=[(0, b'Indefinida'), (1, b'Oraci\xc3\xb3n Bimembre Simple'), (2, b'Oraci\xc3\xb3n Bimembre Compuesta'), (3, b'Oraci\xc3\xb3n Unimembre')]),
        ),
    ]
