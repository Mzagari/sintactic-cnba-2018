# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0042_auto_20160219_1351'),
    ]

    operations = [
        migrations.CreateModel(
            name='Andamiaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='ConsignaConAndamiaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero', models.IntegerField(default=1)),
                ('string', models.TextField()),
                ('mensaje_default', models.CharField(default=b'Revis\xc3\xa1 tu respuesta por favor.', max_length=1024)),
                ('andamiajes', models.ManyToManyField(related_name='andamiajes', to='sintactic.Andamiaje')),
                ('ejercicio', models.ForeignKey(to='sintactic.Ejercicio', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RespuestaAndamiaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opcion_de_respuesta', models.PositiveIntegerField(default=300, blank=True, verbose_name=b'Respuesta del andamiaje', choices=[(300, b'Indefinido'), (301, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Sustantiva'), (302, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adjetiva'), (303, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adverbial'), (304, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal'), (305, b'Construcci\xc3\xb3n Exoc\xc3\xa9ntrica'), (306, b'Modificador del N\xc3\xbacleo Oracional Condicional'), (307, b'Modificador del N\xc3\xbacleo Oracional Concesivo'), (308, b'Oraci\xc3\xb3n Simple Bimembre'), (309, b'Oraci\xc3\xb3n Simple Unimembre'), (310, b'Oraci\xc3\xb3n Completa Coordinativa'), (311, b'Oraci\xc3\xb3n Completa Adjuntiva'), (312, b'Oraci\xc3\xb3n Completa Adjuntiva Parent\xc3\xa9tica'), (313, b'Proposici\xc3\xb3n Incluida Sustantiva'), (314, b'Proposici\xc3\xb3n Incluida Adjetiva'), (315, b'Proposici\xc3\xb3n Incluida Adverbial'), (316, b'Proposici\xc3\xb3n Incluida de lugar'), (317, b'Proposici\xc3\xb3n Incluida de modo'), (318, b'Proposici\xc3\xb3n Incluida de tiempo'), (319, b'Proposici\xc3\xb3n Incluida de causa'), (320, b'Proposici\xc3\xb3n Incluida de cantidad'), (321, b'Proposici\xc3\xb3n Incluida Consecutiva'), (322, b'Proposici\xc3\xb3n Incluida Comparativa'), (323, b'N\xc3\xbacleo'), (324, b'Modificador Directo'), (325, b'Modificador Indirecto'), (326, b'Modificador en Genitivo'), (327, b'Sobordinante'), (328, b'T\xc3\xa9rmino'), (329, b'Aposici\xc3\xb3n'), (330, b'R\xc3\xa9gimen'), (331, b'Sujeto T\xc3\xa1cito'), (332, b'Sujeto Simple'), (333, b'Sujeto Compuesto'), (334, b'Predicado Verbal Simple'), (335, b'Predicado Verbal Compuesto'), (336, b'Predicado Verbal Compuesto Objeto Directo'), (337, b'Predicado Verbal Compuesto Objeto Indirecto'), (338, b'Predicado Verbal Compuesto Circunstancial'), (339, b'Predicado Verbal Compuesto Agente'), (340, b'Predicado Verbal Compuesto Predicativo Subjetivo'), (345, b'Predicado Verbal Compuesto Predicativo Objetivo'), (341, b'Predicado Nominal Simple'), (342, b'Predicado Nominal Compuesto'), (343, b'Predicado Adverbial Simple'), (344, b'Predicado Adverbial Compuesto'), (346, b'Predicado Verboidal Simple'), (347, b'Predicado Verboidal Compuesto'), (348, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal de Infinitivo'), (349, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal de Participio'), (350, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal de Gerundio'), (351, b'Oraci\xc3\xb3n Completa Subordinativa'), (352, b'Adjunto Vocativo'), (353, b'Adjunto Parent\xc3\xa9tico'), (354, b'Sujeto desinencial'), (355, b'Sujeto contextual'), (356, b'Predicado Dativo de punto de vista'), (357, b'Predicado Dativo posesivo'), (358, b'Predicado Signo de Cuasirreflejo'), (359, b'Predicado Signo de Cuasirreflejo Pasivo'), (360, b'Predicado Signo de Cuasirreflejo Impersonal')])),
                ('mensaje', models.CharField(max_length=1024)),
                ('consigna', models.ForeignKey(to='sintactic.ConsignaConAndamiaje')),
                ('palabras', models.ManyToManyField(to='sintactic.Palabra')),
            ],
        ),
        migrations.AddField(
            model_name='andamiaje',
            name='acierto',
            field=models.ForeignKey(related_name='acierto', to='sintactic.RespuestaAndamiaje'),
        ),
        migrations.AddField(
            model_name='andamiaje',
            name='errores',
            field=models.ManyToManyField(related_name='errores', to='sintactic.RespuestaAndamiaje'),
        ),
    ]
