# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0011_respuestaanalisismorfologico'),
    ]

    operations = [
        migrations.AlterField(
            model_name='respuestaanalisismorfologico',
            name='analisis_morfologico',
            field=models.PositiveIntegerField(default=0, blank=True, verbose_name=b'An\xc3\xa1lisis morfol\xc3\xb3gico', choices=[(0, b'Indefinido'), (1, b'Declinaci\xc3\xb3n Primera'), (2, b'Declinaci\xc3\xb3n Segunda'), (3, b'Declinaci\xc3\xb3n Tercera'), (4, b'Declinaci\xc3\xb3n Cuarta'), (5, b'Declinaci\xc3\xb3n Quinta'), (6, b'Caso Nominativo'), (7, b'Caso Vocativo'), (8, b'Caso Acusativo'), (9, b'Caso Genitivo'), (10, b'Caso Dativo'), (11, b'Caso Ablativo'), (12, b'Caso Locativo'), (13, b'G\xc3\xa9nero Masculino'), (14, b'G\xc3\xa9nero Femenino'), (15, b'G\xc3\xa9nero Neutro'), (16, b'N\xc3\xbamero Singular'), (17, b'N\xc3\xbamero Plural'), (18, b'Verbo Conjugaci\xc3\xb3n Primera'), (19, b'Verbo Conjugaci\xc3\xb3n Segunda'), (20, b'Verbo Conjugaci\xc3\xb3n Tercera'), (21, b'Verbo Conjugaci\xc3\xb3n Cuarta'), (22, b'Verbo Conjugaci\xc3\xb3n Quinta'), (23, b'Verbo Conjugaci\xc3\xb3n Irregular'), (24, b'Verbo Modo Indicativo'), (25, b'Verbo Modo Subjuntivo'), (26, b'Verbo Modo Imperativo'), (27, b'Verbo Modo Potencial'), (28, b'Verbo Tiempo Presente'), (29, b'Verbo Tiempo Pret\xc3\xa9rito Imperfecto'), (30, b'Verbo Tiempo Pret\xc3\xa9rito Perfecto'), (31, b'Verbo Tiempo Pret\xc3\xa9rito Compuesto'), (32, b'Verbo Tiempo Pret\xc3\xa9rito Pluscuamperfecto'), (33, b'Verbo Tiempo Futuro Imperfecto'), (34, b'Verbo Tiempo Futuro Perfecto'), (35, b'Verbo Voz Activa'), (36, b'Verbo Voz Pasiva'), (37, b'Verbo Voz Deponente'), (38, b'Verbo Primera Persona'), (39, b'Verbo Segunda Persona'), (40, b'Verbo Tercera Persona'), (41, b'Invariable')]),
        ),
    ]
