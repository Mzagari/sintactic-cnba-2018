# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0008_auto_20150928_1938'),
    ]

    operations = [
        migrations.CreateModel(
            name='RespuestaClaseDePalabra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clase_de_palabra', models.PositiveIntegerField(default=0, blank=True, verbose_name=b'Clase de palabra', choices=[(0, b'Indefinido'), (1, b'Sustantivo'), (2, b'Adjetivo'), (3, b'Adverbio'), (4, b'Subordinante Preposicional'), (5, b'Subordinante Comparativo'), (6, b'Subordinante Incluyente'), (7, b'Coordinante'), (8, b'Verbo'), (9, b'Verboide'), (10, b'Relacionante'), (11, b'Interjecci\xc3\xb3n'), (12, b'Pronombre sustantivo'), (13, b'Pronombre adjetivo'), (14, b'Pronombre adverbio')])),
                ('consigna', models.ForeignKey(to='sintactic.ConsignaClaseDePalabra')),
                ('palabra', models.ForeignKey(to='sintactic.Palabra')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
