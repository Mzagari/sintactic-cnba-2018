# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0040_auto_20160216_1345'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ejercicio',
            options={'ordering': ['nombre']},
        ),
        migrations.AlterField(
            model_name='carpeta',
            name='nombre',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='curso',
            name='nombre',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='ejercicio',
            name='nombre',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterUniqueTogether(
            name='carpeta',
            unique_together=set([('curso', 'nombre')]),
        ),
        migrations.AlterUniqueTogether(
            name='curso',
            unique_together=set([('profesor', 'nombre')]),
        ),
        migrations.AlterUniqueTogether(
            name='ejercicio',
            unique_together=set([('carpeta', 'nombre')]),
        ),
    ]
