# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0016_auto_20151021_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='respuestaanalisismorfologico',
            name='analisis_morfologico',
            field=models.PositiveIntegerField(default=200, blank=True, verbose_name=b'An\xc3\xa1lisis morfol\xc3\xb3gico', choices=[(200, b'Indefinido'), (201, b'Declinaci\xc3\xb3n Primera'), (202, b'Declinaci\xc3\xb3n Segunda'), (203, b'Declinaci\xc3\xb3n Tercera'), (204, b'Declinaci\xc3\xb3n Cuarta'), (205, b'Declinaci\xc3\xb3n Quinta'), (206, b'Caso Nominativo'), (207, b'Caso Vocativo'), (208, b'Caso Acusativo'), (209, b'Caso Genitivo'), (210, b'Caso Dativo'), (211, b'Caso Ablativo'), (212, b'Caso Locativo'), (213, b'G\xc3\xa9nero Masculino'), (214, b'G\xc3\xa9nero Femenino'), (215, b'G\xc3\xa9nero Neutro'), (216, b'N\xc3\xbamero Singular'), (217, b'N\xc3\xbamero Plural'), (218, b'Verbo Conjugaci\xc3\xb3n Primera'), (219, b'Verbo Conjugaci\xc3\xb3n Segunda'), (220, b'Verbo Conjugaci\xc3\xb3n Tercera'), (221, b'Verbo Conjugaci\xc3\xb3n Cuarta'), (222, b'Verbo Conjugaci\xc3\xb3n Quinta'), (223, b'Verbo Conjugaci\xc3\xb3n Irregular'), (224, b'Verbo Modo Indicativo'), (225, b'Verbo Modo Subjuntivo'), (226, b'Verbo Modo Imperativo'), (227, b'Verbo Modo Potencial'), (228, b'Verbo Tiempo Presente'), (229, b'Verbo Tiempo Pret\xc3\xa9rito Imperfecto'), (230, b'Verbo Tiempo Pret\xc3\xa9rito Perfecto'), (231, b'Verbo Tiempo Pret\xc3\xa9rito Perfecto Compuesto'), (232, b'Verbo Tiempo Pret\xc3\xa9rito Pluscuamperfecto'), (233, b'Verbo Tiempo Futuro Imperfecto'), (234, b'Verbo Tiempo Futuro Perfecto'), (235, b'Verbo Voz Activa'), (236, b'Verbo Voz Pasiva'), (237, b'Verbo Voz Deponente'), (238, b'Verbo Primera Persona'), (239, b'Verbo Segunda Persona'), (240, b'Verbo Tercera Persona'), (241, b'Invariable')]),
        ),
        migrations.AlterField(
            model_name='respuestaanalisissintactico',
            name='analisis_sintactico',
            field=models.PositiveIntegerField(default=300, blank=True, verbose_name=b'An\xc3\xa1lisis sint\xc3\xa1ctico', choices=[(300, b'Indefinido'), (301, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Sustantiva'), (302, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adjetiva'), (303, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Adverbial'), (304, b'Construcci\xc3\xb3n Endoc\xc3\xa9ntrica Verboidal'), (305, b'Construcci\xc3\xb3n Exoc\xc3\xa9ntrica'), (306, b'Modificador del N\xc3\xbacleo Oracional Condicional'), (307, b'Modificador del N\xc3\xbacleo Oracional Concesivo'), (308, b'Oraci\xc3\xb3n Simple Bimembre'), (309, b'Oraci\xc3\xb3n Simple Unimembre'), (310, b'Oraci\xc3\xb3n Completa Coordinativa'), (311, b'Oraci\xc3\xb3n Completa Adjuntiva'), (312, b'Oraci\xc3\xb3n Completa Adjuntiva Parent\xc3\xa9tica'), (313, b'Proposici\xc3\xb3n Incluida Sustantiva'), (314, b'Proposici\xc3\xb3n Incluida Adjetiva'), (315, b'Proposici\xc3\xb3n Incluida Adverbial'), (316, b'Proposici\xc3\xb3n Incluida de lugar'), (317, b'Proposici\xc3\xb3n Incluida de modo'), (318, b'Proposici\xc3\xb3n Incluida de tiempo'), (319, b'Proposici\xc3\xb3n Incluida de causa'), (320, b'Proposici\xc3\xb3n Incluida de cantidad'), (321, b'Proposici\xc3\xb3n Incluida Consecutiva'), (322, b'Proposici\xc3\xb3n Incluida Comparativa'), (323, b'N\xc3\xbacleo'), (324, b'Modificador Directo'), (325, b'Modificador Indirecto'), (326, b'Modificador en Genitivo'), (327, b'Sobordinante'), (328, b'T\xc3\xa9rmino'), (329, b'Aposici\xc3\xb3n'), (330, b'R\xc3\xa9gimen'), (331, b'Sujeto T\xc3\xa1cito'), (332, b'Sujeto Simple'), (333, b'Sujeto Compuesto'), (334, b'Predicado Verbal Simple'), (335, b'Predicado Verbal Compuesto'), (336, b'Predicado Verbal Compuesto Objeto Directo'), (337, b'Predicado Verbal Compuesto Objeto Indirecto'), (338, b'Predicado Verbal Compuesto Circunstancial'), (339, b'Predicado Verbal Compuesto Agente'), (340, b'Predicado Verbal Compuesto Predicativo Subjetivo'), (345, b'Predicado Verbal Compuesto Predicativo Objetivo'), (341, b'Predicado Nominal Simple'), (342, b'Predicado Nominal Compuesto'), (343, b'Predicado Adverbial Simple'), (344, b'Predicado Adverbial Compuesto'), (346, b'Predicado Verboidal Simple'), (347, b'Predicado Verboidal Compuesto')]),
        ),
    ]
