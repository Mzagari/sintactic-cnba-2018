# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0051_auto_20160304_1122'),
    ]

    operations = [
        migrations.AddField(
            model_name='respuestaanalisismorfologico',
            name='alumno',
            field=models.ForeignKey(to='sintactic.Usuario', null=True),
        ),
        migrations.AddField(
            model_name='respuestaanalisissintactico',
            name='alumno',
            field=models.ForeignKey(to='sintactic.Usuario', null=True),
        ),
        migrations.AddField(
            model_name='respuestacategoriadeoracion',
            name='alumno',
            field=models.ForeignKey(to='sintactic.Usuario', null=True),
        ),
        migrations.AddField(
            model_name='respuestaclasedepalabra',
            name='alumno',
            field=models.ForeignKey(to='sintactic.Usuario', null=True),
        ),
    ]
