# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0031_auto_20160126_2248'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='carpeta',
            options={'ordering': ['nombre']},
        ),
        migrations.AlterModelOptions(
            name='categoriaejercicio',
            options={'ordering': ['nombre']},
        ),
        migrations.AlterModelOptions(
            name='curso',
            options={'ordering': ['nombre']},
        ),
    ]
