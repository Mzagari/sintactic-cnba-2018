# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0012_auto_20150929_0032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consignaanalisissintactico',
            name='respuesta',
        ),
        migrations.DeleteModel(
            name='RespuestaAnalisisSintactico',
        ),
    ]
