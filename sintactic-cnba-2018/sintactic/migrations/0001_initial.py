# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoriaDeOracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Consigna',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('string', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Ejercicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Enunciado',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('string', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Oracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('string', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Palabra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('string', models.CharField(max_length=255)),
                ('oracion', models.ForeignKey(to='sintactic.Oracion')),
            ],
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='enunciado',
            field=models.ForeignKey(related_name='enunciado_ejercicio', to='sintactic.Enunciado'),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='oracion',
            field=models.ForeignKey(related_name='oracion_ejercicio', to='sintactic.Oracion'),
        ),
        migrations.AddField(
            model_name='consigna',
            name='enunciado',
            field=models.ForeignKey(to='sintactic.Enunciado'),
        ),
    ]
