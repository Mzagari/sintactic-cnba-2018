# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0033_auto_20160127_2249'),
    ]

    operations = [
        migrations.AddField(
            model_name='ejercicio',
            name='nombre',
            field=models.CharField(default=b'Ejercicio nuevo', max_length=100),
        ),
    ]
