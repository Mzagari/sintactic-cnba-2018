# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0002_palabra_posicion'),
    ]

    operations = [
        migrations.AddField(
            model_name='consigna',
            name='numero',
            field=models.IntegerField(default=1),
        ),
    ]
