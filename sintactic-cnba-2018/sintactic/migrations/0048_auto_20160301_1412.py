# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0047_auto_20160301_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consignaanalisismorfologico',
            name='string',
            field=models.TextField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='consignaanalisissintactico',
            name='string',
            field=models.TextField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='consignacategoriadeoracion',
            name='string',
            field=models.TextField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='consignaclasedepalabra',
            name='string',
            field=models.TextField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='consignaconandamiaje',
            name='string',
            field=models.TextField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='oracion',
            name='string',
            field=models.TextField(max_length=2048),
        ),
    ]
