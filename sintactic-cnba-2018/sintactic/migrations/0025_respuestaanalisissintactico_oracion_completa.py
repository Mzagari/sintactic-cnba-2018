# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0024_auto_20151202_2044'),
    ]

    operations = [
        migrations.AddField(
            model_name='respuestaanalisissintactico',
            name='oracion_completa',
            field=models.BooleanField(default=False),
        ),
    ]
