# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0049_auto_20160301_1442'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='andamiaje',
            name='acierto',
        ),
        migrations.RemoveField(
            model_name='andamiaje',
            name='errores',
        ),
        migrations.RemoveField(
            model_name='consignaconandamiaje',
            name='andamiajes',
        ),
        migrations.RemoveField(
            model_name='consignaconandamiaje',
            name='ejercicio',
        ),
        migrations.RemoveField(
            model_name='respuestaandamiaje',
            name='consigna',
        ),
        migrations.RemoveField(
            model_name='respuestaandamiaje',
            name='palabras',
        ),
        migrations.AlterModelOptions(
            name='palabra',
            options={'ordering': ['posicion']},
        ),
        migrations.DeleteModel(
            name='Andamiaje',
        ),
        migrations.DeleteModel(
            name='ConsignaConAndamiaje',
        ),
        migrations.DeleteModel(
            name='RespuestaAndamiaje',
        ),
    ]
