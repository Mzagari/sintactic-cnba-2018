# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import cloudinary.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sintactic', '0027_auto_20151222_2023'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('avatar', cloudinary.models.CloudinaryField(max_length=255, null=True, verbose_name=b'image', blank=True)),
                ('user', models.OneToOneField(related_name='alumno', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Profesor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('avatar', cloudinary.models.CloudinaryField(max_length=255, null=True, verbose_name=b'image', blank=True)),
                ('user', models.OneToOneField(related_name='profesor', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='usuario',
            name='user',
        ),
        migrations.AlterField(
            model_name='curso',
            name='administrador',
            field=models.ForeignKey(related_name='cursos', to='sintactic.Profesor'),
        ),
        migrations.DeleteModel(
            name='Usuario',
        ),
        migrations.AddField(
            model_name='curso',
            name='alumnos',
            field=models.ManyToManyField(to='sintactic.Alumno'),
        ),
    ]
