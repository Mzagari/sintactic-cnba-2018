# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0052_auto_20170503_1604'),
    ]

    operations = [
        migrations.AddField(
            model_name='respuestaanalisismorfologico',
            name='correcta',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='respuestaanalisissintactico',
            name='correcta',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='respuestacategoriadeoracion',
            name='correcta',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='respuestaclasedepalabra',
            name='correcta',
            field=models.BooleanField(default=False),
        ),
    ]
