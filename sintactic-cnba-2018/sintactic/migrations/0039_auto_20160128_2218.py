# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sintactic', '0038_auto_20160128_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='palabra',
            name='string',
            field=models.TextField(max_length=100),
        ),
    ]
