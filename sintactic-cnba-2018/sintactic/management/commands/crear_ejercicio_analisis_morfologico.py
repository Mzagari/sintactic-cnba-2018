# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import *


class Command(BaseCommand):
    help = 'Crea un ejercicio de analisis morfologico con la oracion "Phoebus uidet filium Cythereiae cum pharetra in umero."'

    def add_arguments(self, parser):
        parser.add_argument('profesor_email', nargs='+', type=str)

    def handle(self, *args, **options):
        # oracion
        oracion_string = 'Phoebus uidet filium Cythereiae cum pharetra in umero.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        profesor_email = options['profesor_email'][0]
        profesor = Usuario.objects.get(user__email=profesor_email)

        if not profesor.es_docente:
            print 'ERROR: El usuario {} no es docente'
            return

        curso = Curso(nombre='Castellano 1', profesor=profesor)
        curso.save()

        carpeta = Carpeta(nombre='Carpeta 1 - Verbos', curso=curso)
        carpeta.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        self.crear_consigna_analisis_morfologico(ejercicio)

        print "ID del ejercicio: ", ejercicio.id

    def crear_consigna_analisis_morfologico(self, ejercicio):
        consigna_string = 'Realizar el análisis morfológico de cada palabra.'
        consigna = ConsignaAnalisisMorfologico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        oracion = ejercicio.oracion

        palabra_phoebus = Palabra.objects.get(string='Phoebus', oracion=oracion)
        palabra_uidet = Palabra.objects.get(string='uidet', oracion=oracion)
        palabra_filium = Palabra.objects.get(string='filium', oracion=oracion)
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae', oracion=oracion)
        palabra_cum = Palabra.objects.get(string='cum', oracion=oracion)
        palabra_pharetra = Palabra.objects.get(string='pharetra', oracion=oracion)
        palabra_in = Palabra.objects.get(string='in', oracion=oracion)
        palabra_umero = Palabra.objects.get(string='umero', oracion=oracion)

        # Phoebus (2°d, N, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_NOMINATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()

        # uidet (2°c, MInd, Pte, 3°, sg, VA)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_CONJUGACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_MODO_INDICATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRESENTE).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_PERSONA_TERCERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_VOZ_ACTIVA).save()

        # filium (2°d, AC, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ACUSATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()

        # Cythereiae (1°d, G, f, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_GENITIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()

        # pharetra (1°d, AB, f, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()

        # umero (2°d, AB, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
