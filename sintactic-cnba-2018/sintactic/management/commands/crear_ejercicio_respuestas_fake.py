# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import *


class Command(BaseCommand):
    help = 'Crea un ejercicio con respuestas fake para poderlo resolver rapido.'

    def add_arguments(self, parser):
        parser.add_argument('profesor_email', nargs='+', type=str)

    def handle(self, *args, **options):
        # oracion
        oracion_string = 'Phoebus uidet filium Cythereiae cum pharetra in umero.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        profesor_email = options['profesor_email'][0]
        profesor = Usuario.objects.get(user__email=profesor_email)

        if not profesor.es_docente:
            print 'ERROR: El usuario {} no es docente'
            return

        curso = Curso(nombre='Castellano 1', profesor=profesor)
        curso.save()

        carpeta = Carpeta(nombre='Carpeta 1 - Verbos', curso=curso)
        carpeta.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        self.crear_consigna_1_clases_de_palabras(ejercicio)
        self.crear_consigna_2_analisis_morfologico(ejercicio)
        self.crear_consigna_3_analisis_sintactico(ejercicio)

        print "ID del ejercicio: ", ejercicio.id

    def crear_consigna_1_clases_de_palabras(self, ejercicio):
        # consigna 1
        consigna_string = 'Clase de palabra (RESPUESTA FAKE: Clase de palabra/Variables/Verboide/Participio/Participio perfecto pasivo).'
        consigna = ConsignaClaseDePalabra(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas
        for palabra in ejercicio.oracion.palabras.all():
            respuesta_clase_de_palabra = RespuestaClaseDePalabra(palabra=palabra, consigna=consigna,
                                                                 opcion=RespuestaClaseDePalabra.TiposDePalabra.VERBOIDE_PARTICIPIO_PERFECTO_PASIVO)
            respuesta_clase_de_palabra.save()

    def crear_consigna_2_analisis_morfologico(self, ejercicio):
        consigna_string = 'Análisis morfológico (RESPUESTA FAKE: Verbo/Tiempo/Pretérito Pluscuamperfecto)'
        consigna = ConsignaAnalisisMorfologico(ejercicio=ejercicio, numero=2, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        oracion = ejercicio.oracion

        palabra_phoebus = Palabra.objects.get(string='Phoebus', oracion=oracion)
        palabra_uidet = Palabra.objects.get(string='uidet', oracion=oracion)
        palabra_filium = Palabra.objects.get(string='filium', oracion=oracion)
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae', oracion=oracion)
        palabra_cum = Palabra.objects.get(string='cum', oracion=oracion)
        palabra_pharetra = Palabra.objects.get(string='pharetra', oracion=oracion)
        palabra_in = Palabra.objects.get(string='in', oracion=oracion)
        palabra_umero = Palabra.objects.get(string='umero', oracion=oracion)

        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cum,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_in,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRETERITO_PLUSCUAMPERFECTO).save()

    def crear_consigna_3_analisis_sintactico(self, ejercicio):
        consigna_string = 'Análisis sintáctico (RESPONDER CORRECTAMENTE).'
        consigna = ConsignaAnalisisSintactico(ejercicio=ejercicio, numero=3, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        oracion = ejercicio.oracion

        palabra_phoebus = Palabra.objects.get(string='Phoebus', oracion=oracion)
        palabra_uidet = Palabra.objects.get(string='uidet', oracion=oracion)
        palabra_filium = Palabra.objects.get(string='filium', oracion=oracion)
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae', oracion=oracion)
        palabra_cum = Palabra.objects.get(string='cum', oracion=oracion)
        palabra_pharetra = Palabra.objects.get(string='pharetra', oracion=oracion)
        palabra_in = Palabra.objects.get(string='in', oracion=oracion)
        palabra_umero = Palabra.objects.get(string='umero', oracion=oracion)

        # Phoebus (S, n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_phoebus)
        respuesta.save()

        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE)
        respuesta.save()
        respuesta.palabras.add(palabra_phoebus)
        respuesta.save()

        # uidet filium Cythereiae cum pharetra in umero (P)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO)
        respuesta.save()
        respuesta.palabras.add(palabra_uidet)
        respuesta.palabras.add(palabra_filium)
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.palabras.add(palabra_cum)
        respuesta.palabras.add(palabra_pharetra)
        respuesta.palabras.add(palabra_in)
        respuesta.palabras.add(palabra_umero)
        respuesta.save()

        # uidet (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_uidet)
        respuesta.save()

        # filium (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_filium)
        respuesta.save()

        # pharetra (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()

        # pharetra (t)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()

        # umero (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_umero)
        respuesta.save()

        # umero (t)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_umero)
        respuesta.save()

        # cythereiae (md)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.MODIFICADOR_DIRECTO)
        respuesta.save()
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.save()

        # cum (s)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_cum)
        respuesta.save()

        # in (s)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_in)
        respuesta.save()

        # filium cythereiae (od)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO)
        respuesta.save()
        respuesta.palabras.add(palabra_filium)
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.save()

        # cum pharetra (circ)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL)
        respuesta.save()
        respuesta.palabras.add(palabra_cum)
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()

        # in umero (circ)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL)
        respuesta.save()
        respuesta.palabras.add(palabra_in)
        respuesta.palabras.add(palabra_umero)
        respuesta.save()
