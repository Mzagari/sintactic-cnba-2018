# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import CategoriaEjercicio
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Crear las categorias de ejercicios.'

    def handle(self, *args, **options):
        nombres_categorias = ['Verboides', 'Verbos', 'Sustantivos', 'Sujeto y predicado']

        for nombre_categoria in nombres_categorias:
            try:
                categoria = CategoriaEjercicio(nombre=nombre_categoria)
                categoria.save()
                print 'Creada la categoria "{}"'.format(categoria.nombre)
            except IntegrityError:
                print 'La categoria "{}" ya existia'.format(nombre_categoria)