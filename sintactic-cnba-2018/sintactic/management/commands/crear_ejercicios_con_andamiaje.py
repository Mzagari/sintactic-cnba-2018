# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import *


class Command(BaseCommand):
    help = 'Crea un ejercicio solamente con una sola consigna de analisis sintactico con andamiaje con la ' \
           'oracion "Me gustan los chocolates."'

    def add_arguments(self, parser):
        parser.add_argument('profesor_email', nargs='+', type=str)

    def handle(self, *args, **options):
        profesor_email = options['profesor_email'][0]
        profesor = Usuario.objects.get(user__email=profesor_email)

        if not profesor.es_docente:
            print 'ERROR: El usuario {} no es docente'
            return

        curso = Curso(nombre='Castellano 1', profesor=profesor)
        curso.save()

        carpeta = Carpeta(nombre='Carpeta 1 - Verbos', curso=curso)
        carpeta.save()

        self.crear_ejercicio_con_1_consigna_de_andamiaje(carpeta)
        self.crear_ejercicio_con_2_consignas_de_andamiajes(carpeta)
        print 'Se crearon 2 ejercicios con andamiaje.'

    def crear_ejercicio_con_1_consigna_de_andamiaje(self, carpeta):
        oracion_string = 'Me gustan los chocolates.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        consigna_string = 'Marcar el sujeto de la siguiente oración.'
        consigna = ConsignaConAndamiaje(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # andamiaje

        palabra_me = Palabra.objects.get(string='Me', oracion=oracion)
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_chocolates = Palabra.objects.get(string='chocolates', oracion=oracion)

        respuesta_andamiaje_error = RespuestaAndamiaje(consigna=consigna)
        respuesta_andamiaje_error.opcion = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_andamiaje_error.mensaje = u'Atención: ¿En qué persona y número está el verbo? Recordá que debe tener ' \
                                            u'concordancia con el núcleo del sujeto.'
        respuesta_andamiaje_error.save()
        respuesta_andamiaje_error.palabras.add(palabra_me)
        respuesta_andamiaje_error.save()

        respuesta_andamiaje_acierto = RespuestaAndamiaje(consigna=consigna)
        respuesta_andamiaje_acierto.opcion = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_andamiaje_acierto.mensaje = u'MUY BIEN: En esta oración, el sujeto gramatical (“los chocolates”) no ' \
                                              u'coincide con el sujeto que realiza o experimenta la acción (la primera ' \
                                              u'persona identificable en el pronombre “me”).'
        respuesta_andamiaje_acierto.save()
        respuesta_andamiaje_acierto.palabras.add(palabra_los)
        respuesta_andamiaje_acierto.palabras.add(palabra_chocolates)
        respuesta_andamiaje_acierto.save()

        andamiaje = Andamiaje(acierto=respuesta_andamiaje_acierto)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error)
        andamiaje.save()

        consigna.andamiajes.add(andamiaje)
        consigna.save()

        print "ID del ejercicio: ", ejercicio.id

    def crear_ejercicio_con_2_consignas_de_andamiajes(self, carpeta):
        oracion_string = 'Encontraron ahogados a los navegantes desaparecidos.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna 1

        consigna_string = 'Marcar el OD de la siguiente oración.'
        consigna_1 = ConsignaConAndamiaje(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna_1.save()

        palabra_ahogados = Palabra.objects.get(string='ahogados', oracion=oracion)
        palabra_a = Palabra.objects.get(string='a', oracion=oracion)
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_navegantes = Palabra.objects.get(string='navegantes', oracion=oracion)
        palabra_desaparecidos = Palabra.objects.get(string='desaparecidos', oracion=oracion)

        respuesta_andamiaje_error = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_error.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_error.mensaje = u'Atención: Recordá que el OD, al pasarse a voz pasiva, es Sujeto. ¿Qué ' \
                                            u'parte de la oración del ejercicio cumple con esa característica?'
        respuesta_andamiaje_error.save()
        respuesta_andamiaje_error.palabras.add(palabra_a)
        respuesta_andamiaje_error.palabras.add(palabra_los)
        respuesta_andamiaje_error.palabras.add(palabra_navegantes)
        respuesta_andamiaje_error.save()

        respuesta_andamiaje_acierto = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_acierto.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_acierto.mensaje = u'MUY BIEN'
        respuesta_andamiaje_acierto.save()
        respuesta_andamiaje_acierto.palabras.add(palabra_a)
        respuesta_andamiaje_acierto.palabras.add(palabra_los)
        respuesta_andamiaje_acierto.palabras.add(palabra_navegantes)
        respuesta_andamiaje_acierto.palabras.add(palabra_desaparecidos)
        respuesta_andamiaje_acierto.save()

        andamiaje = Andamiaje(acierto=respuesta_andamiaje_acierto)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error)
        andamiaje.save()

        consigna_1.andamiajes.add(andamiaje)
        consigna_1.save()

        # consigna 2

        consigna_string = '¿Qué función tiene "ahogados"?'
        consigna_2 = ConsignaConAndamiaje(ejercicio=ejercicio, numero=2, string=consigna_string)
        consigna_2.save()

        respuesta_andamiaje_error_1 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_error_1.opcion = RespuestaAnalisisSintactico.Opciones.PROPOSICION_INCLUIDA_DE_MODO
        respuesta_andamiaje_error_1.mensaje = u'Atención: Recordá que todos los circunstanciales son, por definición, ' \
                                              u'invariables. ¿Qué pasaría en esta oración si cambiara el género y/o el ' \
                                              u'número del núcleo del OD?'
        respuesta_andamiaje_error_1.save()
        respuesta_andamiaje_error_1.palabras.add(palabra_ahogados)
        respuesta_andamiaje_error_1.save()

        respuesta_andamiaje_error_2 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_error_2.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_SUBJETIVO
        respuesta_andamiaje_error_2.mensaje = u'Atención: El predicativo subjetivo es el modificador del núcleo del ' \
                                              u'predicado que concuerda en género y número con el núcleo del sujeto; ' \
                                              u'en este caso, la concordancia se da con el núcleo del OD. Por lo ' \
                                              u'tanto, ¿qué función sintáctica es la correcta?'
        respuesta_andamiaje_error_2.save()
        respuesta_andamiaje_error_2.palabras.add(palabra_ahogados)
        respuesta_andamiaje_error_2.save()

        respuesta_andamiaje_acierto_2 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_acierto_2.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_OBJETIVO
        respuesta_andamiaje_acierto_2.mensaje = u'MUY BIEN: A diferencia de los circunstanciales, que modifican ' \
                                                u'únicamente a los verbos y no establecen concordancia en género/número ' \
                                                u'con ninguna parte de la oración, los predicativos aportan información ' \
                                                u'sobre el sujeto (PS) o el OD (PO); por esa razón, concuerdan en género ' \
                                                u'y número con el núcleo respectivo.'
        respuesta_andamiaje_acierto_2.save()
        respuesta_andamiaje_acierto_2.palabras.add(palabra_ahogados)
        respuesta_andamiaje_acierto_2.save()

        andamiaje = Andamiaje(acierto=respuesta_andamiaje_acierto_2)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error_1)
        andamiaje.errores.add(respuesta_andamiaje_error_2)
        andamiaje.save()

        consigna_2.andamiajes.add(andamiaje)
        consigna_2.save()

        print "ID del ejercicio: ", ejercicio.id
