# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from allauth.account.models import EmailAddress
from sintactic.models import Usuario
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Creates an active user for testing purposes.'

    def add_arguments(self, parser):
        parser.add_argument('email', nargs='+', type=str)
        parser.add_argument('rol', nargs='+', type=str)

    def handle(self, *args, **options):
        email = options['email'][0]
        rol = options['rol'][0]
        self.create_user_profile(email, rol)

    def create_user_profile(self, email, rol):
        if rol == 'p':
            role = 'docente'
            es_docente = True
        else:
            role = 'alumno'
            es_docente = False

        password = 'test123'
        try:
            user = User.objects.create_user(email, email, password)
        except IntegrityError:
            print "ERROR: email address {} is already in use".format(email)
            return

        usuario = Usuario(user=user, es_docente=es_docente)
        usuario.save()

        usuario.user.is_active = True
        usuario.user.first_name = 'Juan'
        usuario.user.last_name = 'Pérez'
        usuario.user.save()

        emailaddress = EmailAddress.objects.create(user=usuario.user,
                                                   email=email,
                                                   primary=True,
                                                   verified=True)
        emailaddress.save()

        print "created user {} with password '{}' and role {}".format(email, password, role)

        self.crear_cursos_y_carpetas(usuario)

    def crear_cursos_y_carpetas(self, profesor):
        if profesor.es_docente:
            curso1 = profesor.crear_curso('Castellano 1')
            print u'Creado curso "{}"'.format(curso1.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta 1 - Verbos', curso1)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso1.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta 2 - Verboides', curso1)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso1.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta 3 - Sujeto y predicado', curso1)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso1.nombre.decode('utf-8'))

            curso2 = profesor.crear_curso('Castellano 2')
            print u'Creado curso "{}"'.format(curso2.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta 1 - Análisis morfológico', curso2)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso2.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta 2 - Análisis sintáctico', curso2)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso2.nombre.decode('utf-8'))

            curso3 = profesor.crear_curso('Latín I')
            print u'Creado curso "{}"'.format(curso3.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta I - Phoebus', curso3)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso3.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta II - Cum', curso3)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso3.nombre.decode('utf-8'))
            carpeta = profesor.crear_carpeta('Carpeta III - Umero', curso3)
            print u'Creada carpeta "{}" en curso "{}"'.format(carpeta.nombre.decode('utf-8'), curso3.nombre.decode('utf-8'))
