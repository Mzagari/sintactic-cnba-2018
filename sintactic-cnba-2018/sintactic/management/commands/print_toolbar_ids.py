# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import *


class Command(BaseCommand):
    help = 'Crea un ejercicio completo con la oracion "Phoebus uidet filium Cythereiae cum pharetra in umero."'

    def handle(self, *args, **options):
        print 'Categorias de oracion:'
        print
        for opcion in Oracion.opciones_categoria:
            print '{} - {}'.format(opcion[0], opcion[1])
        print

        print 'Clases de palabras:'
        print
        for opcion in RespuestaClaseDePalabra.opciones_tipos_de_palabras:
            print '{} - {}'.format(opcion[0], opcion[1])
        print

        print 'Analisis morfologico:'
        print
        for opcion in RespuestaAnalisisMorfologico.opciones_analisis_morfologico:
            print '{} - {}'.format(opcion[0], opcion[1])
        print

        print 'Analisis sintactico:'
        print
        for opcion in RespuestaAnalisisSintactico.opciones:
            print '{} - {}'.format(opcion[0], opcion[1])
        print
