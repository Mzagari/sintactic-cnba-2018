# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from sintactic.models import *


class Command(BaseCommand):
    help = 'Crea un ejercicio solamente con una sola consigna de analisis sintactico con andamiaje con una ' \
           'oracion larga.'

    def handle(self, *args, **options):
        oracion_string = '¿Qué está pasando, Juan? ¡Esto es increíble! Genial, único (ponele). "Hay que repetirlo..." - exclamó Pedro.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        # enunciado
        enunciado_string = 'Análisis sintactico con andamiaje de una oración'
        enunciado = Enunciado(string=enunciado_string)
        enunciado.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, enunciado=enunciado)
        ejercicio.save()

        consigna_string = 'Marcar el sujeto de la siguiente oración.'
        consigna = ConsignaConAndamiaje(enunciado=enunciado, numero=1, string=consigna_string)
        consigna.save()

        print "ID del ejercicio: {} (respuestas correctas no cargadas)".format(ejercicio.id)
