from django.shortcuts import get_object_or_404
from sintactic.models import Usuario


def get_usuario(request):
    return get_object_or_404(Usuario, user__id=request.user.id)
