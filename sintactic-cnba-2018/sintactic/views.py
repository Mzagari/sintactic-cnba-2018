from __future__ import unicode_literals
from allauth.account.forms import LoginForm
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render, render_to_response
from sintactic.models import Ejercicio, Consigna, Palabra, Respuesta, Carpeta, Curso, NombreRepetido, Usuario, \
    ConsignaConAndamiaje
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from sintactic.utils import get_usuario
from sintactic.forms import EditUserProfileForm
import simplejson as json
import cloudinary


def docente_only(function):
    """
    Solo un usuario docente puede acceder a las vistas decoradas con este decorator.
    """

    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if request.user.is_authenticated():
                user_profile = get_usuario(request)

                if user_profile.es_docente:
                    return view_func(request, *args, **kwargs)
                else:
                    return HttpResponseRedirect(reverse('estudiante_cursos'))
            else:
                return HttpResponse(status=404)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)


def home(request):
    if request.user.is_authenticated():
        context = {
            'user': request.user,
            'user_profile': get_usuario(request)
        }
        return render(request, 'home.html', context)
    else:
        context = {'login_form': LoginForm()}
        return render(request, 'home.html', context)


@login_required
def ejercicio_general(request, ejercicio_id, numero_consigna_actual):
    estudiante = get_usuario(request)
    ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
    carpeta = ejercicio.carpeta

    if estudiante.es_docente:
        try:
            # se empieza a corregir por la primera consigna
            numero_consigna_actual = int(numero_consigna_actual) + 1
            consigna = ejercicio.consignas[numero_consigna_actual - 1]

            respuestas = consigna.respuestas
            andamiajes = consigna.andamiajes
        except IndexError:
            consigna = None
            respuestas = list()
            andamiajes = list()
    else:
        try:
            # se empieza a corregir por la primera consigna
            numero_consigna_actual = int(numero_consigna_actual) + 1
            consigna = ejercicio.consignas[numero_consigna_actual - 1]

            if isinstance(consigna, ConsignaConAndamiaje):
                respuestas = list()
                andamiajes = consigna.obtener_respuestas_alumno(estudiante)
            else:
                respuestas = consigna.obtener_respuestas_alumno(estudiante)
                andamiajes = list()
        except IndexError:
            consigna = None
            respuestas = list()
            andamiajes = list()

    context = {
        'ejercicio': ejercicio,
        'consigna': consigna,
        'user_profile': estudiante,
        'respuestas': respuestas,
        'andamiajes': andamiajes,
        'es_docente': False,
        'estudiante': estudiante,
        'carpeta': carpeta,
        'avanzar_consigna': False
    }
    return render(request, 'ejercicios/ejercicio_general.html', context)


@csrf_exempt
@login_required
def ejercicio_general_siguiente(request):
    try:
        respuestas_parciales = json.loads(request.POST['respuestas_parciales'])
        numero_consigna = json.loads(request.POST['numero_consigna'])
        ejercicio_id = json.loads(request.POST['ejercicio_id'])
        ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
        consigna = Consigna.get_consigna(ejercicio, numero_consigna)
        sobreescribir = json.loads(request.POST['sobreescribir'])

        # conversion de tipos de datos
        for respuesta_parcial in respuestas_parciales:
            respuesta_parcial['opcion'] = int(respuesta_parcial['opcion'])
            posiciones = list()
            for posicion in respuesta_parcial['posiciones_palabras']:
                posiciones.append(int(posicion))
            respuesta_parcial['posiciones_palabras'] = posiciones

        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas_parciales)

        # se guarda la respuesta del alumno
        if sobreescribir:
            user_profile = get_usuario(request)
            respuestas = Respuesta.from_dict(consigna, respuestas_parciales)
            user_profile.responder_consigna(consigna, respuestas, sobreescribir)

        try:
            siguiente_consigna = Consigna.get_consigna(ejercicio, numero_consigna + 1)

            if user_profile.es_docente:
                nuevas_respuestas = siguiente_consigna.respuestas
            else:
                nuevas_respuestas = siguiente_consigna.obtener_respuestas_alumno(user_profile)

            nuevas_respuestas = map(lambda respuesta: respuesta.to_dict(), nuevas_respuestas)
        except Exception:
            nuevas_respuestas = list()

        return_data = {
            'respuesta_correcta': respuesta_correcta,
            'palabras_incorrectas': palabras_incorrectas,
            'faltan_respuestas': faltan_respuestas,
            'error_oracion': error_oracion,
            'nuevas_respuestas': nuevas_respuestas
        }
        return HttpResponse(json.dumps(return_data), status=200)
    except Exception as e:
        import traceback
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@csrf_exempt
@login_required
def ejercicio_con_andamiaje(request):
    try:
        posiciones_palabras = map(lambda palabra_id: int(palabra_id), request.POST.getlist('posiciones_palabras[]'))
        opcion_de_respuesta = int(json.loads(request.POST['opcion_de_respuesta']))
        numero_consigna = json.loads(request.POST['numero_consigna'])
        ejercicio_id = json.loads(request.POST['ejercicio_id'])
        sobreescribir = json.loads(request.POST['sobreescribir'])

        ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
        consigna = Consigna.get_consigna(ejercicio, numero_consigna)
        palabras = Palabra.objects.filter(oracion=ejercicio.oracion, posicion__in=posiciones_palabras)

        respuesta_correcta, mensaje = consigna.es_respuesta_correcta(palabras, opcion_de_respuesta)

        # se guarda la respuesta del alumno
        respuestas_parciales = [{
            'posiciones_palabras': posiciones_palabras,
            'opcion': opcion_de_respuesta
        }]
        respuestas = Respuesta.from_dict(consigna, respuestas_parciales)
        user_profile = get_usuario(request)
        user_profile.responder_consigna(consigna, respuestas, sobreescribir)

        return_data = {
            'respuesta_correcta': respuesta_correcta,
            'mensaje': mensaje
        }
        return HttpResponse(json.dumps(return_data), status=200)
    except Exception as e:
        import traceback
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@login_required
@docente_only
def ejercicio_nuevo(request, carpeta_id=None, ejercicio_id=None):
    profesor = get_usuario(request)
    #categorias = CategoriaEjercicio.objects.order_by('nombre')

    if carpeta_id is not None:
        carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    else:
        carpeta = None

    if ejercicio_id is not None:
        ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
    else:
        ejercicio = None

    context = {
        'profesor': profesor,
        #'categorias': categorias,
        'carpeta': carpeta,
        'ejercicio': ejercicio,
        'user_profile': get_usuario(request),
    }
    return render(request, 'ejercicios/nuevo.html', context)


@docente_only
@login_required
@csrf_exempt
def web_service_ejercicio_nuevo(request):
    try:
        nombre_ejercicio = request.POST['nombre_ejercicio']
        curso_id = request.POST['curso_id']
        carpeta_id = int(request.POST['carpeta_id'])
        #categorias = json.loads(request.POST['categorias'])
        categorias = []
        oracion_string = request.POST['oracion']

        try:
            ejercicio_id = int(request.POST['ejercicio_id'])
        except Exception:
            ejercicio_id = None

        carpeta = get_object_or_404(Carpeta, id=carpeta_id)

        if ejercicio_id is None:
            ejercicio = Ejercicio.crear(nombre_ejercicio, carpeta, categorias, oracion_string)
        else:
            ejercicio = Ejercicio.editar(ejercicio_id, nombre_ejercicio, carpeta, categorias, oracion_string)

        return_data = {
            'ejercicio_id': ejercicio.id
        }
        return HttpResponse(json.dumps(return_data), status=200)
    except NombreRepetido:
        return_data = {
            'error': 'Ya existe un ejercicio con este nombre en la misma carpeta'
        }
        return HttpResponse(json.dumps(return_data), status=400)
    except Exception as e:
        print str(e)
        return HttpResponse(str(e), status=500)


@docente_only
@login_required
def nueva_consigna(request, ejercicio_id):
    ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
    user_profile = get_usuario(request)

    context = {
        'user_profile': user_profile,
        'ejercicio': ejercicio,
        'numero_consigna': len(ejercicio.consignas) + 1
    }
    return render(request, 'ejercicios/nueva_consigna.html', context)


@docente_only
@login_required
def editar_consigna(request, ejercicio_id, numero_consigna):
    ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
    consigna = Consigna.get_consigna(ejercicio, numero_consigna)
    user_profile = get_usuario(request)

    context = {
        'user_profile': user_profile,
        'ejercicio': ejercicio,
        'consigna': consigna,
        'numero_consigna': consigna.numero,
        'es_docente': user_profile.es_docente,
        'respuestas': consigna.respuestas,
        'andamiajes': consigna.andamiajes
    }
    return render(request, 'ejercicios/nueva_consigna.html', context)


@docente_only
@login_required
def corregir_ejercicios(request, carpeta_id, ejercicio_id):
    user_profile = get_usuario(request)
    carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)

    estudiantes = list()
    for estudiante in carpeta.curso.alumnos.all():
        estudiante.estado_resolucion_ejercicio = estudiante.get_estado_resolucion_ejercicio(ejercicio)
        estudiantes.append(estudiante)

    context = {
        'user_profile': user_profile,
        'estudiantes': estudiantes,
        'ejercicio': ejercicio,
        'carpeta': carpeta
    }
    return render(request, 'ejercicios/listado_corregir.html', context)


@docente_only
@login_required
def corregir_ejercicio(request, carpeta_id, ejercicio_id, estudiante_id, numero_consigna_anterior):
    '''
    Permite ver el ejercicio (read only).
    '''
    user_profile = get_usuario(request)
    ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
    estudiante = get_object_or_404(Usuario, id=estudiante_id)
    carpeta = ejercicio.carpeta
    try:
        # se empieza a corregir por la primera consigna
        numero_consigna_actual = int(numero_consigna_anterior) + 1
        consigna = ejercicio.consignas[numero_consigna_actual - 1]

        andamiajes = list()
        if isinstance(consigna, ConsignaConAndamiaje):
            respuestas = list()
            andamiajes_queryset = consigna.obtener_respuestas_alumno(estudiante)
            for andamiaje in andamiajes_queryset.all():
                andamiaje.dictionary = andamiaje.to_dict()
                andamiajes.append(andamiaje)
        else:
            respuestas = consigna.obtener_respuestas_alumno(estudiante)

    except IndexError:
        consigna = None
        respuestas = list()
        andamiajes = list()

    # import pdb;pdb.set_trace()

    context = {
        'user_profile': user_profile,
        'estudiante': estudiante,
        'ejercicio': ejercicio,
        'es_docente': True,
        'carpeta': carpeta,
        'consigna': consigna,
        'respuestas': respuestas,
        'andamiajes': andamiajes,
        'avanzar_consigna': False
    }
    return render(request, 'ejercicios/ejercicio_general.html', context)


@csrf_exempt
@docente_only
@login_required
def web_service_guardar_consigna(request):
    try:
        ejercicio_id = request.POST['ejercicio_id']
        numero_consigna = int(request.POST['numero_consigna'])
        texto_consigna = request.POST['texto_consigna']
        tipo_actividad = int(request.POST['tipo_actividad'])
        respuestas = json.loads(request.POST['respuestas'])
        categorias = []
        ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)

        Ejercicio.editar(ejercicio.id, ejercicio.nombre, ejercicio.carpeta, categorias, ejercicio.oracion.string,
                         texto_consigna, tipo_actividad, respuestas, numero_consigna)

        return HttpResponse(status=200)
    except Exception as e:
        print str(e)
        return HttpResponse(str(e), status=500)


@csrf_exempt
@docente_only
@login_required
def web_service_borrar_consigna(request):
    try:
        ejercicio_id = request.POST['ejercicio_id']
        numero_consigna = int(request.POST['numero_consigna'])

        ejercicio = get_object_or_404(Ejercicio, id=ejercicio_id)
        consigna = filter(lambda consigna: consigna.numero == numero_consigna, ejercicio.consignas)[0]
        consigna.delete()

        return HttpResponse(status=200)
    except Exception as e:
        print str(e)
        return HttpResponse(str(e), status=404)


@csrf_exempt
@docente_only
@login_required
def web_service_borrar_ejercicio(request):
    ejercicio_id = request.POST['ejercicio_id']
    Ejercicio.objects.get(id=ejercicio_id).delete()
    return HttpResponse(status=200)


@docente_only
@login_required
def docente_cursos(request):
    user_profile = get_usuario(request)
    context = {
        'user_profile': user_profile,
        'cursos': user_profile.cursos.order_by('nombre')
    }
    return render(request, 'general/docente/cursos.html', context)


@docente_only
@login_required
def docente_ver_estudiantes_curso(request, curso_id):
    user_profile = get_usuario(request)
    curso = get_object_or_404(Curso, id=curso_id)
    context = {
        'user_profile': user_profile,
        'estudiantes': curso.alumnos,
        'curso': curso
    }
    return render(request, 'general/docente/estudiantes.html', context)


@csrf_exempt
@docente_only
@login_required
def web_service_borrar_estudiante_de_curso(request):
    estudiante_id = request.POST['user_id']
    curso_id = request.POST['curso_id']

    curso = get_object_or_404(Curso, id=curso_id)
    estudiante = get_object_or_404(Usuario, id=estudiante_id)
    curso.alumnos.remove(estudiante)

    return HttpResponse(status=200)


@csrf_exempt
@docente_only
@login_required
def web_service_borrar_todos_estudiantes_de_curso(request):
    curso_id = request.POST['curso_id']

    curso = get_object_or_404(Curso, id=curso_id)
    curso.alumnos.all().delete()

    return HttpResponse(status=200)


@docente_only
@login_required
def docente_carpetas(request, curso_id):
    curso = get_object_or_404(Curso, id=curso_id)
    context = {
        'user_profile': get_usuario(request),
        'curso': curso,
        'carpetas': curso.carpetas.order_by('nombre')
    }
    return render(request, 'general/docente/carpetas.html', context)


@docente_only
@login_required
@csrf_exempt
def docente_visibilidad_carpeta(request):
    carpeta_id = request.POST['carpeta_id']
    carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    carpeta.cambiar_visibilidad()
    return HttpResponse(status=200)


@docente_only
@login_required
def docente_ejercicios(request, carpeta_id):
    user_profile = get_usuario(request)
    carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    context = {
        'user_profile': user_profile,
        'ejercicios': carpeta.ejercicios,
        'curso': carpeta.curso,
        'carpeta': carpeta
    }
    return render(request, 'general/docente/ejercicios.html', context)


@login_required
def estudiante_cursos(request):
    user_profile = get_usuario(request)
    context = {
        'user_profile': user_profile,
        'cursos': user_profile.cursos
    }
    return render(request, 'general/estudiante/cursos.html', context)


@login_required
def estudiante_carpetas(request, curso_id):
    user_profile = get_usuario(request)
    curso = get_object_or_404(Curso, id=curso_id)
    carpetas = curso.carpetas.filter(visible=True)

    context = {
        'user_profile': user_profile,
        'curso': curso,
        'carpetas': carpetas
    }
    return render(request, 'general/estudiante/carpetas.html', context)


@login_required
def estudiante_ejercicios(request, carpeta_id):
    carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    ejercicios = carpeta.ejercicios
    estudiante = get_usuario(request)

    ejercicios_list = list()
    for ejercicio in ejercicios.all():
        # el alumno debe ver ejercicios que tengan al menos una consigna
        if ejercicio.consignas:
            ejercicio.estado_resolucion_ejercicio = estudiante.get_estado_resolucion_ejercicio(ejercicio)
            ejercicios_list.append(ejercicio)

    context = {
        'user_profile': get_usuario(request),
        'ejercicios': ejercicios_list,
        'carpeta': carpeta
    }
    return render(request, 'general/estudiante/ejercicios.html', context)


@login_required
@csrf_exempt
def estudiante_unirme_a_curso(request):
    user_profile = get_usuario(request)
    curso_code = request.POST['curso_code']
    curso = get_object_or_404(Curso, code=curso_code)

    if user_profile.es_docente:
        return HttpResponse(status=400)

    curso.alumnos.add(user_profile)
    curso.save()

    return HttpResponse(status=200)


def terminos_y_condiciones(request):
    try:
        user_profile = get_usuario(request)
    except:
        user_profile = None

    context = {
        'user_profile': user_profile
    }
    return render(request, 'terminos_y_condiciones.html', context)


@csrf_exempt
@login_required
@docente_only
def web_service_curso_nuevo(request):
    nombre = request.POST['nombre']
    profesor = get_usuario(request)

    try:
        curso = profesor.crear_curso(nombre)

        return_data = {
            'curso_code': curso.code
        }
        return HttpResponse(json.dumps(return_data), status=200)
    except NombreRepetido:
        return_data = {
            'error': 'Ya existe un curso con ese nombre.'
        }
        return HttpResponse(json.dumps(return_data), status=400)


@csrf_exempt
@login_required
@docente_only
def web_service_borrar_curso(request):
    curso_id = request.POST['curso_id']
    Curso.objects.get(id=curso_id).delete()
    return HttpResponse(status=200)


@csrf_exempt
@login_required
@docente_only
def web_service_editar_curso(request):
    curso_id = request.POST['curso_id']
    nombre = request.POST['nombre']

    curso = Curso.objects.get(id=curso_id)
    curso.nombre = nombre
    curso.save()

    return HttpResponse(status=200)


@csrf_exempt
@login_required
@docente_only
def web_service_carpeta_nueva(request):
    nombre = request.POST['nombre']
    curso_id = request.POST['curso_id']
    curso = get_object_or_404(Curso, id=curso_id)
    profesor = get_usuario(request)

    try:
        carpeta = profesor.crear_carpeta(nombre, curso)
        return_data = {
            'carpeta_id': carpeta.id
        }
        return HttpResponse(json.dumps(return_data), status=200)
    except NombreRepetido:
        return_data = {
            'error': 'Ya existe una carpeta con ese nombre.'
        }
        return HttpResponse(json.dumps(return_data), status=400)


@csrf_exempt
@login_required
@docente_only
def web_service_editar_carpeta(request):
    carpeta_id = request.POST['carpeta_id']
    nombre = request.POST['nombre']

    carpeta = Carpeta.objects.get(id=carpeta_id)
    carpeta.nombre = nombre
    carpeta.save()

    return HttpResponse(status=200)


@csrf_exempt
@login_required
@docente_only
def web_service_copiar_carpeta(request):
    carpeta_id = request.POST['carpeta_id']
    curso_id = request.POST['curso_id']

    carpeta = get_object_or_404(Carpeta, id=carpeta_id)
    curso = get_object_or_404(Curso, id=curso_id)

    carpeta.copiar(curso)

    return HttpResponse(status=200)


@csrf_exempt
@login_required
@docente_only
def web_service_borrar_carpeta(request):
    carpeta_id = request.POST['carpeta_id']
    Carpeta.objects.get(id=carpeta_id).delete()
    return HttpResponse(status=200)


@login_required
def mi_perfil(request):
    user_profile = None
    ctx = dict(backend_form=EditUserProfileForm())

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = EditUserProfileForm(request.POST, request.FILES)

        # check whether it's valid:
        if form.is_valid():
            try:
                # process the data in form.cleaned_data as required
                user = request.user
                user_profile = Usuario.objects.get(user=user)
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']

                if form.cleaned_data['avatar']:
                    # delete previous profile picture
                    if user_profile.avatar:
                        cloudinary.uploader.destroy(public_id=user_profile.avatar.public_id, invalidate=True)

                    # save new profile picture
                    user_profile.avatar = form.cleaned_data['avatar']

                user.save()
                user_profile.save()

                success = 'Perfil guardado correctamente'
                error = None
            except Exception:
                success = None
                error = 'Se produjo un error al guardar los datos del perfil'

            # redirect to a new URL:
            context = {
                'form': form,
                'user_profile': user_profile,
                'success': success,
                'error': error,
                'mis_cursos': user_profile.cursos
            }
            return render(request, 'general/mi_perfil.html', context)

    # if a GET (or any other method) we'll create a blank form
    else:
        user_profile = get_usuario(request)
        ctx = {
            'user_profile': user_profile,
            'first_name': user_profile.first_name,
            'last_name': user_profile.last_name,
            'mis_cursos': user_profile.cursos
        }
        form = EditUserProfileForm(ctx)

    context = {
        'form': form,
        'user_profile': user_profile,
        'mis_cursos': user_profile.cursos
    }
    return render(request, 'general/mi_perfil.html', context)


def handler500(request):
    response = render_to_response('500.html', {}, context_instance=RequestContext(request))
    response.status_code = 500
    return response


def handler404(request):
    response = render_to_response('404.html', {}, context_instance=RequestContext(request))
    response.status_code = 404
    return response


@login_required
@csrf_exempt
def email_testing(request, email_path):
    user_profile = get_usuario(request)
    user_email = 'test@test.com'

    ctx = {
        'user_profile': user_profile,
        'user_email': user_email,
        'site_name': 'Sintactic-CNBA-2018',
        'activate_url': 'http://sintactic-cnba-2018.herokuapp.com/accounts/confirm-email/OA:1d8DOs:akE_40Tu9pWoJxFbtqCTZ30YMlo/',

    }
    return render(request, 'account/email/' + email_path, ctx)


def error_500_test(request):
    raise Exception('500 test')
