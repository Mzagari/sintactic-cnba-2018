# -*- coding: utf-8 -*-

from django.test import TestCase
from sintactic.models import *
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from allauth.account.models import EmailAddress


class BaseTestCase(TestCase):

    def setUp(self):
        self.oracion_string = 'Phoebus uidet filium Cythereiae cum pharetra in umero.'
        self.oracion = Oracion(string=self.oracion_string)
        self.oracion.categoria = Oracion.Categorias.OBS
        self.oracion.save()

    def __crear_usuario(self, email, es_docente):
        password = 'test123'
        try:
            user = User.objects.create_user(email, email, password)
        except IntegrityError:
            self.fail('email address {} is already in use'.format(email))

        usuario = Usuario(user=user, es_docente=es_docente)
        usuario.save()

        usuario.user.is_active = True
        usuario.user.first_name = 'Juan'
        usuario.user.last_name = 'Pérez'
        usuario.user.save()

        emailaddress = EmailAddress.objects.create(user=usuario.user,
                                                   email=email,
                                                   primary=True,
                                                   verified=True)
        emailaddress.save()

        return usuario

    def crear_alumno(self, email='alumno@testcase.com'):
        return self.__crear_usuario(email, False)

    def crear_profesor(self, email='profesor@testcase.com'):
        return self.__crear_usuario(email, True)

    def crear_curso_y_carpeta(self, profesor, nombre_curso='Castellano 1', nombre_carpeta='Carpeta 1 - Verbos'):
        curso = Curso(nombre=nombre_curso, profesor=profesor)
        curso.save()

        carpeta = Carpeta(nombre=nombre_carpeta, curso=curso)
        carpeta.save()

        return curso, carpeta

    def agregar_respuesta(self, respuesta, numero_respuesta, respuestas):
        respuesta_dict = {
            'posiciones_palabras': map(lambda p: p.posicion, respuesta.palabras.all()),
            'opcion': respuesta.opcion,
            'numero_respuesta': numero_respuesta
        }
        respuestas.append(respuesta_dict)
        numero_respuesta += 1


class EjercicioTest(BaseTestCase):

    def test_ejercicio_tiene_oracion(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        consigna_1 = 'Determinar que clase de palabra es cada una en la oracion siguiente.'
        objeto_consigna_1 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=1, string=consigna_1)
        objeto_consigna_1.save()

        consigna_2 = 'Realizar el analisis morfologico de cada palabra.'
        objeto_consigna_2 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=2, string=consigna_2)
        objeto_consigna_2.save()

        consigna_3 = 'Realizar el analisis sintactico de la oracion, teniendo en cuenta lo analizado en los pasos anteriores.'
        objeto_consigna_3 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=3, string=consigna_3)
        objeto_consigna_3.save()

        # oracion
        self.assertEqual(ejercicio.oracion.string, self.oracion_string)

        # consignas
        self.assertEqual(len(ejercicio.consignas), 3)
        self.assertEqual(ejercicio.consignas[0].string, consigna_1)
        self.assertEqual(ejercicio.consignas[1].string, consigna_2)
        self.assertEqual(ejercicio.consignas[2].string, consigna_3)

    def test_palabras_no_tienen_signos_de_puntuacion(self):
        oracion_string = '¿Qué está pasando, Juan? ¡Esto es increíble! Genial, único (ponele). "Hay que repetirlo..." - exclamó Pedro.'
        oracion = Oracion(string=oracion_string)
        oracion.save()

        for palabra in self.oracion.palabras.all():
            self.assertTrue('.' not in palabra.string)
            self.assertTrue(',' not in palabra.string)
            self.assertTrue(';' not in palabra.string)
            self.assertTrue(':' not in palabra.string)
            self.assertTrue('?' not in palabra.string)
            self.assertTrue(u'¿' not in palabra.string)
            self.assertTrue(u'¡' not in palabra.string)
            self.assertTrue('!' not in palabra.string)
            self.assertTrue('(' not in palabra.string)
            self.assertTrue(')' not in palabra.string)
            self.assertTrue('...' not in palabra.string)
            self.assertTrue('-' not in palabra.string)
            self.assertTrue('"' not in palabra.string)
            self.assertTrue("'" not in palabra.string)

        resultado_esperado = u'Qué está pasando Juan Esto es increíble Genial único ponele Hay que repetirlo exclamó Pedro'
        resultado_obtenido = ' '.join(map(lambda palabra: palabra.string, oracion.palabras.all()))
        self.assertEqual(resultado_esperado, resultado_obtenido)

    def test_al_borrar_ejercicio_se_borran_objetos_asociados(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        consigna_1 = 'Determinar que clase de palabra es cada una en la oracion siguiente.'
        consigna_1 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=1, string=consigna_1)
        consigna_1.save()

        consigna_2 = 'Realizar el analisis morfologico de cada palabra.'
        consigna_2 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=2, string=consigna_2)
        consigna_2.save()

        consigna_3 = 'Realizar el analisis sintactico de la oracion, teniendo en cuenta lo analizado en los pasos anteriores.'
        consigna_3 = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=3, string=consigna_3)
        consigna_3.save()

        self.assertEqual(1, Ejercicio.objects.count())
        self.assertEqual(1, Oracion.objects.count())
        self.assertEqual(3, ConsignaCategoriaDeOracion.objects.count())
        self.assertEqual(8, Palabra.objects.count())

        ejercicio.delete()

        self.assertEqual(0, Ejercicio.objects.count())
        self.assertEqual(0, Oracion.objects.count())
        self.assertEqual(0, ConsignaCategoriaDeOracion.objects.count())
        self.assertEqual(0, Palabra.objects.count())


class OracionTest(BaseTestCase):

    def test_oracion_tiene_categoria(self):
        self.assertEqual(Oracion.Categorias.OBS, self.oracion.categoria)

    def test_ejercicio_determinar_categoria_de_la_oracion(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna con respuesta correcta
        respuesta_categoria_de_oracion = RespuestaCategoriaDeOracion(opcion=Oracion.Categorias.OBS)
        respuesta_categoria_de_oracion.save()
        consigna_string = 'Determinar la categoría de la oración.'
        consigna = ConsignaCategoriaDeOracion(ejercicio=ejercicio, numero=1, string=consigna_string, respuesta=respuesta_categoria_de_oracion)

        consigna.respuesta_categoria_de_oracion = respuesta_categoria_de_oracion
        consigna.save()

        self.assertTrue(consigna.es_respuesta_correcta(Oracion.Categorias.OBS))
        self.assertFalse(consigna.es_respuesta_correcta(Oracion.Categorias.OBC))
        self.assertFalse(consigna.es_respuesta_correcta(Oracion.Categorias.OU))


class EjercicioClaseDePalabraTest(BaseTestCase):

    def test_ejercicio_determinar_clase_de_palabras_en_la_oracion(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Determinar qué clase de palabra es cada una en la oración siguiente.'
        consigna = ConsignaClaseDePalabra(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas
        for palabra in self.oracion.palabras.all():
            respuesta_clase_de_palabra = RespuestaClaseDePalabra(palabra=palabra, consigna=consigna,
                                                                 opcion=RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO)
            if palabra.string == 'uidet':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.VERBO
            elif palabra.string == 'cum' or palabra.string == 'in':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL

            respuesta_clase_de_palabra.save()

        # verificacion de respuestas parciales correctas e incorrectas
        palabra_phoebus = Palabra.objects.get(string='Phoebus')
        palabra_uidet = Palabra.objects.get(string='uidet')
        palabra_filium = Palabra.objects.get(string='filium')
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae')
        palabra_cum = Palabra.objects.get(string='cum')
        palabra_pharetra = Palabra.objects.get(string='pharetra')
        palabra_in = Palabra.objects.get(string='in')
        palabra_umero = Palabra.objects.get(string='umero')

        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaClaseDePalabra.TiposDePalabra.VERBO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_cum, RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO))

        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaClaseDePalabra.TiposDePalabra.PRONOMBRE_SUSTANTIVO))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_cum, RespuestaClaseDePalabra.TiposDePalabra.VERBO))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaClaseDePalabra.TiposDePalabra.ADJETIVO))

        # respuesta completa correcta
        respuestas = list()
        respuestas.append({'posiciones_palabras': [palabra_phoebus.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 1})
        respuestas.append({'posiciones_palabras': [palabra_uidet.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.VERBO, 'numero_respuesta': 2})
        respuestas.append({'posiciones_palabras': [palabra_filium.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 3})
        respuestas.append({'posiciones_palabras': [palabra_cythereiae.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 4})
        respuestas.append({'posiciones_palabras': [palabra_cum.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL, 'numero_respuesta': 5})
        respuestas.append({'posiciones_palabras': [palabra_pharetra.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 6})
        respuestas.append({'posiciones_palabras': [palabra_in.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL, 'numero_respuesta': 7})
        respuestas.append({'posiciones_palabras': [palabra_umero.posicion], 'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 8})
        self.assertTrue(consigna.es_respuesta_correcta(respuestas)[0])


class EjercicioAnalisisMorfologicoTest(BaseTestCase):

    def test_ejercicio_realizar_analisis_morfologico_de_la_oracion(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Realizar el analisis morfologico de cada palabra.'
        consigna = ConsignaAnalisisMorfologico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:

        palabra_phoebus = Palabra.objects.get(string='Phoebus')
        palabra_uidet = Palabra.objects.get(string='uidet')
        palabra_filium = Palabra.objects.get(string='filium')
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae')
        palabra_cum = Palabra.objects.get(string='cum')
        palabra_pharetra = Palabra.objects.get(string='pharetra')
        palabra_in = Palabra.objects.get(string='in')
        palabra_umero = Palabra.objects.get(string='umero')

        # Phoebus (2°d, N, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_NOMINATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_phoebus,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_phoebus, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_phoebus, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_phoebus, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_NOMINATIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_phoebus, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_phoebus, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))

        # uidet (2°c, MInd, Pte, 3°, sg, VA)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_CONJUGACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_MODO_INDICATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRESENTE).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_PERSONA_TERCERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_uidet,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_VOZ_ACTIVA).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_CONJUGACION_SEGUNDA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_MODO_INDICATIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRESENTE))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_PERSONA_TERCERA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_VOZ_ACTIVA))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_uidet, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_VOZ_PASIVA))

        # filium (2°d, AC, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ACUSATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_filium,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ACUSATIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_filium, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_PLURAL))

        # Cythereiae (1°d, G, f, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_GENITIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_cythereiae,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_cythereiae, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_cythereiae, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_GENITIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_cythereiae, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_cythereiae, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_cythereiae, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_PLURAL))

        # pharetra (1°d, AB, f, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_pharetra,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_pharetra, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_pharetra, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_pharetra, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_pharetra, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_pharetra, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_PLURAL))

        # umero (2°d, AB, m, sg)
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO).save()
        RespuestaAnalisisMorfologico(consigna=consigna, palabra=palabra_umero,
                                     opcion=RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR).save()
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO))
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR))
        self.assertFalse(consigna.es_respuesta_parcial_correcta(palabra_umero, RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_PLURAL))

        respuestas_oracion_completa = [
            {
                'posiciones_palabras': [0],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA
            },
            {
                'posiciones_palabras': [0],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_NOMINATIVO
            },
            {
                'posiciones_palabras': [0],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO
            },
            {
                'posiciones_palabras': [0],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_CONJUGACION_SEGUNDA
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_MODO_INDICATIVO
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_TIEMPO_PRESENTE
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_PERSONA_TERCERA
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            },
            {
                'posiciones_palabras': [1],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.VERBO_VOZ_ACTIVA
            },
            {
                'posiciones_palabras': [2],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA
            },
            {
                'posiciones_palabras': [2],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ACUSATIVO
            },
            {
                'posiciones_palabras': [2],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO
            },
            {
                'posiciones_palabras': [2],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            },
            {
                'posiciones_palabras': [3],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA
            },
            {
                'posiciones_palabras': [3],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_GENITIVO
            },
            {
                'posiciones_palabras': [3],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO
            },
            {
                'posiciones_palabras': [3],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            },
            {
                'posiciones_palabras': [4],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.INVARIABLE
            },
            {
                'posiciones_palabras': [5],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_PRIMERA
            },
            {
                'posiciones_palabras': [5],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO
            },
            {
                'posiciones_palabras': [5],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_FEMENINO
            },
            {
                'posiciones_palabras': [5],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            },
            {
                'posiciones_palabras': [6],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.INVARIABLE
            },
            {
                'posiciones_palabras': [7],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.DECLINACION_SEGUNDA
            },
            {
                'posiciones_palabras': [7],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.CASO_ABLATIVO
            },
            {
                'posiciones_palabras': [7],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.GENERO_MASCULINO
            },
            {
                'posiciones_palabras': [7],
                'opcion': RespuestaAnalisisMorfologico.OpcionesAnalisisMorfologico.NUMERO_SINGULAR
            }
        ]
        self.assertTrue(consigna.es_respuesta_correcta(respuestas_oracion_completa)[0])


class EjercicioAnalisisSintacticoTest(BaseTestCase):

    def test_ejercicio_realizar_analisis_sintactico_de_la_oracion(self):
        numero_respuesta = 1

        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Realizar el análisis sintáctico de la oración.'
        consigna = ConsignaAnalisisSintactico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        respuestas = []

        palabra_phoebus = Palabra.objects.get(string='Phoebus')
        palabra_uidet = Palabra.objects.get(string='uidet')
        palabra_filium = Palabra.objects.get(string='filium')
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae')
        palabra_cum = Palabra.objects.get(string='cum')
        palabra_pharetra = Palabra.objects.get(string='pharetra')
        palabra_in = Palabra.objects.get(string='in')
        palabra_umero = Palabra.objects.get(string='umero')

        # Phoebus (S, n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_phoebus)
        respuesta.save()

        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_phoebus], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE)
        respuesta.save()
        respuesta.palabras.add(palabra_phoebus)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_phoebus], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        # uidet filium Cythereiae cum pharetra in umero (P)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO)
        respuesta.save()
        respuesta.palabras.add(palabra_uidet)
        respuesta.palabras.add(palabra_filium)
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.palabras.add(palabra_cum)
        respuesta.palabras.add(palabra_pharetra)
        respuesta.palabras.add(palabra_in)
        respuesta.palabras.add(palabra_umero)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        palabras_respuesta = [palabra_uidet, palabra_filium, palabra_cum, palabra_cythereiae, palabra_umero, palabra_in, palabra_pharetra]
        self.assertTrue(consigna.es_respuesta_parcial_correcta(palabras_respuesta, RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO, False))

        # uidet (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_uidet)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_uidet], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        # filium (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_filium)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_filium], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        # pharetra (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_pharetra], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        # pharetra (t)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_pharetra], RespuestaAnalisisSintactico.Opciones.TERMINO, False))

        # umero (n)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_umero)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_umero], RespuestaAnalisisSintactico.Opciones.NUCLEO, False))

        # umero (t)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_umero)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_umero], RespuestaAnalisisSintactico.Opciones.TERMINO, False))

        # cythereiae (md)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.MODIFICADOR_DIRECTO)
        respuesta.save()
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_cythereiae], RespuestaAnalisisSintactico.Opciones.MODIFICADOR_DIRECTO, False))

        # cum (s)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_cum)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_cum], RespuestaAnalisisSintactico.Opciones.SUBORDINANTE, False))

        # in (s)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_in)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_in], RespuestaAnalisisSintactico.Opciones.SUBORDINANTE, False))

        # filium cythereiae (od)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO)
        respuesta.save()
        respuesta.palabras.add(palabra_filium)
        respuesta.palabras.add(palabra_cythereiae)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_cythereiae, palabra_filium], RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO, False))

        # cum pharetra (circ)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL)
        respuesta.save()
        respuesta.palabras.add(palabra_cum)
        respuesta.palabras.add(palabra_pharetra)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_cum, palabra_pharetra], RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL, False))

        # in umero (circ)
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL)
        respuesta.save()
        respuesta.palabras.add(palabra_in)
        respuesta.palabras.add(palabra_umero)
        respuesta.save()

        # falta una respuesta correcta
        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(0, len(palabras_incorrectas))
        self.assertTrue(faltan_respuestas)
        self.assertFalse(error_oracion)

        # una respuesta incorrecta
        respuesta_incorrecta = {
                'posiciones_palabras': [palabra_phoebus.posicion],
                'opcion': RespuestaAnalisisSintactico.Opciones.APOSICION,
                'numero_respuesta': numero_respuesta + 1
            }
        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas + [respuesta_incorrecta])
        self.assertFalse(respuesta_correcta)
        self.assertEqual(1, len(palabras_incorrectas))
        self.assertFalse(faltan_respuestas)
        self.assertFalse(error_oracion)

        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)
        self.assertTrue(consigna.es_respuesta_parcial_correcta([palabra_umero, palabra_in], RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_CIRCUNSTANCIAL, False))

        # todas las respuestas correctas de la oracion completa
        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas)
        self.assertTrue(respuesta_correcta)
        self.assertEqual(0, len(palabras_incorrectas))
        self.assertFalse(faltan_respuestas)
        self.assertFalse(error_oracion)

    def __test_respuesta_oracion_completa(self):
        numero_respuesta = 1
        oracion = Oracion(string='Viajé a Sydney')  # Sujeto tácito: yo
        oracion.save()

        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Realizar el análisis sintáctico de la oración.'
        consigna = ConsignaAnalisisSintactico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        respuestas = []

        palabra_viaje = Palabra.objects.get(string='Viajé')
        palabra_a = Palabra.objects.get(string='a')
        palabra_sydney = Palabra.objects.get(string='Sydney')

        # Viajé (n) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_viaje)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        # a (sub.) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_a)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        # Sydney (T.) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_sydney)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        return respuestas, numero_respuesta, consigna

    def test_respuesta_correcta_de_oracion_completa(self):
        respuestas, numero_respuesta, consigna = self.__test_respuesta_oracion_completa()

        # Sujeto Tácito
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUJETO_TACITO)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        # todas las respuestas correctas de la oracion completa
        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas)
        self.assertTrue(respuesta_correcta)
        self.assertEqual(0, len(palabras_incorrectas))
        self.assertFalse(faltan_respuestas)
        self.assertFalse(error_oracion)

    def test_respuesta_incorrecta_de_oracion_completa(self):
        respuestas, numero_respuesta, consigna = self.__test_respuesta_oracion_completa()

        # Sujeto Tácito
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUJETO_TACITO)
        respuesta.save()

        respuesta_incorrecta = {
            'posiciones_palabras': [],
            'opcion': RespuestaAnalisisSintactico.Opciones.SUJETO_DESINENCIAL,
            'numero_respuesta': numero_respuesta
        }
        respuestas.append(respuesta_incorrecta)

        # todas las respuestas correctas de la oracion completa
        respuesta_correcta, palabras_incorrectas, faltan_respuestas, error_oracion = consigna.es_respuesta_correcta(respuestas)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(1, len(palabras_incorrectas))
        self.assertFalse(faltan_respuestas)
        self.assertTrue(error_oracion)


class CursoTest(BaseTestCase):

    def test_curso_tiene_alumnos_y_carpetas(self):
        alumno1 = self.crear_alumno('alumno1@test.com')
        alumno2 = self.crear_alumno('alumno2@test.com')
        alumno3 = self.crear_alumno('alumno3@test.com')
        profesor = self.crear_profesor('profesor@test.com')

        # cursos
        nombre_curso = 'Castellano 1'
        curso = profesor.crear_curso(nombre_curso)

        self.assertEqual(0, curso.alumnos.count())
        curso.alumnos.add(alumno1)
        self.assertEqual(1, curso.alumnos.count())
        curso.alumnos.add(alumno2)
        self.assertEqual(2, curso.alumnos.count())
        curso.alumnos.add(alumno3)
        self.assertEqual(3, curso.alumnos.count())

        self.assertEqual(nombre_curso, curso.nombre)
        self.assertEqual(profesor, curso.profesor)

        # carpetas
        self.assertEqual(0, curso.carpetas.count())

        nombre_carpeta_1 = 'Carpeta 1 - Verbos'
        carpeta1 = profesor.crear_carpeta(nombre_carpeta_1, curso)
        self.assertEqual(1, curso.carpetas.count())
        self.assertEqual(nombre_carpeta_1, carpeta1.nombre)

        nombre_carpeta_2 = 'Carpeta 2 - Adverbios'
        carpeta2 = profesor.crear_carpeta(nombre_carpeta_2, curso)
        self.assertEqual(2, curso.carpetas.count())
        self.assertEqual(nombre_carpeta_2, carpeta2.nombre)


class CarpetaTest(BaseTestCase):

    def test_carpeta_tiene_ejercicios(self):
        profesor = self.crear_profesor('profesor2@test.com')

        nombre_curso = 'Castellano 2'
        curso = profesor.crear_curso(nombre_curso)

        nombre_carpeta = 'Carpeta 2 - Análisis Sintáctico'
        carpeta = profesor.crear_carpeta(nombre_carpeta, curso)
        self.assertEqual(nombre_carpeta, carpeta.nombre)

        self.assertEqual(0, Ejercicio.objects.count())
        self._crear_ejercicio('Ejercicio 1', carpeta)
        self.assertEqual(1, Ejercicio.objects.count())
        self._crear_ejercicio('Ejercicio 2', carpeta)
        self.assertEqual(2, Ejercicio.objects.count())

        ejercicio1 = Ejercicio.objects.all()[0]
        ejercicio2 = Ejercicio.objects.all()[1]

        self.assertEqual(2, carpeta.ejercicios.count())
        self.assertTrue(ejercicio1 in carpeta.ejercicios.all())
        self.assertTrue(ejercicio2 in carpeta.ejercicios.all())

    def _crear_ejercicio(self, nombre, carpeta):
        oracion = Oracion(string='Me gustan los chocolates.')
        oracion.save()

        ejercicio = Ejercicio(nombre=nombre, oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Marcar el sujeto de la oración.'
        consigna = ConsignaAnalisisSintactico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_chocolates = Palabra.objects.get(string='chocolates', oracion=oracion)

        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE)
        respuesta.save()
        respuesta.palabras.add(palabra_los)
        respuesta.palabras.add(palabra_chocolates)
        respuesta.save()

    def test_publicar_carpeta(self):
        profesor = self.crear_profesor('profesor.carpeta@test.com')
        nombre_curso = 'Castellano 2'
        curso = profesor.crear_curso(nombre_curso)
        nombre_carpeta = 'Carpeta 2 - Análisis Sintáctico'
        carpeta = profesor.crear_carpeta(nombre_carpeta, curso)
        self.assertFalse(carpeta.visible)

        carpeta.publicar()
        self.assertTrue(carpeta.visible)

        carpeta.ocultar()
        self.assertFalse(carpeta.visible)

        carpeta.cambiar_visibilidad()
        self.assertTrue(carpeta.visible)

        carpeta.cambiar_visibilidad()
        self.assertFalse(carpeta.visible)


class EjerciciosConAndamiajeTest(BaseTestCase):

    def test_ejercicio_con_1_consigna_de_andamiaje(self):
        oracion_string = 'Me gustan los chocolates.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        consigna_string = 'Marcar el sujeto de la siguiente oración.'
        consigna = ConsignaConAndamiaje(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # andamiaje

        palabra_me = Palabra.objects.get(string='Me', oracion=oracion)
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_chocolates = Palabra.objects.get(string='chocolates', oracion=oracion)

        respuesta_andamiaje_error = RespuestaAndamiaje(consigna=consigna)
        respuesta_andamiaje_error.opcion = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_andamiaje_error.mensaje = u'Atención: ¿En qué persona y número está el verbo? Recordá que debe tener ' \
                                            u'concordancia con el núcleo del sujeto.'
        respuesta_andamiaje_error.save()
        respuesta_andamiaje_error.palabras.add(palabra_me)
        respuesta_andamiaje_error.save()

        respuesta_andamiaje_acierto = RespuestaAndamiaje(consigna=consigna)
        respuesta_andamiaje_acierto.opcion = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_andamiaje_acierto.mensaje = u'MUY BIEN: En esta oración, el sujeto gramatical (“los chocolates”) no ' \
                                              u'coincide con el sujeto que realiza o experimenta la acción (la primera ' \
                                              u'persona identificable en el pronombre “me”).'
        respuesta_andamiaje_acierto.save()
        respuesta_andamiaje_acierto.palabras.add(palabra_los)
        respuesta_andamiaje_acierto.palabras.add(palabra_chocolates)
        respuesta_andamiaje_acierto.save()

        andamiaje = Andamiaje(consigna=consigna, acierto=respuesta_andamiaje_acierto)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error)
        andamiaje.save()

        consigna.andamiajes.add(andamiaje)
        consigna.save()

        # verificacion de respuestas

        palabras = [palabra_me]
        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_correcta, mensaje = consigna.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_error.mensaje, mensaje)

        palabras = [palabra_los, palabra_chocolates]
        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.SUJETO_SIMPLE
        respuesta_correcta, mensaje = consigna.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertTrue(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_acierto.mensaje, mensaje)

    def test_ejercicio_con_2_consignas_de_andamiajes(self):
        oracion_string = 'Encontraron ahogados a los navegantes desaparecidos.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna 1

        consigna_string = 'Marcar el OD de la siguiente oración.'
        consigna_1 = ConsignaConAndamiaje(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna_1.save()

        palabra_ahogados = Palabra.objects.get(string='ahogados', oracion=oracion)
        palabra_a = Palabra.objects.get(string='a', oracion=oracion)
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_navegantes = Palabra.objects.get(string='navegantes', oracion=oracion)
        palabra_desaparecidos = Palabra.objects.get(string='desaparecidos', oracion=oracion)

        respuesta_andamiaje_error = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_error.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_error.mensaje = u'Atención: Recordá que el OD, al pasarse a voz pasiva, es Sujeto. ¿Qué ' \
                                            u'parte de la oración del ejercicio cumple con esa característica?'
        respuesta_andamiaje_error.save()
        respuesta_andamiaje_error.palabras.add(palabra_a)
        respuesta_andamiaje_error.palabras.add(palabra_los)
        respuesta_andamiaje_error.palabras.add(palabra_navegantes)
        respuesta_andamiaje_error.save()

        respuesta_andamiaje_acierto = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_acierto.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_acierto.mensaje = u'MUY BIEN'
        respuesta_andamiaje_acierto.save()
        respuesta_andamiaje_acierto.palabras.add(palabra_a)
        respuesta_andamiaje_acierto.palabras.add(palabra_los)
        respuesta_andamiaje_acierto.palabras.add(palabra_navegantes)
        respuesta_andamiaje_acierto.palabras.add(palabra_desaparecidos)
        respuesta_andamiaje_acierto.save()

        andamiaje = Andamiaje(consigna=consigna_1, acierto=respuesta_andamiaje_acierto)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error)
        andamiaje.save()

        consigna_1.andamiajes.add(andamiaje)
        consigna_1.save()

        # consigna 2

        consigna_string = '¿Qué función tiene "ahogados"?'
        consigna_2 = ConsignaConAndamiaje(ejercicio=ejercicio, numero=2, string=consigna_string)
        consigna_2.save()

        respuesta_andamiaje_error_1 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_error_1.opcion = RespuestaAnalisisSintactico.Opciones.PROPOSICION_INCLUIDA_DE_MODO
        respuesta_andamiaje_error_1.mensaje = u'Atención: Recordá que todos los circunstanciales son, por definición, ' \
                                              u'invariables. ¿Qué pasaría en esta oración si cambiara el género y/o el ' \
                                              u'número del núcleo del OD?'
        respuesta_andamiaje_error_1.save()
        respuesta_andamiaje_error_1.palabras.add(palabra_ahogados)
        respuesta_andamiaje_error_1.save()

        respuesta_andamiaje_error_2 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_error_2.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_SUBJETIVO
        respuesta_andamiaje_error_2.mensaje = u'Atención: El predicativo subjetivo es el modificador del núcleo del ' \
                                              u'predicado que concuerda en género y número con el núcleo del sujeto; ' \
                                              u'en este caso, la concordancia se da con el núcleo del OD. Por lo ' \
                                              u'tanto, ¿qué función sintáctica es la correcta?'
        respuesta_andamiaje_error_2.save()
        respuesta_andamiaje_error_2.palabras.add(palabra_ahogados)
        respuesta_andamiaje_error_2.save()

        respuesta_andamiaje_acierto_2 = RespuestaAndamiaje(consigna=consigna_2)
        respuesta_andamiaje_acierto_2.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_OBJETIVO
        respuesta_andamiaje_acierto_2.mensaje = u'MUY BIEN: A diferencia de los circunstanciales, que modifican ' \
                                                u'únicamente a los verbos y no establecen concordancia en género/número ' \
                                                u'con ninguna parte de la oración, los predicativos aportan información ' \
                                                u'sobre el sujeto (PS) o el OD (PO); por esa razón, concuerdan en género ' \
                                                u'y número con el núcleo respectivo.'
        respuesta_andamiaje_acierto_2.save()
        respuesta_andamiaje_acierto_2.palabras.add(palabra_ahogados)
        respuesta_andamiaje_acierto_2.save()

        andamiaje = Andamiaje(consigna=consigna_2, acierto=respuesta_andamiaje_acierto_2)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error_1)
        andamiaje.errores.add(respuesta_andamiaje_error_2)
        andamiaje.save()

        consigna_2.andamiajes.add(andamiaje)
        consigna_2.save()

        # verificacion de respuestas de la consigna 1

        palabras = [palabra_a, palabra_los, palabra_navegantes]
        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_correcta, mensaje = consigna_1.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_error.mensaje, mensaje)

        palabras = [palabra_a, palabra_los, palabra_navegantes, palabra_desaparecidos]
        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_correcta, mensaje = consigna_1.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertTrue(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_acierto.mensaje, mensaje)

        # verificacion de respuestas de la consigna 2

        palabras = [palabra_ahogados]
        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.PROPOSICION_INCLUIDA_DE_MODO
        respuesta_correcta, mensaje = consigna_2.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_error_1.mensaje, mensaje)

        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_SUBJETIVO
        respuesta_correcta, mensaje = consigna_2.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertFalse(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_error_2.mensaje, mensaje)

        opcion_de_respuesta = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_PREDICATIVO_OBJETIVO
        respuesta_correcta, mensaje = consigna_2.es_respuesta_correcta(palabras, opcion_de_respuesta)
        self.assertTrue(respuesta_correcta)
        self.assertEqual(respuesta_andamiaje_acierto_2.mensaje, mensaje)


class BorradoDeCursosTest(BaseTestCase):

    def test_borrar_un_curso_borra_carpetas_y_ejercicios_de_ese_curso(self):
        def crear_ejercicio(nombre, oracion_string, carpeta):
            oracion = Oracion(string=oracion_string)
            oracion.save()
            ejercicio = Ejercicio(nombre=nombre, oracion=oracion, carpeta=carpeta)
            ejercicio.save()

        profesor = self.crear_profesor()
        curso, carpeta1 = self.crear_curso_y_carpeta(profesor)
        carpeta2 = profesor.crear_carpeta('Carpeta 2', curso)

        crear_ejercicio('Ejercicio 1', 'Hola, mundo.', carpeta1)
        crear_ejercicio('Ejercicio 2', 'Chau, mundo.', carpeta1)
        crear_ejercicio('Ejercicio 1', 'Hoy comí helado.', carpeta2)
        crear_ejercicio('Ejercicio 2', 'Ayer comí pastas.', carpeta2)

        self.assertEqual(2, carpeta1.ejercicios.count())
        self.assertEqual(2, carpeta2.ejercicios.count())
        self.assertEqual(4, Ejercicio.objects.count())

        self.assertEqual(2, curso.carpetas.count())
        self.assertEqual(2, Carpeta.objects.count())

        self.assertEqual(1, Curso.objects.count())

        curso.delete()

        self.assertEqual(0, Ejercicio.objects.count())
        self.assertEqual(0, Carpeta.objects.count())
        self.assertEqual(0, Curso.objects.count())

    def test_borrar_una_carpeta_borra_ejercicios_de_esa_carpeta(self):
        def crear_ejercicio(nombre, oracion_string, carpeta):
            oracion = Oracion(string=oracion_string)
            oracion.save()
            ejercicio = Ejercicio(nombre=nombre, oracion=oracion, carpeta=carpeta)
            ejercicio.save()

        profesor = self.crear_profesor()
        curso, carpeta1 = self.crear_curso_y_carpeta(profesor)
        carpeta2 = profesor.crear_carpeta('Carpeta 2', curso)

        crear_ejercicio('Ejercicio 1', 'Hola, mundo.', carpeta1)
        crear_ejercicio('Ejercicio 2', 'Chau, mundo.', carpeta1)
        crear_ejercicio('Ejercicio 1', 'Hoy comí helado.', carpeta2)
        crear_ejercicio('Ejercicio 2', 'Ayer comí pastas.', carpeta2)

        self.assertEqual(2, carpeta1.ejercicios.count())
        self.assertEqual(2, carpeta2.ejercicios.count())
        self.assertEqual(4, Ejercicio.objects.count())

        self.assertEqual(2, curso.carpetas.count())
        self.assertEqual(2, Carpeta.objects.count())

        self.assertEqual(1, Curso.objects.count())

        carpeta1.delete()

        self.assertEqual(2, carpeta2.ejercicios.count())
        self.assertEqual(2, Ejercicio.objects.count())

        self.assertEqual(1, curso.carpetas.count())
        self.assertEqual(1, Carpeta.objects.count())

        self.assertEqual(1, Curso.objects.count())


class CopiaDeCarpetaTest(BaseTestCase):

    def crear_consigna_clase_palabra(self, ejercicio):
        # consigna
        consigna_string = 'Determinar qué clase de palabra es cada una en la oración siguiente.'
        consigna = ConsignaClaseDePalabra(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas
        for palabra in self.oracion.palabras.all():
            respuesta_clase_de_palabra = RespuestaClaseDePalabra(palabra=palabra, consigna=consigna,
                                                                 opcion=RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO)
            if palabra.string == 'uidet':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.VERBO
            elif palabra.string == 'cum' or palabra.string == 'in':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL

            respuesta_clase_de_palabra.save()
        return consigna

    def crear_consigna_analisis_sintactico_sin_andamiaje(self, ejercicio):
        numero_respuesta = 1

        # consigna
        consigna_string = 'Realizar el análisis sintáctico de la oración.'
        consigna = ConsignaAnalisisSintactico(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        # respuestas correctas:
        respuestas = []

        palabra_viaje = Palabra.objects.get(string='Viajé')
        palabra_a = Palabra.objects.get(string='a')
        palabra_sydney = Palabra.objects.get(string='Sydney')

        # Viajé (n) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.NUCLEO)
        respuesta.save()
        respuesta.palabras.add(palabra_viaje)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        # a (sub.) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna,
                                                opcion=RespuestaAnalisisSintactico.Opciones.SUBORDINANTE)
        respuesta.save()
        respuesta.palabras.add(palabra_a)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        # Sydney (T.) / respuesta "falsa" que se asume correcta
        respuesta = RespuestaAnalisisSintactico(consigna=consigna, opcion=RespuestaAnalisisSintactico.Opciones.TERMINO)
        respuesta.save()
        respuesta.palabras.add(palabra_sydney)
        respuesta.save()
        self.agregar_respuesta(respuesta, numero_respuesta, respuestas)

        return consigna

    def crear_consigna_con_andamiaje(self, ejercicio):
        oracion = ejercicio.oracion

        # consigna 1
        consigna_string = 'Marcar el OD de la siguiente oración.'
        consigna_1 = ConsignaConAndamiaje(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna_1.save()

        palabra_ahogados = Palabra.objects.get(string='ahogados', oracion=oracion)
        palabra_a = Palabra.objects.get(string='a', oracion=oracion)
        palabra_los = Palabra.objects.get(string='los', oracion=oracion)
        palabra_navegantes = Palabra.objects.get(string='navegantes', oracion=oracion)
        palabra_desaparecidos = Palabra.objects.get(string='desaparecidos', oracion=oracion)

        respuesta_andamiaje_error = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_error.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_error.mensaje = u'Atención: Recordá que el OD, al pasarse a voz pasiva, es Sujeto. ¿Qué ' \
                                            u'parte de la oración del ejercicio cumple con esa característica?'
        respuesta_andamiaje_error.save()
        respuesta_andamiaje_error.palabras.add(palabra_a)
        respuesta_andamiaje_error.palabras.add(palabra_los)
        respuesta_andamiaje_error.palabras.add(palabra_navegantes)
        respuesta_andamiaje_error.save()

        respuesta_andamiaje_acierto = RespuestaAndamiaje(consigna=consigna_1)
        respuesta_andamiaje_acierto.opcion = RespuestaAnalisisSintactico.Opciones.PREDICADO_VERBAL_COMPUESTO_OBJETO_DIRECTO
        respuesta_andamiaje_acierto.mensaje = u'MUY BIEN'
        respuesta_andamiaje_acierto.save()
        respuesta_andamiaje_acierto.palabras.add(palabra_a)
        respuesta_andamiaje_acierto.palabras.add(palabra_los)
        respuesta_andamiaje_acierto.palabras.add(palabra_navegantes)
        respuesta_andamiaje_acierto.palabras.add(palabra_desaparecidos)
        respuesta_andamiaje_acierto.save()

        andamiaje = Andamiaje(consigna=consigna_1, acierto=respuesta_andamiaje_acierto)
        andamiaje.save()
        andamiaje.errores.add(respuesta_andamiaje_error)
        andamiaje.save()

        consigna_1.andamiajes.add(andamiaje)
        consigna_1.save()

        return consigna_1

    def test_carpeta_y_ejercicio_clase_palabra_son_copiados_a_otro_curso_junto_con_las_respuestas(self):
        profesor = self.crear_profesor()
        curso1, carpeta = self.crear_curso_y_carpeta(profesor)

        self.assertEqual(curso1.carpetas.count(), 1)
        self.assertEqual(carpeta.ejercicios.count(), 0)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        consigna = self.crear_consigna_clase_palabra(ejercicio)

        self.assertEqual(carpeta.ejercicios.count(), 1)
        self.assertEqual(len(ejercicio.consignas), 1)
        self.assertEqual(consigna.respuestas.count(), 8)

        # se copia la carpeta a otro curso
        curso2 = Curso(nombre='Curso 2', profesor=profesor)
        curso2.save()

        self.assertEqual(curso2.carpetas.count(), 0)

        carpeta_copiada = carpeta.copiar(curso2)

        self.assertEqual(curso2.carpetas.count(), 1)
        self.assertEqual(carpeta_copiada.ejercicios.count(), 1)

        ejercicio_copiado = carpeta_copiada.ejercicios.all()[0]
        self.assertEqual(len(ejercicio_copiado.consignas), 1)

        consigna_copiada = ejercicio_copiado.consignas[0]
        self.assertEqual(consigna_copiada.respuestas.count(), 8)

        self.assertNotEqual(ejercicio.id, ejercicio_copiado.id)
        self.assertNotEqual(carpeta.id, carpeta_copiada.id)
        self.assertNotEqual(consigna.id, consigna_copiada.id)
        self.assertNotEqual(ejercicio.oracion.id, ejercicio_copiado.oracion.id)

    def test_carpeta_y_ejercicio_analisis_sintactico_sin_andamiaje_son_copiados_a_otro_curso_junto_con_las_respuestas(self):
        profesor = self.crear_profesor()
        curso1, carpeta = self.crear_curso_y_carpeta(profesor)

        self.assertEqual(curso1.carpetas.count(), 1)
        self.assertEqual(carpeta.ejercicios.count(), 0)

        oracion = Oracion(string='Viajé a Sydney')  # Sujeto tácito: yo
        oracion.save()

        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        consigna = self.crear_consigna_analisis_sintactico_sin_andamiaje(ejercicio)

        self.assertEqual(carpeta.ejercicios.count(), 1)
        self.assertEqual(len(ejercicio.consignas), 1)
        self.assertEqual(consigna.respuestas.count(), 3)

        # se copia la carpeta a otro curso
        curso2 = Curso(nombre='Curso 2', profesor=profesor)
        curso2.save()

        self.assertEqual(curso2.carpetas.count(), 0)

        carpeta_copiada = carpeta.copiar(curso2)

        self.assertEqual(curso2.carpetas.count(), 1)
        self.assertEqual(carpeta_copiada.ejercicios.count(), 1)

        ejercicio_copiado = carpeta_copiada.ejercicios.all()[0]
        self.assertEqual(len(ejercicio_copiado.consignas), 1)

        consigna_copiada = ejercicio_copiado.consignas[0]
        self.assertEqual(consigna_copiada.respuestas.count(), 3)

        self.assertNotEqual(ejercicio.id, ejercicio_copiado.id)
        self.assertNotEqual(carpeta.id, carpeta_copiada.id)
        self.assertNotEqual(consigna.id, consigna_copiada.id)
        self.assertNotEqual(ejercicio.oracion.id, ejercicio_copiado.oracion.id)

    def test_carpeta_y_ejercicio_con_andamiaje_son_copiados_a_otro_curso_junto_con_las_respuestas(self):
        profesor = self.crear_profesor()
        curso1, carpeta = self.crear_curso_y_carpeta(profesor)

        self.assertEqual(curso1.carpetas.count(), 1)
        self.assertEqual(carpeta.ejercicios.count(), 0)

        oracion_string = 'Encontraron ahogados a los navegantes desaparecidos.'
        oracion = Oracion(string=oracion_string)
        oracion.categoria = Oracion.Categorias.OBS
        oracion.save()

        # ejercicio
        ejercicio = Ejercicio(oracion=oracion, carpeta=carpeta)
        ejercicio.save()

        consigna = self.crear_consigna_con_andamiaje(ejercicio)

        self.assertEqual(carpeta.ejercicios.count(), 1)
        self.assertEqual(len(ejercicio.consignas), 1)
        self.assertEqual(consigna.respuestas.count(), 2)

        # se copia la carpeta a otro curso
        curso2 = Curso(nombre='Curso 2', profesor=profesor)
        curso2.save()

        self.assertEqual(curso2.carpetas.count(), 0)

        carpeta_copiada = carpeta.copiar(curso2)

        self.assertEqual(curso2.carpetas.count(), 1)
        self.assertEqual(carpeta_copiada.ejercicios.count(), 1)

        ejercicio_copiado = carpeta_copiada.ejercicios.all()[0]
        self.assertEqual(len(ejercicio_copiado.consignas), 1)

        consigna_copiada = ejercicio_copiado.consignas[0]
        self.assertEqual(consigna_copiada.respuestas.count(), 2)

        self.assertNotEqual(ejercicio.id, ejercicio_copiado.id)
        self.assertNotEqual(carpeta.id, carpeta_copiada.id)
        self.assertNotEqual(consigna.id, consigna_copiada.id)
        self.assertNotEqual(ejercicio.oracion.id, ejercicio_copiado.oracion.id)


class ResolucionDeEjercicioEsGuardada(BaseTestCase):

    def crear_consigna(self):
        profesor = self.crear_profesor()
        curso, carpeta = self.crear_curso_y_carpeta(profesor)

        ejercicio = Ejercicio(oracion=self.oracion, carpeta=carpeta)
        ejercicio.save()

        # consigna
        consigna_string = 'Determinar qué clase de palabra es cada una en la oración siguiente.'
        consigna = ConsignaClaseDePalabra(ejercicio=ejercicio, numero=1, string=consigna_string)
        consigna.save()

        return consigna

    def crear_ejercicio(self):
        consigna = self.crear_consigna()

        # se guardan las respuestas correctas, tal como las cargaria el profesor
        respuestas = list()
        for palabra in self.oracion.palabras.all():
            respuesta_clase_de_palabra = RespuestaClaseDePalabra(palabra=palabra, consigna=consigna,
                                                                 opcion=RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO)
            if palabra.string == 'uidet':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.VERBO
            elif palabra.string == 'cum' or palabra.string == 'in':
                respuesta_clase_de_palabra.opcion = RespuestaClaseDePalabra.TiposDePalabra.SUBORDINANTE_PREPOSICIONAL

            respuesta_clase_de_palabra.save()
            respuestas.append(respuesta_clase_de_palabra)

        return consigna, respuestas

    def crear_diccionario_respuestas(self):
        # se crea la consigna y por consiguiente las palabras
        consigna = self.crear_consigna()

        # se obtienen las palabras
        palabra_phoebus = Palabra.objects.get(string='Phoebus')
        palabra_uidet = Palabra.objects.get(string='uidet')
        palabra_filium = Palabra.objects.get(string='filium')
        palabra_cythereiae = Palabra.objects.get(string='Cythereiae')
        palabra_cum = Palabra.objects.get(string='cum')
        palabra_pharetra = Palabra.objects.get(string='pharetra')
        palabra_in = Palabra.objects.get(string='in')
        palabra_umero = Palabra.objects.get(string='umero')

        # respuesta completa correcta
        respuestas = list()
        respuestas.append({'posiciones_palabras': [palabra_phoebus.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 1})
        respuestas.append({'posiciones_palabras': [palabra_uidet.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 2})
        respuestas.append({'posiciones_palabras': [palabra_filium.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 3})
        respuestas.append({'posiciones_palabras': [palabra_cythereiae.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 4})
        respuestas.append({'posiciones_palabras': [palabra_cum.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                           'numero_respuesta': 5})
        respuestas.append({'posiciones_palabras': [palabra_pharetra.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 6})
        respuestas.append({'posiciones_palabras': [palabra_in.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                           'numero_respuesta': 7})
        respuestas.append({'posiciones_palabras': [palabra_umero.posicion],
                           'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO, 'numero_respuesta': 8})

        return consigna, respuestas

    def test_resolucion_de_ejercicio_es_guardada(self):
        self.assertEqual(0, len(RespuestaClaseDePalabra.objects.all()))

        consigna, respuestas = self.crear_ejercicio()

        # se crea un alumno para que responda a la consigna
        alumno = self.crear_alumno()

        self.assertEqual(8, len(RespuestaClaseDePalabra.objects.all()))
        for respuesta in respuestas:
            self.assertTrue(respuesta.correcta)

        respuestas_alumno = list()
        for respuesta in respuestas:
            respuesta.id = None
            respuesta.save()
            respuestas_alumno.append(respuesta)

        alumno.responder_consigna(consigna, respuestas_alumno)

        self.assertEqual(8, len(RespuestaClaseDePalabra.objects.filter(alumno=None)))
        self.assertEqual(8, len(RespuestaClaseDePalabra.objects.filter(alumno=alumno)))
        for respuesta in respuestas:
            self.assertTrue(respuesta.correcta)

        # se le pide a la consigna ver las respuestas del alumno a dicha consigna
        self.assertEqual(8, consigna.obtener_respuestas_alumno(alumno).count())

        self.assertEqual(8, len(RespuestaClaseDePalabra.objects.filter(alumno=None)))
        self.assertEqual(8, len(RespuestaClaseDePalabra.objects.filter(alumno=alumno)))
        for respuesta in respuestas:
            self.assertTrue(respuesta.correcta)

    def test_respuestas_anteriores_son_reemplazadas_por_nuevas_respuestas(self):
        consigna, respuestas = self.crear_ejercicio()
        self.assertEqual(8, consigna.tipo_respuesta.objects.filter(alumno=None, consigna=consigna).count())

        # se copian las respuestas correctas solo para el test
        respuestas_copia = list()
        for respuesta in respuestas:
            respuesta.id = None
            respuesta.save()
            respuestas_copia.append(respuesta)
        respuestas = respuestas_copia

        # se crea un alumno para que responda a la consigna
        alumno = self.crear_alumno()
        alumno.responder_consigna(consigna, respuestas, True)

        # se le pide a la consigna ver las respuestas del alumno a dicha consigna
        self.assertEqual(8, consigna.obtener_respuestas_alumno(alumno).count())

        # se cambia una respuesta
        palabra = self.oracion.palabras.all()[0]
        nueva_respuesta = RespuestaClaseDePalabra(palabra=palabra, consigna=consigna, opcion=RespuestaClaseDePalabra.TiposDePalabra.ADJETIVO)
        respuestas[0] = nueva_respuesta
        alumno.responder_consigna(consigna, respuestas, True)

        # se le pide a la consigna ver las respuestas del alumno a dicha consigna
        self.assertEqual(8, consigna.obtener_respuestas_alumno(alumno).count())

        # las respuestas del profesor deben persistir inalteradas
        self.assertEqual(8, consigna.tipo_respuesta.objects.filter(alumno=None, consigna=consigna).count())

    def test_respuestas_se_convierten_de_diccionario_a_objetos(self):
        consigna, diccionario_respuestas = self.crear_diccionario_respuestas()

        respuestas = Respuesta.from_dict(consigna, diccionario_respuestas)

        for respuesta in respuestas:
            self.assertEqual(respuesta.opcion, RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO)
            self.assertEqual(respuesta.consigna, consigna)

    def test_respuesta_from_dict_contempla_multiples_palabras_en_una_respuesta(self):
        consigna = self.crear_consigna()
        respuestas_list = [
            {
                'opcion': '401',
                'numero_respuesta': 1,
                'posiciones_palabras': ['1', '2']
            },
            {
                'opcion': '408',
                'numero_respuesta': 2, 'posiciones_palabras': ['0']
            }
        ]

        respuestas = Respuesta.from_dict(consigna, respuestas_list)
        self.assertEqual(2, len(respuestas_list))
        self.assertEqual(3, len(respuestas))

    def test_respuesta_incorrecta_es_detectada(self):
        consigna, respuestas_correctas = self.crear_ejercicio()
        alumno = self.crear_alumno()
        respuestas_list = [
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                'numero_respuesta': 1,
                'posiciones_palabras': ['0', '2']
            },
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                'numero_respuesta': 2, 'posiciones_palabras': ['1']
            }
        ]

        respuestas_alumno = Respuesta.from_dict(consigna, respuestas_list)
        respuestas_alumno = alumno.responder_consigna(consigna, respuestas_alumno)

        respuestas_alumno = sorted(respuestas_alumno, key=lambda respuesta: respuesta.palabra.posicion)
        self.assertTrue(respuestas_alumno[0].correcta)
        self.assertFalse(respuestas_alumno[1].correcta)
        self.assertTrue(respuestas_alumno[2].correcta)

    def test_se_borran_respuestas_de_consigna_eliminada(self):
        consigna, respuestas_correctas = self.crear_ejercicio()
        tipo_respuesta = consigna.tipo_respuesta

        self.assertEqual(8, tipo_respuesta.objects.filter(alumno=None, consigna=consigna).count())
        consigna.delete()
        self.assertEqual(0, tipo_respuesta.objects.filter(alumno=None, consigna=consigna).count())

    def test_respuesta_incorrecta_es_reemplazada(self):
        consigna, respuestas_correctas = self.crear_ejercicio()
        alumno = self.crear_alumno()
        respuestas_list = [
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                'numero_respuesta': 1,
                'posiciones_palabras': ['0', '2']
            },
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                'numero_respuesta': 2,
                'posiciones_palabras': ['1']
            }
        ]

        respuestas_alumno = Respuesta.from_dict(consigna, respuestas_list)
        respuestas_alumno = alumno.responder_consigna(consigna, respuestas_alumno)

        respuestas_alumno = sorted(respuestas_alumno, key=lambda respuesta: respuesta.palabra.posicion)
        self.assertTrue(respuestas_alumno[0].correcta)
        self.assertFalse(respuestas_alumno[1].correcta)
        self.assertTrue(respuestas_alumno[2].correcta)

        self.assertEqual(3, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna).count())
        self.assertEqual(2, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna, correcta=True).count())
        self.assertEqual(1, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna, correcta=False).count())

        respuestas_list = [
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.SUSTANTIVO,
                'numero_respuesta': 3,
                'posiciones_palabras': ['0', '2']
            },
            {
                'opcion': RespuestaClaseDePalabra.TiposDePalabra.VERBO,
                'numero_respuesta': 4,
                'posiciones_palabras': ['1']
            }
        ]

        respuestas_alumno = Respuesta.from_dict(consigna, respuestas_list)
        respuestas_alumno = alumno.responder_consigna(consigna, respuestas_alumno)

        respuestas_alumno = sorted(respuestas_alumno, key=lambda respuesta: respuesta.palabra.posicion)
        self.assertTrue(respuestas_alumno[0].correcta)
        self.assertTrue(respuestas_alumno[1].correcta)
        self.assertTrue(respuestas_alumno[2].correcta)

        self.assertEqual(3, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna).count())
        self.assertEqual(3, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna, correcta=True).count())
        self.assertEqual(0, RespuestaClaseDePalabra.objects.filter(alumno=alumno, consigna=consigna, correcta=False).count())
